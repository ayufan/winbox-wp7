﻿using System;
using System.Net;
using System.Collections.ObjectModel;
using System.IO.IsolatedStorage;
using System.Diagnostics;

namespace RouterOS
{
    public class AddressCollection : ObservableCollection<string>
    {
        private static AddressCollection collection;

        public static AddressCollection Addresses
        {
            get
            {
                if (collection != null)
                    return collection;

                try
                {
                    lock (IsolatedStorageSettings.ApplicationSettings)
                    {
                        if (!IsolatedStorageSettings.ApplicationSettings.TryGetValue("addresses", out collection))
                        {
                            collection = new AddressCollection();
                            IsolatedStorageSettings.ApplicationSettings["addresses"] = collection;
                        }
                    }

                    collection.CollectionChanged += AddressChanged;
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("[ADDRESSES] " + ex.Message);
                    collection = new AddressCollection();
                }
                return collection;
            }
        }

        private static void AddressChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() => IsolatedStorageSettings.ApplicationSettings.Save());
        }
    }
}
