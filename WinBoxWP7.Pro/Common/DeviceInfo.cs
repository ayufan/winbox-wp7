﻿using System;
using System.Net;
using System.Collections.ObjectModel;
using System.IO;
using System.IO.IsolatedStorage;
using System.Text;
using System.Linq;
using System.Runtime.Serialization;

using CommonLib;

namespace RouterOS
{
    public class DeviceInfo : IComparable<DeviceInfo>
    {
        public Guid Uid { get; private set; }
        public string Name { get; set; }
        public string Host { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public ushort Port { get; set; }
        public DateTime? LastLogged { get; set; }
        public MACAddress? MacAddress { get; set; }

        public bool UseSSH { get; set; }
        public Guid SshConnection { get; set; }

        [IgnoreDataMember]
        public RouterOS.API.Connection API { get; set; }

        public DeviceInfo()
        {
            Uid = Guid.NewGuid();
            Name = "New device";
            Port = 8728;
            User = "admin";
            Password = "";
        }

        public bool IsValid
        {
            get
            {
                if (string.IsNullOrWhiteSpace(Name))
                    return false;
                if (string.IsNullOrWhiteSpace(Host))
                    return false;
                if (string.IsNullOrWhiteSpace(User))
                    return false;
                if (Port == 0)
                    return false;
                return true;
            }
        }

        public override string ToString()
        {
            return String.Format("{0} // {2}", Name, Host, User);
        }

        public int CompareTo(DeviceInfo other)
        {
            return Name.CompareTo(other.Name);
        }

        public DeviceInfo Clone()
        {
            DeviceInfo info = (DeviceInfo)this.MemberwiseClone();
            info.Uid = Guid.NewGuid();
            return info;
        }
    }

    public class DeviceCollection : UpdatableObservableCollection<DeviceInfo>
    {
        public DeviceCollection()
        {
        }

        public DeviceInfo Find(MACAddress address)
        {
            foreach (DeviceInfo info in this)
                if (info.MacAddress.GetValueOrDefault() == address)
                    return info;
            return null;
        }

        public DeviceInfo Find(Guid uid)
        {
            foreach (DeviceInfo info in this)
                if (info.Uid == uid)
                    return info;
            return null;
        }

        private static DeviceCollection collection;

        public static DeviceCollection Devices
        {
            get {
                if (collection != null)
                    return collection;
                lock (IsolatedStorageSettings.ApplicationSettings)
                {
                    if (!IsolatedStorageSettings.ApplicationSettings.TryGetValue("devices", out collection))
                    {
                        collection = new DeviceCollection();
#if TEST_CREDENTIALS
                        collection.Add(new DeviceInfo()
                        {
                            Name = "home",
                            Host = "home.ayufan.eu",
                            User = "api",
                            Password = "api"
                        });
                        collection.Add(new DeviceInfo()
                        {
                            Name = "rtr",
                            Host = "127.0.0.1",
                            User = "api",
                            Password = "nmkQrZpdIXLUruA3",
                            UseSSH = true,
                            SshConnection = new Guid("16573C59-8673-488F-923A-8E98DBB794AC"),
                        });
#endif
                        IsolatedStorageSettings.ApplicationSettings["devices"] = collection;
                    }
                }
                collection.CollectionChanged += collection.DevicesChanged;
                return collection;
            }
        }

        private bool disableSorting = false;

        private void DevicesChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (disableSorting)
                return;
            System.Windows.Deployment.Current.Dispatcher.BeginInvoke(this.DeviceSort);
        }

        private void DeviceSort()
        {
            lock(this)
            {
                var sorted = this.ToList();

                sorted.Sort();

                disableSorting = true;

                try
                {
                    for (int i = 0; i < Count; ++i)
                    {
                        if (this[i] == sorted[i])
                            continue;
                        SetItem(i, sorted[i]);
                    }
                }
                catch (Exception)
                {
                }
                finally
                {
                    disableSorting = true;
                }

                IsolatedStorageSettings.ApplicationSettings.Save();
            }
        }
    }
}
