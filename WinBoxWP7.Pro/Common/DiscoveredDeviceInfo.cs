﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

using System.Text;
using CommonLib;

namespace RouterOS
{
    public class DiscoveredDeviceInfo
    {
        public string Identity { get; set; }
        public MACAddress? MacAddress { get; set; }
        public IPAddress IpAddress { get; set; }
        public string Vendor { get; set; }
        public string BoardName { get; set; }
        public string Version { get; set; }
        public string License { get; set; }
        public string Interface { get; set; }
        public DateTime Date { get; set; }
        public TimeSpan Age { get { return DateTime.Now - Date; } }

        public bool IsAdded
        {
            get { return DeviceInfo != null; }
        }
        public bool IsNew
        {
            get { return DeviceInfo == null; }
        }
        public DeviceInfo DeviceInfo
        {
            get { return MacAddress.HasValue ? DeviceCollection.Devices.Find(MacAddress.Value) : null; }
        }

        public override string ToString()
        {
            if (!string.IsNullOrEmpty(Identity))
                return Identity;
            if (IpAddress != null)
                return IpAddress.ToString();
            if (MacAddress.HasValue)
                return MacAddress.ToString();
            return "unknown device";
        }

        public bool Parse(byte[] buffer)
        {
            int totalSize = (ushort)((buffer[0] << 24) + (buffer[1] << 16) + (buffer[2] << 8) + buffer[3]);

            for (int offset = 4; offset < buffer.Length; )
            {
                ushort cmd = (ushort)((buffer[offset] << 8) + buffer[offset + 1]);
                ushort cmdSize = (ushort)((buffer[offset + 2] << 8) + buffer[offset + 3]);
                int cmdOffset = offset + 4;
                offset += 4 + cmdSize;

                try
                {
                    switch (cmd)
                    {
                        case 0x1:
                            MacAddress = new MACAddress(buffer, cmdOffset, cmdSize);
                            break;

                        case 0x5:
                            Identity = Encoding.UTF8.GetString(buffer, cmdOffset, cmdSize);
                            break;

                        case 0x7:
                            Version = Encoding.UTF8.GetString(buffer, cmdOffset, cmdSize);
                            break;

                        case 0x8:
                            Vendor = Encoding.UTF8.GetString(buffer, cmdOffset, cmdSize);
                            break;

                        case 0xb:
                            License = Encoding.UTF8.GetString(buffer, cmdOffset, cmdSize);
                            break;

                        case 0xc:
                            BoardName = Encoding.UTF8.GetString(buffer, cmdOffset, cmdSize);
                            break;

                        case 0x10:
                            Interface = Encoding.UTF8.GetString(buffer, cmdOffset, cmdSize);
                            break;
                    }
                }
                catch (Exception)
                {
                }
            }

            if (!string.IsNullOrEmpty(Identity) && !string.IsNullOrEmpty(Vendor) && !string.IsNullOrEmpty(Version))
            {
                Date = DateTime.Now;
                return true;
            }
            return false;
        }
    }

    public class DiscoveredDeviceInfoCollection : UpdatableObservableCollection<DiscoveredDeviceInfo>
    {
        public DiscoveredDeviceInfo Find(IPAddress address)
        {
            foreach (DiscoveredDeviceInfo device in this)
            {
                if (device.IpAddress.Equals(address))
                    return device;
            }
            return null;
        }
    }
}
