﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using CommonLib;
using System.IO.IsolatedStorage;

namespace RouterOS
{
    public class SshConnectionInfo
    {
        public Guid Uid { get; set; }
        public string Name { get; set; }
        public string SshHost { get; set; }
        public string SshUser { get; set; }
        public string SshPassword { get; set; }
        public ushort SshPort { get; set; }
        public byte[] SshHostKey { get; set; }

        public SshConnectionInfo()
        {
            Uid = Guid.NewGuid();
            Name = "New connection";
            SshUser = "admin";
            SshPort = 22;
        }

        public bool IsValid
        {
            get
            {
                if (string.IsNullOrWhiteSpace(Name))
                    return false;
                if (string.IsNullOrWhiteSpace(SshHost))
                    return false;
                if (string.IsNullOrWhiteSpace(SshUser))
                    return false;
                if (SshPort == 0)
                    return false;
                return true;
            }
        }

        public override string ToString()
        {
            return String.Format("{0} // {2}", Name, SshHost, SshUser);
        }
    }

    public class SshConnectionCollection : UpdatableObservableCollection<SshConnectionInfo>
    {
        public SshConnectionCollection()
        {
        }

        public SshConnectionInfo Find(Guid uid)
        {
            foreach (var info in this)
                if (info.Uid == uid)
                    return info;
            return null;
        }

        private static SshConnectionCollection collection;

        public static SshConnectionCollection SshConnections
        {
            get
            {
                if (collection != null)
                    return collection;
                lock (IsolatedStorageSettings.ApplicationSettings)
                {
                    if (!IsolatedStorageSettings.ApplicationSettings.TryGetValue("sshConnections", out collection))
                    {
                        collection = new SshConnectionCollection();
#if TEST_CREDENTIALS
                        collection.Add(new SshConnectionInfo()
                        {
                            Uid = new Guid("16573C59-8673-488F-923A-8E98DBB794AC"),
                            Name = "OSK-NET VPN",
                            SshHost = "rtr.osk-net.pl",
                            SshUser = "api",
                            SshPassword = "nmkQrZpdIXLUruA3"
                        });
#endif
                        IsolatedStorageSettings.ApplicationSettings["sshConnections"] = collection;
                    }
                }
                collection.CollectionChanged += SshConnectionsChanged;
                return collection;
            }
        }

        private static void SshConnectionsChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() => IsolatedStorageSettings.ApplicationSettings.Save());
        }
    }
}
