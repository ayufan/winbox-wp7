﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace RouterOS.Converters
{
    public class StringFormatConverter : DependencyObject, System.Windows.Data.IValueConverter
    {
        public bool FormatEmptyStrings { get; set; }

        public StringFormatConverter()
        {
            FormatEmptyStrings = false;
        }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            String format = parameter as String;
            if (value == null || format == null)
                return value;

            if (value == null && !FormatEmptyStrings)
                return value;

            if (format.StartsWith("{}"))
                format = format.Substring(2);

            return String.Format(culture, format, value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
