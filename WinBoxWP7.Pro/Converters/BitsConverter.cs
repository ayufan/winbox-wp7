﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace RouterOS.Converters
{
    [CommonLib.ValueConversion(typeof(Bits), typeof(string))]
    public class BitsConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is Bits)
            {
                Bits bits = (Bits)value;
                return bits.GetNiceString();
            }

            if (value is long)
            {
                return new Bits((long)value).GetNiceString();
            }

            if (value is string)
            {
                return Bits.Parse(value.ToString()).GetNiceString();
            }

            throw new NotImplementedException();
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
