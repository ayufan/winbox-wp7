﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace RouterOS.Converters
{
    [CommonLib.ValueConversion(typeof(Bytes), typeof(string))]
    public class BytesConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int scalar = 1;

            if (parameter is string)
            {
                int.TryParse((string)parameter, out scalar);
            }

            if (value is Bytes)
            {
                Bytes bytes = (Bytes)value;
                bytes = new Bytes(bytes.Count * scalar);
                return bytes.GetNiceString();
            }

            if (value is Bits)
            {
                Bits bits = (Bits)value;
                bits = new Bits(bits.Count * scalar);
                return bits.GetNiceString();
            }

            if (value is long)
            {
                return new Bytes((long)value * scalar).GetNiceString();
            }

            if (value is string)
            {
                return new Bytes((long)(double.Parse(value.ToString()) * scalar)).GetNiceString();
            }

            throw new NotImplementedException();
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
