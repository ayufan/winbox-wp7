﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using RouterOS.API;

namespace RouterOS.API
{
    public class UserControl : System.Windows.Controls.UserControl
    {
        protected Connection API { get; private set; }
        protected DeviceInfo Info { get; private set; }

        protected Page Frame
        {
            get { return CommonLib.ClassExtensions.GetFrame(this); }
        }        

        public string DeviceName
        {
            get { return Info != null ? Info.ToString() : "API"; }
        }

        public UserControl()
        {
            Loaded += new RoutedEventHandler(APIUserControl_Loaded);
            Unloaded += new RoutedEventHandler(APIUserControl_Unloaded);
        }

        protected virtual void StartUpdate()
        {
        }

        protected virtual void PauseUpdate()
        {
        }

        private void APIUserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            PauseUpdate();
        }

        private void APIUserControl_Loaded(object sender, RoutedEventArgs e)
        {
            string uid;
            if (Frame != null && Frame.NavigationContext != null &&
                Frame.NavigationContext.QueryString.TryGetValue("uid", out uid))
            {
                Info = DeviceCollection.Devices.Find(new Guid(uid));

                if (Info == null || Info.API == null)
                {
                    return;
                }

                API = Info.API;
            }

            if (API != null && API.IsReady)
            {
                StartUpdate();
            }
        }

        public void ErrorOccurred(RouterOS.API.Record response)
        {
            Deployment.Current.Dispatcher.BeginInvoke(() => { MessageBox.Show(response.Contains("message") ? response["message"] : "error occurred", "Error", MessageBoxButton.OK); });
        }

        public void CheckResult(RouterOS.API.Connection conn, RouterOS.API.Record response)
        {
            if (response.Type == RouterOS.API.RecordType.Error)
            {
                Deployment.Current.Dispatcher.BeginInvoke(() => { MessageBox.Show(response.Contains("message") ? response["message"] : "error occurred", "Error", MessageBoxButton.OK); });
            }
        }
    }
}
