﻿using System;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Phone.Controls;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.GamerServices;
using RouterOS.API;
using RouterOS.Serialization;
using CommonLib;

namespace RouterOS.API
{
    public class ItemApplicationPage : BaseApplicationPage
    {
        public void EditItem(object sender, RoutedEventArgs e)
        {
            RosItemObject info = sender.GetTag<RosItemObject>();
            if (info == null)
                return;
        }

        public void RemoveItem(object sender, RoutedEventArgs e)
        {
            if (App.IsTrial)
            {
                App.TrialMessage("TRIAL doesn't allow to remove items.");
                return;
            }

            RosItemObject info = sender.GetTag<RosItemObject>();
            if (info == null)
                return;

            if (MessageBox.Show("Are you sure that you want to REMOVE?", info.ToString(), MessageBoxButton.OKCancel) == MessageBoxResult.OK)
            {
                try
                {
                    info.Remove(API, this.CheckResult);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK);
                }
            }
        }

        public void CommentItem(object sender, RoutedEventArgs e)
        {
            RosItemObject info = sender.GetTag<RosItemObject>();
            if (info == null)
                return;

            Guide.BeginShowKeyboardInput(PlayerIndex.One,
                info.ToString(), "Enter new comment:", info.Comment, this.EndCommentItem, info);
        }

        private void EndCommentItem(IAsyncResult result)
        {
            string newComment = Guide.EndShowKeyboardInput(result);

            RosItemObject info = result.AsyncState as RosItemObject;
            if (info == null)
                return;

            info.SetComment(API, newComment, this.CheckResult);
        }

        public void EnableItem(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (App.IsTrial)
            {
                App.TrialMessage("TRIAL doesn't allow to enable items.");
                return;
            }

            RosItemObject info = sender.GetTag<RosItemObject>();
            if (info == null)
                return;

            if (MessageBox.Show("Are you sure that you want to ENABLE?", info.ToString(), MessageBoxButton.OKCancel) == MessageBoxResult.OK)
            {
                try
                {
                    info.Enable(API, this.CheckResult);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK);
                }
            }
        }

        public void DisableItem(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (App.IsTrial)
            {
                App.TrialMessage("TRIAL doesn't allow to disable items.");
                return;
            }

            RosItemObject info = sender.GetTag<RosItemObject>();
            if (info == null)
                return;

            if (MessageBox.Show("Are you sure that you want to DISABLE?", info.ToString(), MessageBoxButton.OKCancel) == MessageBoxResult.OK)
            {
                try
                {
                    info.Disable(API, this.CheckResult);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK);
                }
            }
        }

        public void EnableOrDisableItem(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (App.IsTrial)
            {
                App.TrialMessage("TRIAL doesn't allow to enable/disable items.");
                return;
            }

            RosItemObject info = sender.GetTag<RosItemObject>();
            if (info == null)
                return;

            if (info.Disabled.GetValueOrDefault())
            {
                if (MessageBox.Show("Are you sure that you want to ENABLE?", info.ToString(), MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                {
                    try
                    {
                        info.Enable(API, this.CheckResult);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK);
                    }
                }
            }
            else
            {
                if (MessageBox.Show("Are you sure that you want to DISABLE?", info.ToString(), MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                {
                    try
                    {
                        info.Disable(API, this.CheckResult);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK);
                    }
                }
            }
        }
    }
}
