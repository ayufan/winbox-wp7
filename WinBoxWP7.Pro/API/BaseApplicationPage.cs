﻿using System;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Phone.Controls;
using RouterOS.API;

namespace RouterOS.API
{
    public class BaseApplicationPage : PhoneApplicationPage
    {
        protected Connection API { get; private set; }
        protected DeviceInfo Info { get; private set; }

        private bool wasNavigatedTo;
        
        protected string DeviceName
        {
            get { return Info != null ? Info.ToString() : "API"; }
        }

        public BaseApplicationPage()
        {
            this.Loaded += APIPhoneApplicationPage_Loaded;
            this.Unloaded += APIPhoneApplicationPage_Unloaded;
        }

        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);

            if (API != null)
            {
                API.Disconnected -= API_Disconnected;
            }
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            string uid;
            if (NavigationContext.QueryString.TryGetValue("uid", out uid))
            {
                Info = DeviceCollection.Devices.Find(new Guid(uid));

                if (Info == null)
                {
                    NavigationService.GoBack();
                    return;
                }

                if (Info.API == null || !Info.API.IsReady)
                {
                    NavigationService.Navigate(new Uri(Pages.DeviceConnectPage.Uri + "?uid=" + Info.Uid.ToString() + "&reconnect=true", UriKind.Relative));
                    return;
                }

                API = Info.API;
                API.Disconnected += API_Disconnected;
            }

            if (!wasNavigatedTo)
            {
                TextBlock blockTitle = FindName("ApplicationTitle") as TextBlock;

                if (blockTitle != null)
                {
                    blockTitle.Text = DeviceName + " // " + blockTitle.Text;
                }

                Pivot pivotTitle = FindName("PivotTitle") as Pivot;

                if (pivotTitle != null)
                {
                    pivotTitle.Title = DeviceName + " // " + pivotTitle.Title;
                }

                wasNavigatedTo = true;
            }
        }

        protected virtual void StartUpdate()
        {
        }

        protected virtual void PauseUpdate()
        {
        }

        void APIPhoneApplicationPage_Unloaded(object sender, RoutedEventArgs e)
        {
            PauseUpdate();
        }

        void APIPhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            if (API != null && API.IsReady)
            {
                StartUpdate();
            }
        }

        void API_Disconnected(Connection conn, Exception ex)
        {
            Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                NavigationService.Navigate(new Uri(Pages.DeviceConnectPage.Uri + "?uid=" + Info.Uid.ToString() + "&reconnect=true", UriKind.Relative));
            });
        }

        public void CheckResult(RouterOS.API.Connection conn, RouterOS.API.Record response)
        {
            if (response.Type == RouterOS.API.RecordType.Error)
            {
                Deployment.Current.Dispatcher.BeginInvoke(() => { MessageBox.Show(response.Contains("message") ? response["message"] : "error occurred", "Error", MessageBoxButton.OK); });
            }
        }
    }
}
