﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Phone.Controls;
using RouterOS.API;
using RouterOS.Serialization;

namespace RouterOS.API
{
    public delegate string ItemMappingFormatter(string name, string recordText);

    public struct ItemMapping
    {
        public string Name;
        public ContentControl View;
        public FrameworkElement Title;
        public FrameworkElement Item;
        public ItemMappingFormatter Formatter;

        public ItemMapping(string name, ContentControl view, FrameworkElement title, FrameworkElement item)
            : this()
        {
            View = view;
            Name = name;
            Title = title;
            Item = item;
        }
        public ItemMapping(string name, FrameworkElement title, FrameworkElement item)
            : this()
        {
            Name = name;
            Title = title;
            Item = item;
        }
        public ItemMapping(string name, TextBlock item)
            : this()
        {
            Name = name;
            Item = item;
        }

        private void MakeVisible(Pivot pivot, PivotItem pivotItem)
        {
            if (pivotItem.Parent == null)
            {
                bool added = false;
                for (int i = 0; i < pivot.Items.Count; ++i)
                {
                    if (pivotItem.TabIndex <= ((PivotItem)pivot.Items[i]).TabIndex)
                    {
                        pivot.Items.Insert(i, pivotItem);
                        added = true;
                        break;
                    }
                }
                if (!added)
                    pivot.Items.Add(pivotItem);
            }
            pivotItem.Visibility = Visibility.Visible;
        }

        public void MakeVisible(Pivot pivot)
        {
            if (View != null)
            {
                if (View is PivotItem)
                    MakeVisible(pivot, (PivotItem)View);
                else
                    View.Visibility = Visibility.Visible;
            }

            if (Title != null)
            {
                Title.Visibility = Visibility.Visible;
            }

            if (Item != null)
            {
                Item.Visibility = Visibility.Visible;
            }
        }

        public void SetContent(object data)
        {
            if (Item == null)
                return;

            string text = null;
            if (Formatter != null)
                text = Formatter(Name, data.ToString());
            else
                text = data.ToString();

            if (Item is TextBlock)
                ((TextBlock)Item).Text = text;
            else if (Item is TextBox)
                ((TextBox)Item).Text = text;
        }
    }

    public class MappedApplicationPage : BaseApplicationPage
    {
        private List<ItemMapping> ItemMappings = new List<ItemMapping>();

        public MappedApplicationPage()
        {
        }

        public void EnableEdit(object sender, EventArgs e)
        {
            foreach (ItemMapping item in ItemMappings)
            {
                if (item.Item is TextBox)
                    ((TextBox)item.Item).IsEnabled = true;
            }
        }

        public void CancelEdit(object sender, EventArgs e)
        {
            foreach (ItemMapping item in ItemMappings)
            {
                if (item.Item is TextBox)
                    ((TextBox)item.Item).IsEnabled = false;
            }
        }

        public void SaveEdit(object sender, EventArgs e)
        {
            foreach (ItemMapping item in ItemMappings)
            {
                if (item.Item is TextBox)
                    ((TextBox)item.Item).IsEnabled = false;
            }
        }

        protected void AddMapping(string name, FrameworkElement item)
        {
            AddMapping(name, (PanoramaItem)null, null, item);
        }

        protected void AddMapping(string name, FrameworkElement title, FrameworkElement item)
        {
            AddMapping(name, (PanoramaItem)null, title, item);
        }

        protected void AddMapping(string name, ContentControl view, FrameworkElement title, FrameworkElement item)
        {
            if (name != null)
            {
                ItemMapping mapping = new ItemMapping(name, view, title, item);
                ItemMappings.Add(mapping);
            }

            if (view != null)
            {
                if (name != null)
                {
                    view.Visibility = Visibility.Collapsed;
                }
            }
            if (title != null)
            {
                if (name != null)
                {
                    title.Visibility = Visibility.Collapsed;
                }

                if (title is TextBlock)
                {
                    Thickness margin = title.Margin;
                    margin.Top = 20;
                    title.Margin = margin;
                }
            }
            if (item != null)
            {
                if (name != null)
                {
                    item.Visibility = Visibility.Collapsed;
                }

                if (item is TextBox)
                {
                    ((TextBox)item).Width = 440;
                    ((TextBox)item).IsEnabled = false;
                }
            }
        }

        protected void MakeVisible(PivotItem pivotItem)
        {
            if (pivotItem.Parent == null)
            {
                Pivot pivot = (Pivot)FindName("PanoramaView");
                bool added = false;
                for (int i = 0; i < pivot.Items.Count; ++i)
                {
                    if (pivotItem.TabIndex <= ((PivotItem)pivot.Items[i]).TabIndex)
                    {
                        pivot.Items.Insert(i, pivotItem);
                        added = true;
                        break;
                    }
                }
                if (!added)
                    pivot.Items.Add(pivotItem);
            }
            pivotItem.Visibility = Visibility.Visible;
        }

        protected void EnableKeys(IEnumerable<string> keys)
        {
            Pivot pivot = (Pivot)FindName("PanoramaView");

            List<string> keyList = new List<string>(keys);
            keyList.Sort();

            foreach (ItemMapping mapping in ItemMappings)
            {
                if (keyList.BinarySearch(mapping.Name) >= 0)
                {
                    mapping.MakeVisible(pivot);
                }
            }
        }

        protected void ProcessObject(RosObject record)
        {
            Pivot pivot = (Pivot)FindName("PanoramaView");

            Dictionary<string, RouterOS.Serialization.RosValueMember> members = new Dictionary<string, RosValueMember>();
            foreach (var member in record.GetMembers())
                members.Add(member.Name, member);

            foreach (ItemMapping mapping in ItemMappings)
            {
                if (!members.ContainsKey(mapping.Name))
                    continue;

                object value = members[mapping.Name].GetValue(record);
                mapping.MakeVisible(pivot);
                mapping.SetContent(value);
            }
        }

        protected void ProcessRecord(Record record)
        {
            Pivot pivot = (Pivot)FindName("PanoramaView");

            foreach (ItemMapping mapping in ItemMappings)
            {
                if (record.Contains(mapping.Name))
                {
                    mapping.MakeVisible(pivot);
                    mapping.SetContent(record[mapping.Name]);
                }
            }
        }
    }
}
