﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using RouterOS.Serialization;

namespace RouterOS.Views
{
    public partial class IpDhcpNetworkListView : RouterOS.API.ItemUserControl
    {
        private RosItemCollection<RouterOS.IP.Dhcp.Network> networks;

        public IpDhcpNetworkListView()
        {
            InitializeComponent();
        }

        ~IpDhcpNetworkListView()
        {
            if (networks != null)
            {
                networks.Dispose();
                networks = null;
            }
        }

        protected override void StartUpdate()
        {
            base.StartUpdate();

            if (networks == null)
            {
                networks = new RosItemCollection<RouterOS.IP.Dhcp.Network>(API);
                networks.Completed += () => { NetworkListProgress.IsIndeterminate = false; };
                NetworkList.ItemsSource = networks.Items;
            }
            
            networks.Start();
        }

        protected override void PauseUpdate()
        {
            base.PauseUpdate();

            if (networks != null)
            {
                networks.Stop();
            }
        }

        public new void RemoveItem(object sender, RoutedEventArgs e)
        {
            base.RemoveItem(sender, e);
        }

        public new void CommentItem(object sender, RoutedEventArgs e)
        {
            base.CommentItem(sender, e);
        }

        public new void EnableItem(object sender, System.Windows.Input.GestureEventArgs e)
        {
            base.EnableItem(sender, e);
        }

        public new void DisableItem(object sender, System.Windows.Input.GestureEventArgs e)
        {
            base.DisableItem(sender, e);
        }
    }
}
