﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using RouterOS.Serialization;

namespace RouterOS.Views
{
	public partial class IpAddressListView : RouterOS.API.ItemUserControl
    {
        private RosItemCollection<RouterOS.IP.Address> addresses;

		public IpAddressListView()
		{
			InitializeComponent();
		}

        ~IpAddressListView()
        {
            if (addresses != null)
            {
                addresses.Dispose();
                addresses = null;
            }
        }

        protected override void StartUpdate()
        {
            base.StartUpdate();

            if (addresses == null)
            {
                addresses = new RosItemCollection<RouterOS.IP.Address>(API);
                addresses.Completed += () => { AddressListProgress.IsIndeterminate = false; };
                AddressList.ItemsSource = addresses.Items;
            }
            
            addresses.Start();
        }

        protected override void PauseUpdate()
        {
            base.PauseUpdate();

            if (addresses != null)
            {
                addresses.Stop();
            }
        }

        public new void RemoveItem(object sender, RoutedEventArgs e)
        {
            base.RemoveItem(sender, e);
        }

        public new void CommentItem(object sender, RoutedEventArgs e)
        {
            base.CommentItem(sender, e);
        }

        public new void EnableItem(object sender, System.Windows.Input.GestureEventArgs e)
        {
            base.EnableItem(sender, e);
        }

        public new void DisableItem(object sender, System.Windows.Input.GestureEventArgs e)
        {
            base.DisableItem(sender, e);
        }
	}
}