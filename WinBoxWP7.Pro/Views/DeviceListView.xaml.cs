﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using CommonLib;

namespace RouterOS.Views
{
    public partial class DeviceListView : UserControl
    {
        public DeviceCollection Devices
        {
            get { return DeviceCollection.Devices; }
        }

        public DeviceListView()
        {
            InitializeComponent();

            Loaded += DeviceListControl_Loaded;
        }

        void DeviceListControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.DataContext = this.DataContext;
        }

        void ConnectToDevice(object sender, System.Windows.Input.GestureEventArgs e)
        {
            DeviceInfo info = sender.GetTag<DeviceInfo>();
            if (info == null)
                return;

            this.GetFrame().NavigationService.Navigate(new Uri(Pages.DeviceConnectPage.Uri + "?uid=" + info.Uid.ToString(), UriKind.Relative));
        }

        void DisconnectFromDevice(object sender, System.Windows.Input.GestureEventArgs e)
        {
            DeviceInfo info = sender.GetTag<DeviceInfo>();
            if (info == null)
                return;

            lock (info)
            {
                if (info.API == null)
                    return;
                info.API.Disconnect();
                info.API = null;
                DeviceCollection.Devices.Update(info);
            }
        }

        private void DeviceEdit(object sender, GestureEventArgs e)
        {
            DeviceInfo info = sender.GetTag<DeviceInfo>();
            if (info == null)
                return;
            this.GetFrame().NavigationService.Navigate(new Uri(Pages.DeviceEditPage.Uri + "?uid=" + info.Uid.ToString(), UriKind.Relative));
        }
    }
}
