﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using RouterOS.Serialization;

namespace RouterOS.Views
{
    public partial class IpDhcpServerListView : RouterOS.API.ItemUserControl
    {
        private RosItemCollection<RouterOS.IP.Dhcp.Server> servers;

        public IpDhcpServerListView()
        {
            InitializeComponent();
        }

        ~IpDhcpServerListView()
        {
            if (servers != null)
            {
                servers.Dispose();
                servers = null;
            }
        }

        protected override void StartUpdate()
        {
            base.StartUpdate();

            if (servers == null)
            {
                servers = new RosItemCollection<RouterOS.IP.Dhcp.Server>(API);
                servers.Completed += () => { ServerListProgress.IsIndeterminate = false; };
                ServerList.ItemsSource = servers.Items;
            }
            
            servers.Start();
        }

        protected override void PauseUpdate()
        {
            base.PauseUpdate();

            if (servers != null)
            {
                servers.Stop();
            }
        }

        public new void RemoveItem(object sender, RoutedEventArgs e)
        {
            base.RemoveItem(sender, e);
        }

        public new void CommentItem(object sender, RoutedEventArgs e)
        {
            base.CommentItem(sender, e);
        }

        public new void EnableItem(object sender, System.Windows.Input.GestureEventArgs e)
        {
            base.EnableItem(sender, e);
        }

        public new void DisableItem(object sender, System.Windows.Input.GestureEventArgs e)
        {
            base.DisableItem(sender, e);
        }
    }
}
