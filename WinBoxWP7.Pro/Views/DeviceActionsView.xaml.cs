﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using RouterOS.API;

namespace RouterOS.Views
{
    public partial class DeviceActionsView : RouterOS.API.UserControl
    {
        public DeviceActionsView()
        {
            InitializeComponent();
        }

        private void RestoreButton_Tap(object sender, GestureEventArgs e)
        {
            Frame.NavigationService.Navigate(new Uri(Pages.FileRestorePickerPage.Uri + "?uid=" + Info.Uid.ToString(), UriKind.Relative));
        }

        private void BackupButton_Tap(object sender, GestureEventArgs e)
        {
            if (MessageBox.Show("Do you want to create backup of router?", "BACKUP", MessageBoxButton.OKCancel) != MessageBoxResult.OK)
                return;

            CustomCommand cmd = new CustomCommand()
            {
                Scope = new string[] { "system", "backup", "save" }
            };

            API.ExecuteCommand(cmd, this.CheckResult);
        }

        private void RebootButton_Tap(object sender, GestureEventArgs e)
        {
            if (MessageBox.Show("Do you want to reboot router?", "REBOOT", MessageBoxButton.OKCancel) != MessageBoxResult.OK)
                return;

            CustomCommand cmd = new CustomCommand()
            {
                Scope = new string[] { "system", "reboot" }
            };

            API.ExecuteCommand(cmd, this.CheckResult);
        }

        private void ShutdownButton_Tap(object sender, GestureEventArgs e)
        {
            if (MessageBox.Show("Do you want to shutdown router?", "SHUTDOWN", MessageBoxButton.OKCancel) != MessageBoxResult.OK)
                return;

            CustomCommand cmd = new CustomCommand()
            {
                Scope = new string[] { "system", "shutdown" }
            };

            API.ExecuteCommand(cmd, this.CheckResult);
        }

        private void ScanButton_Tap(object sender, GestureEventArgs e)
        {
            Frame.NavigationService.Navigate(new Uri(Pages.WirelessScanPage.Uri + "?uid=" + Info.Uid.ToString(), UriKind.Relative));
        }

        private void PingButton_Tap(object sender, GestureEventArgs e)
        {
            Frame.NavigationService.Navigate(new Uri(Pages.PingPage.Uri + "?uid=" + Info.Uid.ToString(), UriKind.Relative));
        }

        private void TraceButton_Tap(object sender, GestureEventArgs e)
        {
            Frame.NavigationService.Navigate(new Uri(Pages.TracePage.Uri + "?uid=" + Info.Uid.ToString(), UriKind.Relative));
        }
    }
}
