﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using RouterOS.Serialization;

namespace RouterOS.Views
{
    public partial class IpDhcpLeaseListView : RouterOS.API.ItemUserControl
    {
        public static readonly GenericView View = new GenericView(typeof(IpDhcpLeaseListView), "leases");

        private RosItemCollection<RouterOS.IP.Dhcp.Lease> leases;

        public IpDhcpLeaseListView()
        {
            InitializeComponent();
        }

        ~IpDhcpLeaseListView()
        {
            if (leases != null)
            {
                leases.Dispose();
                leases = null;
            }
        }

        protected override void StartUpdate()
        {
            base.StartUpdate();

            if (leases == null)
            {
                leases = new RosItemCollection<RouterOS.IP.Dhcp.Lease>(API);
                leases.Updated += (item) =>
                {
                    NoFound.Visibility = System.Windows.Visibility.Collapsed;
                };
                leases.Completed += () =>
                {
                    LeaseListProgress.IsIndeterminate = false;

                    if (leases.Items.Count == 0)
                        NoFound.Visibility = System.Windows.Visibility.Visible;
                };
                LeaseList.ItemsSource = leases.Items;
            }

            leases.Start();
        }

        protected override void PauseUpdate()
        {
            base.PauseUpdate();

            if (leases != null)
            {
                leases.Stop();
            }
        }

        public new void RemoveItem(object sender, RoutedEventArgs e)
        {
            base.RemoveItem(sender, e);
        }

        public new void CommentItem(object sender, RoutedEventArgs e)
        {
            base.CommentItem(sender, e);
        }

        public new void EnableItem(object sender, System.Windows.Input.GestureEventArgs e)
        {
            base.EnableItem(sender, e);
        }

        public new void DisableItem(object sender, System.Windows.Input.GestureEventArgs e)
        {
            base.DisableItem(sender, e);
        }
    }
}
