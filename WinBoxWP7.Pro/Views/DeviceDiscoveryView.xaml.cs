﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Net.Sockets;
using System.Threading;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Net.NetworkInformation;
using Microsoft.Phone.Tasks;
using CommonLib;

namespace RouterOS.Views
{
    public partial class DeviceDiscoveryView : UserControl
    {
        private DiscoveredDeviceInfoCollection discoveredDevices = new DiscoveredDeviceInfoCollection();
        private Socket discoveryBroadcaster;
        private UdpAnySourceMulticastClient discoveryListener;
        private IAsyncResult discoveryJoiner, discoveryReceiver;
        private Timer discoveryTimer;
        private byte[] discoveryBuffer = new byte[256];

        public DiscoveredDeviceInfoCollection DiscoveredDevices
        {
            get { return discoveredDevices; }
        }

        public DeviceDiscoveryView()
        {
            InitializeComponent();

            Loaded += DeviceDiscoveryView_Loaded;
            Unloaded += DeviceDiscoveryView_Unloaded;
        }

        void DeviceDiscoveryView_Loaded(object sender, RoutedEventArgs e)
        {
            this.DataContext = this.DataContext;

            if (EnableDiscover.IsChecked.GetValueOrDefault())
                StartDiscovery();

            foreach (DiscoveredDeviceInfo info in DiscoveredDevices.ToArray())
                DiscoveredDevices.Update(info);
        }

        void DeviceDiscoveryView_Unloaded(object sender, RoutedEventArgs e)
        {
            StopDiscovery();
        }

        private void EnableDiscover_Checked(object sender, RoutedEventArgs e)
        {
            StartDiscovery();
        }

        private void EnableDiscover_Unchecked(object sender, RoutedEventArgs e)
        {
            StopDiscovery();
        }

        private void StartDiscovery()
        {
            if (discoveryBroadcaster != null)
                return;

            if (discoveryTimer == null)
                discoveryTimer = new Timer(this.SendDiscovery, null, 50, 2000);

            if(ProgressBar != null)
                ProgressBar.IsIndeterminate = true;
        }

        private void StopDiscovery()
        {
            if(ProgressBar != null)
                ProgressBar.IsIndeterminate = false;

            if (discoveryBroadcaster != null)
            {
                discoveryBroadcaster.Close();
                discoveryBroadcaster.Dispose();
                discoveryBroadcaster = null;
            }

            if (discoveryTimer != null)
            {
                discoveryTimer.Dispose();
                discoveryTimer = null;
            }

            Deployment.Current.Dispatcher.BeginInvoke(() => { DiscoveryRequireWLAN.Visibility = Visibility.Collapsed; });
        }

        private void StartDiscoveryReceive()
        {
            if (discoveryListener == null)
            {
                discoveryListener = new UdpAnySourceMulticastClient(IPAddress.Parse("239.255.2.2"), 5678);
                discoveryJoiner = discoveryListener.BeginJoinGroup(this.DiscoveryJoined, null);
                if (discoveryJoiner.IsCompleted)
                    discoveryJoiner = null;
                else
                    return;
            }

            if (discoveryJoiner != null)
                return;

            while (discoveryReceiver == null)
            {
                discoveryReceiver = discoveryListener.BeginReceiveFromGroup(discoveryBuffer, 0, discoveryBuffer.Length, this.DiscoveryReceiveCompleted, null);
                if (discoveryReceiver.IsCompleted)
                    discoveryReceiver = null;
            }
        }

        private void DiscoveryJoined(IAsyncResult result)
        {
            if(discoveryListener == null)
                return;

            discoveryListener.EndJoinGroup(result);

            if(discoveryJoiner == result)
                discoveryJoiner = null;

            StartDiscoveryReceive();
        }

        private void DiscoveryReceiveCompleted(IAsyncResult result)
        {
            if (discoveryListener == null)
                return;

            IPEndPoint ep;
            int bytes = discoveryListener.EndReceiveFromGroup(result, out ep);

            if (discoveryReceiver == result)
                discoveryReceiver = null;

            System.Diagnostics.Debug.WriteLine("[DISCOVERY] Received!");

            byte[] buffer = discoveryBuffer.Take(bytes).ToArray();

            Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                DiscoveredDeviceInfo device = DiscoveredDevices.Find(ep.Address);

                if (device == null)
                {
                    device = new DiscoveredDeviceInfo();
                    device.IpAddress = ep.Address;
                }

                if (device.Parse(buffer))
                    DiscoveredDevices.Update(device);
            });

            StartDiscoveryReceive();
        }

        private void DiscoveryConnected(object sender, SocketAsyncEventArgs e)
        {
            if (discoveryBroadcaster == null)
                return;

            if (e.SocketError != SocketError.Success)
            {
                Deployment.Current.Dispatcher.BeginInvoke(() => { DiscoveryRequireWLAN.Visibility = Visibility.Visible; });

                discoveryBroadcaster.Dispose();
                discoveryBroadcaster = null;
                return;
            }

            Deployment.Current.Dispatcher.BeginInvoke(() => { DiscoveryRequireWLAN.Visibility = Visibility.Collapsed; });

            StartDiscoveryReceive();
        }

        private void SendDiscovery(object state)
        {
            if(!DeviceNetworkInformation.IsNetworkAvailable)
                return;

            if (discoveryBroadcaster == null)
            {
                discoveryBroadcaster = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                discoveryBroadcaster.SetNetworkRequirement(NetworkSelectionCharacteristics.NonCellular);
                SocketAsyncEventArgs connectEvent = new SocketAsyncEventArgs();
                connectEvent.Completed += this.DiscoveryConnected;
                connectEvent.RemoteEndPoint = new IPEndPoint(IPAddress.Broadcast, 5678);
                discoveryBroadcaster.ConnectAsync(connectEvent);
                return;
            }
            else if (!discoveryBroadcaster.Connected)
            {
                return;
            }

            byte[] tmp = new byte[4];

            SocketAsyncEventArgs sendEvent = new SocketAsyncEventArgs();
            sendEvent.SetBuffer(tmp, 0, tmp.Length);
            sendEvent.RemoteEndPoint = new IPEndPoint(IPAddress.Broadcast, 5678);
            discoveryBroadcaster.SendToAsync(sendEvent);
        }

        private void Settings_Click(object sender, RoutedEventArgs e)
        {
            new ConnectionSettingsTask()
            {
                ConnectionSettingsType = ConnectionSettingsType.WiFi
            }.Show();
        }

        private void ConnectToDiscoveredDevice(object sender, RoutedEventArgs e)
        {
            DiscoveredDeviceInfo info = sender.GetTag<DiscoveredDeviceInfo>();
            if (info == null)
                return;
            DeviceInfo info2 = info.DeviceInfo;
            if (info2 == null)
                return;

            this.GetFrame().NavigationService.Navigate(new Uri(Pages.DeviceConnectPage.Uri + "?uid=" + info2.Uid.ToString(), UriKind.Relative));
        }

        private void AddDiscoveredDevice(object sender, RoutedEventArgs e)
        {
            DiscoveredDeviceInfo device = sender.GetTag<DiscoveredDeviceInfo>();
            if (device == null)
                return;
            this.GetFrame().NavigationService.Navigate(new Uri(Pages.DeviceEditPage.Uri +
                "?name=" + HttpUtility.UrlEncode(device.Identity) +
                "&host=" + HttpUtility.UrlEncode(device.IpAddress != null ? device.IpAddress.ToString() : "") +
                "&mac=" + HttpUtility.UrlEncode(device.MacAddress.ToString()), UriKind.Relative));
        }

        private void DiscoveredDevice_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            DiscoveredDeviceInfo device = sender.GetTag<DiscoveredDeviceInfo>();
            if (device == null)
                return;

            if (device.IsNew)
            {
                this.GetFrame().NavigationService.Navigate(new Uri(Pages.DeviceEditPage.Uri +
                    "?name=" + HttpUtility.UrlEncode(device.Identity) +
                    "&host=" + HttpUtility.UrlEncode(device.IpAddress != null ? device.IpAddress.ToString() : "") +
                    "&mac=" + HttpUtility.UrlEncode(device.MacAddress.ToString()), UriKind.Relative));
            }
            else
            {
                this.GetFrame().NavigationService.Navigate(new Uri(Pages.DeviceEditPage.Uri +
                    "?uid=" + device.DeviceInfo.Uid.ToString(), UriKind.Relative));
            }
        }
    }
}
