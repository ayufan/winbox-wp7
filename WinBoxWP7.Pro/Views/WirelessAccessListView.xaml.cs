﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using RouterOS.Serialization;

namespace RouterOS.Views
{
    public partial class WirelessAccessListView : RouterOS.API.ItemUserControl
    {
        private RosItemCollection<RouterOS.Wireless.AccessList> accessLists;

        public WirelessAccessListView()
        {
            InitializeComponent();
        }

        ~WirelessAccessListView()
        {
            if (accessLists != null)
            {
                accessLists.Dispose();
                accessLists = null;
            }
        }

        protected override void StartUpdate()
        {
            base.StartUpdate();

            if (accessLists == null)
            {
                accessLists = new RosItemCollection<RouterOS.Wireless.AccessList>(API);
                accessLists.Completed += () => { Progress.IsIndeterminate = false; };
                List.ItemsSource = accessLists.Items;
            }

            accessLists.Start();
        }

        protected override void PauseUpdate()
        {
            base.PauseUpdate();

            if (accessLists != null)
            {
                accessLists.Stop();
            }
        }

        public new void RemoveItem(object sender, RoutedEventArgs e)
        {
            base.RemoveItem(sender, e);
        }

        public new void CommentItem(object sender, RoutedEventArgs e)
        {
            base.CommentItem(sender, e);
        }

        public new void EnableItem(object sender, System.Windows.Input.GestureEventArgs e)
        {
            base.EnableItem(sender, e);
        }

        public new void DisableItem(object sender, System.Windows.Input.GestureEventArgs e)
        {
            base.DisableItem(sender, e);
        }
    }
}
