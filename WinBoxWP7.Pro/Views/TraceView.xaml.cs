﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using RouterOS.Serialization;
using RouterOS.API;
using Coding4Fun.Phone.Controls;

namespace RouterOS.Views
{
    public partial class TraceView : RouterOS.API.UserControl
    {
        public static readonly GenericView View = new GenericView(typeof(TraceView), "trace");

        private RosItemStats<Tools.Trace> responses;
        private RosItemCollection<Interface> interfaces;

        public TraceView()
        {
            InitializeComponent();
        }

        ~TraceView()
        {
            if (interfaces != null)
            {
                interfaces.Dispose();
                interfaces = null;
            }

            if (responses != null)
            {
                responses.Dispose();
                responses = null;
            }
        }

        protected override void StartUpdate()
        {
            base.StartUpdate();

            if (interfaces == null)
            {
                interfaces = new RosItemCollection<Interface>(API);
                interfaces.Items.Add(new Interface() { Name = "all" });
                InterfaceList.ItemsSource = interfaces.Items;
            }
            
            interfaces.Start();
        }

        protected override void PauseUpdate()
        {
            base.PauseUpdate();

            if (interfaces != null)
            {
                interfaces.Stop();
            }
        }

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            if (responses == null)
            {
                // fill ping options
                Tools.TraceOptions options = new Tools.TraceOptions();
                options.Address = Address.AddressName;
                options.Interface = InterfaceList.SelectedItem as Interface;

                if (string.IsNullOrEmpty(options.Address))
                {
                    MessageBox.Show("Fill the address.");
                    return;
                }

                Address.SaveAddress();

                if (options.Interface.ToString() == "all")
                    options.Interface = null;

                // send ping command
                responses = new RosItemStats<Tools.Trace>(API, options);
                responses.Completed += TraceCompleted;
                responses.Start();

                // attach resources
                TraceActive.IsIndeterminate = true;
                TraceItems.ItemsSource = responses.Items;
            }
        }

        private void Stop_Click(object sender, RoutedEventArgs e)
        {
            TraceCompleted();
        }

        private void ShowAdvanced_Click(object sender, RoutedEventArgs e)
        {
            AdvancedOptions.IsExpanded = true;
        }

        private void HideAdvanced_Click(object sender, RoutedEventArgs e)
        {
            AdvancedOptions.IsExpanded = false;
        }

        void TraceCompleted()
        {
            if (responses != null)
            {
                responses.Completed -= TraceCompleted;
                responses.ErrorOccurred -= ErrorOccurred;
                responses.Dispose();
                responses = null;
            }

            TraceActive.IsIndeterminate = false;
        }
    }
}
