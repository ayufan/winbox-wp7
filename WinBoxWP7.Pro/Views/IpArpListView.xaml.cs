﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using RouterOS.Serialization;

namespace RouterOS.Views
{
    public partial class IpArpListView : RouterOS.API.ItemUserControl
    {
        private RosItemCollection<RouterOS.IP.ARP> arps;

		public IpArpListView()
		{
			InitializeComponent();
		}

        ~IpArpListView()
        {
            if (arps != null)
            {
                arps.Dispose();
                arps = null;
            }
        }

        protected override void StartUpdate()
        {
            base.StartUpdate();

            if (arps == null)
            {
                arps = new RosItemCollection<RouterOS.IP.ARP>(API);
                arps.Completed += () => { ArpListProgress.IsIndeterminate = false; };
                ArpList.ItemsSource = arps.Items;
            }
            
            arps.Start();
        }

        protected override void PauseUpdate()
        {
            base.PauseUpdate();

            if (arps != null)
            {
                arps.Stop();
            }
        }

        public new void RemoveItem(object sender, RoutedEventArgs e)
        {
            base.RemoveItem(sender, e);
        }

        public new void CommentItem(object sender, RoutedEventArgs e)
        {
            base.CommentItem(sender, e);
        }

        public new void EnableItem(object sender, System.Windows.Input.GestureEventArgs e)
        {
            base.EnableItem(sender, e);
        }

        public new void DisableItem(object sender, System.Windows.Input.GestureEventArgs e)
        {
            base.DisableItem(sender, e);
        }
    }
}
