﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using RouterOS.Serialization;

namespace RouterOS.Views
{
    public partial class ResourceView : RouterOS.API.UserControl
    {
        private RosValueUpdater<Tools.Identity> identity;
        private RosValueUpdater<Tools.Resource> resource;

        public event RosValueUpdater<Tools.Identity>.ValueUpdated IdentityUpdated;
        public event RosValueUpdater<Tools.Resource>.ValueUpdated ResourceUpdated;

        public Tools.Identity Identity
        {
            get { return identity != null ? identity.Value : null; }
        }

        public Tools.Resource Resource
        {
            get { return resource != null ? resource.Value : null; }
        }

        public ResourceView()
        {
            InitializeComponent();
        }

        ~ResourceView()
        {
            if (resource != null)
            {
                resource.Dispose();
                resource = null;
            }

            if (identity != null)
            {
                identity.Dispose();
                identity = null;
            }
        }

        protected override void StartUpdate()
        {
            base.StartUpdate();

            if (resource == null)
            {
                resource = new RosValueUpdater<Tools.Resource>(API);
                resource.Updated += resource_Updated;
            }

            resource.Start();

            if (identity == null)
            {
                identity = new RosValueUpdater<Tools.Identity>(API);
                identity.Updated += identity_Updated;
            }

            identity.Start();
        }

        protected override void PauseUpdate()
        {
            base.PauseUpdate();

            if (resource != null)
            {
                resource.Stop();
            }

            if (identity != null)
            {
                identity.Stop();
            }
        }

        void identity_Updated(Tools.Identity value)
        {
            IdentityText.Text = value.Name;

            if (IdentityUpdated != null)
                IdentityUpdated(value);
        }

        void resource_Updated(Tools.Resource value)
        {
            Board.Text = value.BoardName;
            Uptime.Text = value.Uptime.GetValueOrDefault().ToString();
            Version.Text = value.Version != null ? value.Version.ToString() : "";
            BadBlocks.Text = value.BadBlocks.ToString();

            CPU.Text = value.CpuLoad.ToString() + "%";
            Memory.Text = value.MemUsagePercent.ToString() + "%";
            Disk.Text = value.DiskUsagePercent.ToString() + "%";

            if (ResourceUpdated != null)
                ResourceUpdated(value);
        }
    }
}
