﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using RouterOS.Serialization;

namespace RouterOS.Views
{
    public partial class IpRouteListView : RouterOS.API.ItemUserControl
    {
        private RosItemCollection<RouterOS.IP.Route> routes;

        public IpRouteListView()
        {
            InitializeComponent();
        }

        ~IpRouteListView()
        {
            if (routes != null)
            {
                routes.Dispose();
                routes = null;
            }
        }

        protected override void StartUpdate()
        {
            base.StartUpdate();

            if (routes == null)
            {
                routes = new RosItemCollection<RouterOS.IP.Route>(API);
                routes.Completed += () => { RouteListProgress.IsIndeterminate = false; };
                RouteList.ItemsSource = routes.Items;
            }

            routes.Start();
        }

        protected override void PauseUpdate()
        {
            base.PauseUpdate();

            if (routes != null)
            {
                routes.Stop();
            }
        }

        public new void RemoveItem(object sender, RoutedEventArgs e)
        {
            base.RemoveItem(sender, e);
        }

        public new void CommentItem(object sender, RoutedEventArgs e)
        {
            base.CommentItem(sender, e);
        }

        public new void EnableItem(object sender, System.Windows.Input.GestureEventArgs e)
        {
            base.EnableItem(sender, e);
        }

        public new void DisableItem(object sender, System.Windows.Input.GestureEventArgs e)
        {
            base.DisableItem(sender, e);
        }
    }
}
