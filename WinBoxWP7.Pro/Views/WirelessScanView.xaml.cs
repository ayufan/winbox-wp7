﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using RouterOS.Serialization;
using RouterOS.Wireless;
using RouterOS.API;
using Coding4Fun.Phone.Controls;
using CommonLib;

namespace RouterOS.Views
{
    public partial class WirelessScanView : RouterOS.API.UserControl
    {
        public static readonly GenericView View = new GenericView(typeof(WirelessScanView), "scan");

        public class FrequencyInfo
        {
            public int Frequency { get; set; }

            public FastObservableCollection<Scan> Devices { get; set; }

            public FrequencyInfo(int freq)
            {
                Frequency = freq;
                Devices = new FastObservableCollection<Scan>();
            }
        }

        public class FrequencyCollection : ObservableCollection<FrequencyInfo>
        {
            public UpdatableObservableCollection<Scan> GetFrequency(int freq)
            {
                FrequencyInfo info = null;

                for (int i = 0; i < Count; ++i)
                {
                    int curFreq = this[i].Frequency;
                    if (curFreq == freq)
                        return this[i].Devices;

                    if (curFreq > freq)
                    {
                        info = new FrequencyInfo(freq);
                        Insert(i, info);
                        break;
                    }
                }

                if (info == null)
                {
                    info = new FrequencyInfo(freq);
                    Add(info);
                }
                return info.Devices;
            }
        }

        private RosItemStats<Scan> results;
        private RosItemCollection<WirelessInterface> interfaces;
        private FrequencyCollection frequencies;

        public WirelessScanView()
        {
            InitializeComponent();
        }

        ~WirelessScanView()
        {
            if (interfaces != null)
            {
                interfaces.Dispose();
                interfaces = null;
            }

            if (results != null)
            {
                results.Dispose();
                results = null;
            }
        }

        protected override void StartUpdate()
        {
            base.StartUpdate();

            if (interfaces == null)
            {
                interfaces = new RosItemCollection<WirelessInterface>(API, new NotQuery(new EqualQuery("interface-type", "virtual-AP")));
                interfaces.Updated += InterfacesUpdated;
                interfaces.Completed += InterfacesCompleted;
                ScanInterface.ItemsSource = interfaces.Items;
            }
            
            interfaces.Start();
        }

        void InterfacesUpdated(WirelessInterface item)
        {
            NotFound.Visibility = System.Windows.Visibility.Collapsed;
        }

        void InterfacesCompleted()
        {
            if (interfaces.Items.Count == 0)
            {
                NotFound.Visibility = System.Windows.Visibility.Visible;
            }
        }

        protected override void PauseUpdate()
        {
            base.PauseUpdate();

            if (interfaces != null)
            {
                interfaces.Stop();
            }
        }

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            if (results == null)
            {
                // fill ping options
                ScanOptions options = new ScanOptions();
                options.Interface = ScanInterface.SelectedItem as WirelessInterface;
                options.Duration = TimeSpan.FromSeconds(10);

                if (options.Interface == null)
                {
                    MessageBox.Show("Select wireless interface");
                    return;
                }

                // send ping command
                results = new RosItemStats<Scan>(API, options);
                results.Updated += ScanResponse;
                results.Completed += ScanCompleted;
                results.Start();

                // attach resources
                frequencies = new FrequencyCollection();
                ScanActive.IsIndeterminate = true;
                ScanItems.ItemsSource = frequencies;
            }
        }

        private void Stop_Click(object sender, RoutedEventArgs e)
        {
            ScanCompleted();
        }

        void ScanResponse(Scan value, Scan lastValue)
        {
            if (value.Frequency == null)
                return;
            if (frequencies == null)
                return;

            var devices = frequencies.GetFrequency(value.Frequency.GetValueOrDefault());
            devices.UpdateByHash(value);
        }

        void ScanCompleted()
        {
            if (results != null)
            {
                results.Updated -= ScanResponse;
                results.Completed -= ScanCompleted;
                results.ErrorOccurred -= ErrorOccurred;
                results.Dispose();
                results = null;
            }

            ScanActive.IsIndeterminate = false;
        }
    }
}
