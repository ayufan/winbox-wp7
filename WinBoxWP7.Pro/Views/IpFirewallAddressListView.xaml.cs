﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using RouterOS.Serialization;

namespace RouterOS.Views
{
    public partial class IpFirewallAddressListView : RouterOS.API.ItemUserControl
    {
        private RosItemCollection<RouterOS.IP.Firewall.AddressList> addressList;

        public IpFirewallAddressListView()
        {
            InitializeComponent();
        }

        ~IpFirewallAddressListView()
        {
            if (addressList != null)
            {
                addressList.Dispose();
                addressList = null;
            }
        }

        protected override void StartUpdate()
        {
            base.StartUpdate();

            if (addressList == null)
            {
                addressList = new RosItemCollection<RouterOS.IP.Firewall.AddressList>(API);
                addressList.Completed += () => { AddressListProgress.IsIndeterminate = false; };
                AddressList.ItemsSource = addressList.Items;
            }

            addressList.Start();
        }

        protected override void PauseUpdate()
        {
            base.PauseUpdate();

            if (addressList != null)
            {
                addressList.Stop();
            }
        }

        public new void RemoveItem(object sender, RoutedEventArgs e)
        {
            base.RemoveItem(sender, e);
        }

        public new void CommentItem(object sender, RoutedEventArgs e)
        {
            base.CommentItem(sender, e);
        }

        public new void EnableItem(object sender, System.Windows.Input.GestureEventArgs e)
        {
            base.EnableItem(sender, e);
        }

        public new void DisableItem(object sender, System.Windows.Input.GestureEventArgs e)
        {
            base.DisableItem(sender, e);
        }
    }
}
