﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using RouterOS.Serialization;

namespace RouterOS.Views
{
    public partial class WirelessConnectListView : RouterOS.API.ItemUserControl
    {
        private RosItemCollection<RouterOS.Wireless.ConnectList> connectLists;

        public WirelessConnectListView()
        {
            InitializeComponent();
        }

        ~WirelessConnectListView()
        {
            if (connectLists != null)
            {
                connectLists.Dispose();
                connectLists = null;
            }
        }

        protected override void StartUpdate()
        {
            base.StartUpdate();

            if (connectLists == null)
            {
                connectLists = new RosItemCollection<RouterOS.Wireless.ConnectList>(API);
                connectLists.Completed += () => { Progress.IsIndeterminate = false; };
                List.ItemsSource = connectLists.Items;
            }

            connectLists.Start();
        }

        protected override void PauseUpdate()
        {
            base.PauseUpdate();

            if (connectLists != null)
            {
                connectLists.Stop();
            }
        }

        public new void RemoveItem(object sender, RoutedEventArgs e)
        {
            base.RemoveItem(sender, e);
        }

        public new void CommentItem(object sender, RoutedEventArgs e)
        {
            base.CommentItem(sender, e);
        }

        public new void EnableItem(object sender, System.Windows.Input.GestureEventArgs e)
        {
            base.EnableItem(sender, e);
        }

        public new void DisableItem(object sender, System.Windows.Input.GestureEventArgs e)
        {
            base.DisableItem(sender, e);
        }
    }
}
