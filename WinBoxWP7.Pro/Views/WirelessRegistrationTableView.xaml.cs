﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using RouterOS.Serialization;
using CommonLib;

namespace RouterOS.Views
{
    public partial class WirelessRegistrationTableView : RouterOS.API.ItemUserControl
    {
        public static readonly GenericView View = new GenericView(typeof(WirelessRegistrationTableView), "registration");

        public class RegistrationInfo
        {
            public string Name { get; set; }

            public FastObservableCollection<Wireless.RegistrationTable> Registrations { get; set; }

            public RegistrationInfo(string name)
            {
                Name = name;
                Registrations = new FastObservableCollection<Wireless.RegistrationTable>();
            }

            public override string ToString()
            {
                return Name;
            }
        }

        public class RegistrationCollection : SortedObservableCollection<RegistrationInfo>
        {
            public UpdatableObservableCollection<Wireless.RegistrationTable> GetInterface(string name)
            {
                var info = Find(name);

                if (info == null)
                {
                    info = new RegistrationInfo(name);
                    Update(info);
                }

                return info.Registrations;
            }
        }

        private RosItemCollection<Wireless.RegistrationTable> results;
        private RegistrationCollection registrations = new RegistrationCollection();

        public WirelessRegistrationTableView()
        {
            InitializeComponent();
        }

        ~WirelessRegistrationTableView()
        {
            if (results != null)
            {
                results.Updated -= RegistrationUpdated;
                results.Removed -= RegistrationRemoved;
                results.Dispose();
                results = null;
            }
        }

        protected override void StartUpdate()
        {
            base.StartUpdate();

            if (results == null)
            {
                results = new RosItemCollection<Wireless.RegistrationTable>(API, TimeSpan.FromSeconds(2));
                results.Updated += RegistrationUpdated;
                results.Removed += RegistrationRemoved;
                results.Completed += RegistrationCompleted;

                registrations = new RegistrationCollection();
                InterfaceList.ItemsSource = registrations;
            }

            results.Start();
        }

        void RegistrationCompleted()
        {
            if (registrations.Count == 0)
            {
                NotFound.Visibility = System.Windows.Visibility.Visible;
            }
        }

        protected override void PauseUpdate()
        {
            base.PauseUpdate();

            if (results != null)
            {
                results.Stop();
            }
        }

        void RegistrationUpdated(Wireless.RegistrationTable item)
        {
            if (item.Interface == null)
                return;

            var items = registrations.GetInterface(item.Interface);
            items.Update(item);
        }

        void RegistrationRemoved(Wireless.RegistrationTable item)
        {
            foreach (var info in registrations)
            {
                info.Registrations.Remove(item);
            }
        }
    }
}
