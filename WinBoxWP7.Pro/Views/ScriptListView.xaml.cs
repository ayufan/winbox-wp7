﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using CommonLib;
using RouterOS.Serialization;

namespace RouterOS.Views
{
    public partial class ScriptListView : RouterOS.API.ItemUserControl
    {
        private RosItemCollection<Scripting.Script> scripts;

        public ScriptListView()
        {
            InitializeComponent();
        }

        ~ScriptListView()
        {
            if (scripts != null)
            {
                scripts.Dispose();
                scripts = null;
            }
        }

        protected override void StartUpdate()
        {
            base.StartUpdate();

            if (scripts == null)
            {
                scripts = new RosItemCollection<Scripting.Script>(API);
                scripts.Completed += () =>
                    {
                        ScriptListProgress.IsIndeterminate = false;
                        if (scripts.Items.Count == 0)
                        {
                            NoScriptFound.Visibility = System.Windows.Visibility.Visible;
                        }
                    };
                scripts.Updated += (item) =>
                    {
                        NoScriptFound.Visibility = System.Windows.Visibility.Collapsed;
                    };
                ScriptList.ItemsSource = scripts.Items;
            }

            scripts.Start();
        }

        protected override void PauseUpdate()
        {
            base.PauseUpdate();

            if (scripts != null)
            {
                scripts.Stop();
            }
        }

        private void RunScript(object sender, GestureEventArgs e)
        {
            if (App.IsTrial)
            {
                App.TrialMessage("TRIAL doesn't allow to run script.");
                return;
            }

            Scripting.Script script = sender.GetTag<Scripting.Script>();
            if (script == null)
                return;

            if (MessageBox.Show("Run script can potentialy harm your router. Do it anyway?", DeviceName, MessageBoxButton.OKCancel) == MessageBoxResult.OK)
            {
                try
                {
                    script.Run(API, this.CheckResult);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK);
                }
            }
        }
    }
}
