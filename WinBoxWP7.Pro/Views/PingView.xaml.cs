﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using RouterOS.Serialization;
using RouterOS.Formatters;
using Coding4Fun.Phone.Controls;

namespace RouterOS.Views
{
    public partial class PingView : RouterOS.API.UserControl
    {
        public static readonly GenericView View = new GenericView(typeof(PingView), "ping");

        private RosItemStats<Tools.Ping> responses;
        private RosItemCollection<Interface> interfaces;

        private static TimeSpanToNiceStringConverter converter = new TimeSpanToNiceStringConverter();

        public PingView()
        {
            InitializeComponent();
        }

        ~PingView()
        {
            if (interfaces != null)
            {
                interfaces.Dispose();
                interfaces = null;
            }

            if (responses != null)
            {
                responses.Dispose();
                responses = null;
            }
        }

        protected override void StartUpdate()
        {
            base.StartUpdate();

            if (interfaces == null)
            {
                interfaces = new RosItemCollection<Interface>(API);
                interfaces.Items.Add(new Interface() { Name = "all" });
                InterfaceList.ItemsSource = interfaces.Items;
            }
            
            interfaces.Start();
        }

        protected override void PauseUpdate()
        {
            base.PauseUpdate();

            if (interfaces != null)
            {
                interfaces.Stop();
            }
        }

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            if (responses == null)
            {
                // fill ping options
                Tools.PingOptions options = new Tools.PingOptions();
                options.Address = Address.AddressName;
                options.ArpPing = ARPPing.IsChecked.GetValueOrDefault();
                options.Interface = InterfaceList.SelectedItem as Interface;

                if (string.IsNullOrEmpty(options.Address))
                {
                    MessageBox.Show("Fill the address.");
                    return;
                }

                Address.SaveAddress();

                if (options.Interface.ToString() == "all")
                    options.Interface = null;

                // send ping command
                responses = new RosItemStats<Tools.Ping>(API, options);
                responses.Updated += pingResponses_Updated;
                responses.Completed += pingResponses_Completed;
                responses.Start();

                // attach resources
                PingActive.IsIndeterminate = true;
                PingItems.ItemsSource = responses.Items;
                PingStatus.Text = "";
                PingTimes.Text = "";
            }
        }

        private void Stop_Click(object sender, RoutedEventArgs e)
        {
            pingResponses_Completed();
        }

        private void ShowAdvanced_Click(object sender, RoutedEventArgs e)
        {
            AdvancedOptions.IsExpanded = true;
        }

        private void HideAdvanced_Click(object sender, RoutedEventArgs e)
        {
            AdvancedOptions.IsExpanded = false;
        }

        void pingResponses_Updated(Tools.Ping value, Tools.Ping lastValue)
        {
            // fill low pane with statistics
            PingStatus.Text = String.Format("Received:{0} Sent:{1} Loss:{2}%", value.Received, value.Sent, value.PacketLoss);
            PingTimes.Text = String.Format("Min:{0} Max:{1} Avg:{2}",
                converter.Convert(value.MinRTT, typeof(String), null, System.Globalization.CultureInfo.CurrentCulture),
                converter.Convert(value.MaxRTT, typeof(String), null, System.Globalization.CultureInfo.CurrentCulture),
                converter.Convert(value.AvgRTT, typeof(String), null, System.Globalization.CultureInfo.CurrentCulture));
        }

        void pingResponses_Completed()
        {
            if (responses != null)
            {
                responses.Updated -= pingResponses_Updated;
                responses.Completed -= pingResponses_Completed;
                responses.ErrorOccurred -= ErrorOccurred;
                responses.Dispose();
                responses = null;
            }

            PingActive.IsIndeterminate = false;
        }
    }
}
