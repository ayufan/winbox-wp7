﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

namespace RouterOS.Pages
{
    public partial class SshConnectionEditPage : PhoneApplicationPage
    {
        public static readonly string Uri = "/Pages/SshConnectionEditPage.xaml";

        private SshConnectionInfo info;

        public SshConnectionEditPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            string uid;

            if (NavigationContext.QueryString.TryGetValue("uid", out uid))
            {
                info = SshConnectionCollection.SshConnections.Find(new Guid(uid));
                if (info == null)
                    throw new KeyNotFoundException("uid");
                NewTitle.Visibility = Visibility.Collapsed;
                EditTitle.Visibility = Visibility.Visible;
            }
            else
            {
                info = new SshConnectionInfo();
                NewTitle.Visibility = Visibility.Visible;
                EditTitle.Visibility = Visibility.Collapsed;
                // ButtonDelete.IsEnabled = false;
            }

            this.DataContext = info;
        }

        private void ButtonSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(SshName.Text))
            {
                MessageBox.Show("Sorry. Invalid Name!");
                return;
            }
            if (string.IsNullOrWhiteSpace(SshHost.Text))
            {
                MessageBox.Show("Sorry. Invalid Address!");
                return;
            }
            if (string.IsNullOrWhiteSpace(SshUser.Text))
            {
                MessageBox.Show("Sorry. Invalid User!");
                return;
            }
            if (ushort.Parse(SshPort.Text) == 0)
            {
                MessageBox.Show("Sorry. Invalid API Port!");
                return;
            }

            info.Name = SshName.Text;
            info.SshHost = SshHost.Text;
            info.SshUser = SshUser.Text;
            info.SshPassword = SshPassword.Password;
            info.SshPort = ushort.Parse(SshPort.Text);

            SshConnectionCollection.SshConnections.Update(info);

            if (NewTitle.Visibility == System.Windows.Visibility.Visible && App.IsTrial && SshConnectionCollection.SshConnections.Count >= 1)
            {
                SshConnectionCollection.SshConnections.Remove(info);
                App.TrialMessage("TRIAL allows to use up to 1 ssh connection.");
            }

            NavigationService.GoBack();
        }

        private void ButtonDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Delete anyway?", "WinBox Lite", MessageBoxButton.OKCancel) != MessageBoxResult.OK)
                return;

            SshConnectionCollection.SshConnections.Remove(info);

            NavigationService.GoBack();
        }
    }
}