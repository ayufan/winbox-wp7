﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using RouterOS.Serialization;
using RouterOS.API;
using CommonLib;

namespace RouterOS.Pages
{
    public partial class FileRestorePickerPage : RouterOS.API.ItemApplicationPage
    {
        public static readonly string Uri = "/Pages/FileRestorePickerPage.xaml";

        private RosItemCollection<Tools.File> files;

        public FileRestorePickerPage()
        {
            InitializeComponent();
        }
        
        ~FileRestorePickerPage()
        {
            if (files != null)
            {
                files.Dispose();
                files = null;
            }
        }

        protected override void StartUpdate()
        {
            base.StartUpdate();

            if (files == null)
            {
                files = new RosItemCollection<Tools.File>(API, new EqualQuery("type", Tools.File.BackupType));

                files.Completed += () =>
                {
                    FileListProgress.IsIndeterminate = false;
                    if (files.Items.Count == 0)
                    {
                        NoFileFound.Visibility = System.Windows.Visibility.Visible;
                    }
                };
                files.Updated += (item) =>
                {
                    NoFileFound.Visibility = System.Windows.Visibility.Collapsed;
                };
                FileList.ItemsSource = files.Items;
            }
            
            files.Start();
        }
        
        protected override void PauseUpdate()
        {
            base.PauseUpdate();

            if (files != null)
            {
                files.Stop();
            }
        }

        private void File_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Tools.File file = sender.GetTag<Tools.File>();
            if (file == null)
                return;

            if (App.IsTrial)
            {
                App.TrialMessage("TRIAL allows only to create backups.");
                return;
            }

            if (MessageBox.Show("Do you want to restore backup from '" + file.Name + "'?", "RESTORE", MessageBoxButton.OKCancel) != MessageBoxResult.OK)
                return;

            CustomCommand cmd = new CustomCommand();
            cmd.Scope = new string[] { "system", "backup", "load" };
            cmd["name"] = file.NameAndExt;

            API.ExecuteCommand(cmd, this.CheckResult);
        }
    }
}