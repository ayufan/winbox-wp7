﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using RouterOS.API;
using System.IO;
using System.Threading;
using System.Net.Sockets;
using System.Collections.ObjectModel;

namespace RouterOS.Pages
{
    public partial class DeviceConnectPage : PhoneApplicationPage
    {
        public static readonly string Uri = "/Pages/DeviceConnectPage.xaml";

        private DeviceInfo info;
        private SshConnectionInfo sshInfo;
        private Connection API { get; set; }
        private bool reconnect { get; set; }

        public DeviceConnectPage()
        {
            InitializeComponent();

            Unloaded += DeviceAPI_Unloaded;
        }

        void DeviceAPI_Unloaded(object sender, RoutedEventArgs e)
        {
            if (!reconnect)
            {
                NavigationService.RemoveBackEntry();
            }

            if (API is SshStreamConnection)
            {
                SshStreamConnection sshAPI = API as SshStreamConnection;
                sshAPI.HostKeyReceived -= API_HostKeyReceived;
            }

            if (API != null)
            {
                API.Connecting -= API_Connecting;
                API.Established -= API_Established;
                API.Authenticating -= API_Authenticating;
                API.Authenticated -= API_Authenticated;
                API.Connected -= API_Connected;
            }
        }

        protected override void OnRemovedFromJournal(System.Windows.Navigation.JournalEntryRemovedEventArgs e)
        {
            base.OnRemovedFromJournal(e);
        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            base.OnBackKeyPress(e);

            if (API != null)
            {
                API.Disconnect();
                API = null;
            }

            if (reconnect)
            {
                e.Cancel = true;
                NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
            }
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            string uid = null;
            if (!NavigationContext.QueryString.TryGetValue("uid", out uid))
                throw new KeyNotFoundException("no device found");

            if (NavigationContext.QueryString.ContainsKey("reconnect"))
                reconnect = true;

            info = DeviceCollection.Devices.Find(new Guid(uid));
            if (info == null)
                throw new KeyNotFoundException("no device found");

            lock (info)
            {
                if (info.API != null)
                {
                    info.API.Disconnect();

                    if (reconnect)
                        API = info.API;
                    else
                        info.API = null;
                }

                if (API == null)
                {
                    if (info.UseSSH)
                    {
                        sshInfo = SshConnectionCollection.SshConnections.Find(info.SshConnection);

                        if (sshInfo == null)
                        {
                            MessageBox.Show("SSH Connection not found. Go to device settings and select valid connection");
                            NavigationService.GoBack();
                            return;
                        }

                        SshStreamConnection sshAPI = new SshStreamConnection()
                        {
                            Host = info.Host,
                            Login = info.User,
                            Password = info.Password,
                            Port = info.Port,
                            SshHost = sshInfo.SshHost,
                            SshUser = sshInfo.SshUser,
                            SshPassword = sshInfo.SshPassword,
                            SshPort = sshInfo.SshPort
                        };

                        sshAPI.HostKeyReceived += API_HostKeyReceived;

                        API = sshAPI;
                    }
                    else
                    {
                        API = new AsyncSocketConnection()
                        {
                            Host = info.Host,
                            Login = info.User,
                            Password = info.Password,
                            Port = info.Port
                        };
                    }
                }
            }

            API.Connecting += API_Connecting;
            API.Established += API_Established;
            API.Authenticating += API_Authenticating;
            API.Authenticated += API_Authenticated;
            API.Connected += API_Connected;
            API.ScheduleConnect();
        }

        void API_Authenticated(Connection conn, Exception ex)
        {
            Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                if (ex != null)
                {
                    ConnectionStatus.Text = "Authentication error:";
                    ConnectionExceptionBlock.Visibility = Visibility.Visible;
                    ConnectionException.Text = ex.Message;
                }
                else
                {
                    ConnectionStatus.Text = "Authenticated.";
                }
            });
        }

        void API_Authenticating(Connection conn)
        {
            Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                ConnectionStatus.Text = "Authenticating...";
            });
        }

        void API_Established(Connection conn, Exception ex)
        {
            Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                if (ex != null)
                {
                    ConnectionStatus.Text = "Connection cannot be established:";
                    ConnectionExceptionBlock.Visibility = Visibility.Visible;
                    ConnectionException.Text = ex.Message;
                }
                else
                {
                    ConnectionStatus.Text = "Established.";
                }
            });
        }

        void API_Connected(Connection conn, Exception ex)
        {
            Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                ConnectionProgress.IsIndeterminate = false;

                if (ex != null)
                {
                    if (ConnectionExceptionBlock.Visibility == Visibility.Collapsed)
                    {
                        ConnectionStatus.Text = "Connecting error:";
                        ConnectionExceptionBlock.Visibility = Visibility.Visible;
                        ConnectionException.Text = ex.Message;
                    }
                }
                else
                {
                    ConnectionStatus.Text = "Connected.";

                    if (API == null)
                    {
                        return;
                    }

                    lock (info)
                    {
                        info.LastLogged = DateTime.Now;
                        info.API = API;
                        DeviceCollection.Devices.Update(info);
                        if (reconnect)
                            NavigationService.GoBack();
                        else
                            NavigationService.Navigate(new Uri("/DevicePage.xaml?uid=" + info.Uid.ToString(), UriKind.Relative));
                    }
                }
            });
        }

        void API_Connecting(Connection conn)
        {
            Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                ConnectionExceptionBlock.Visibility = Visibility.Collapsed;
                ConnectionProgress.IsIndeterminate = true;
                ConnectionStatus.Text = "Connecting...";
            });
        }

        void API_HostKeyReceived(object sender, Renci.SshNet.Common.HostKeyEventArgs e)
        {
            if (sshInfo == null)
                return;

            string prompt;

            lock (sshInfo)
            {
                if (sshInfo.SshHostKey == null)
                {
                    prompt = "Accept fingerprint: " + BitConverter.ToString(e.FingerPrint);
                }
                else if (!sshInfo.SshHostKey.SequenceEqual(e.HostKey))
                {
                    prompt = "WARNING! Fingerprint changed to: " + BitConverter.ToString(e.FingerPrint) + ". Accept new fingerprint?";
                }
                else
                {
                    return;
                }

                AutoResetEvent resetEvent = new AutoResetEvent(false);
                MessageBoxResult result = MessageBoxResult.OK;

                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    result = MessageBox.Show(prompt, info.Name, MessageBoxButton.OKCancel);
                    resetEvent.Set();
                });

                resetEvent.WaitOne();

                if (result == MessageBoxResult.OK)
                {
                    sshInfo.SshHostKey = e.HostKey;
                    SshConnectionCollection.SshConnections.Update(sshInfo);
                }
                else
                {
                    e.CanTrust = false;
                }
            }
        }

        private void ConnectionRetry_Click(object sender, RoutedEventArgs e)
        {
            if (API != null)
                API.ScheduleConnect();
        }
    }
}