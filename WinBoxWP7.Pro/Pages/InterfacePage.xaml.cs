﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using RouterOS.API;
using System.Windows.Controls.DataVisualization.Charting;
using System.Collections.ObjectModel;
using RouterOS.Serialization;
using CommonLib;

namespace RouterOS.Pages
{
    public class BridgePortInfo : RouterOS.Interfaces.Bridges.Port
    {
        public string Title
        {
            get { return Interface; }
        }
        public string Description
        {
            get
            {
                List<string> items = new List<string>();
                if (Dynamic.GetValueOrDefault())
                    items.Add("dynamic");
                else if (Disabled.GetValueOrDefault())
                    items.Add("disabled");
                else if (Inactive.GetValueOrDefault())
                    items.Add("invalid");
                else
                    items.Add("running");
                if (Role != null)
                    items.Add(Role);
                return string.Join(" ", items);
            }
        }
    }

    public class BridgeHostInfo : RouterOS.Interfaces.Bridges.Host
    {
        public string Title
        {
            get { return MacAddress.ToString(); }
        }
        public string Description
        {
            get
            {
                List<string> items = new List<string>();
                if (OnInterface != null)
                    items.Add(OnInterface);
                else if (Age != null)
                    items.Add(Age.ToString());
                return string.Join(" ", items);
            }
        }
    }

    public partial class InterfacePage : RouterOS.API.MappedApplicationPage
    {
        public string InterfaceId { get; private set; }
        public string InterfaceName { get; private set; }
        public string InterfaceType { get; private set; }

        private RosItemUpdater updater;
        private RosItemStats<Interfaces.Interface> interfaceSamples;

        private RosItemCollection<BridgePortInfo> bridgePorts;
        private RosItemCollection<BridgeHostInfo> bridgeHosts;

        public InterfacePage()
        {
            InitializeComponent();

            for (int i = PanoramaView.Items.Count; i-- > 0; )
            {
                PivotItem item = (PivotItem)PanoramaView.Items[i];
                item.TabIndex = i;

                if (item.Visibility == System.Windows.Visibility.Collapsed)
                    PanoramaView.Items.Remove(item);
            }

            AddMapping("name", NameTitle, Name);
            AddMapping("type", TypeTitle, Type);
            AddMapping("mtu", MTUTitle, MTU);
            AddMapping("l2mtu", L2MTUTitle, L2MTU);
            AddMapping("max-l2mtu", MaxL2MTUTitle, MaxL2MTU);
            AddMapping("mac-address", MacAddressTitle, MacAddress);
            AddMapping("arp", ARPTitle, ARP);
            AddMapping(null, StatusTitle, Status);

            AddMapping("auto-negotiation", EthernetView, AutoNegotationTitle, AutoNegotation);
            AddMapping("full-duplex", EthernetView, FullDuplexTitle, FullDuplex);
            AddMapping("speed", EthernetView, SpeedTitle, Speed);
            AddMapping("master-port", EthernetView, MasterPortTitle, MasterPort);
            AddMapping("switch", EthernetView, SwitchTitle, Switch);

            AddMapping("protocol-mode", BridgeView, ProtocolModeTitle, ProtocolMode);
            AddMapping("auto-mac", BridgeView, AutoMacTitle, AutoMac);
            AddMapping("admin-mac", BridgeView, AdminMacTitle, AdminMac);
        }

        protected override void StartUpdate()
        {
            base.StartUpdate();

            if (InterfaceId == null)
                return;

            interfaceSamples = new RosItemStats<Interfaces.Interface>(API, RosId.Parse(InterfaceId, API), TimeSpan.FromSeconds(1), 100);
            interfaceSamples.Updated += new RosItemStats<Interfaces.Interface>.ItemUpdated(interfaceSamples_Updated);

            ((LineSeries)BytesGraph.Series[0]).ItemsSource = interfaceSamples.Items;
            ((LineSeries)BytesGraph.Series[1]).ItemsSource = interfaceSamples.Items;
            ((LineSeries)PacketsGraph.Series[0]).ItemsSource = interfaceSamples.Items;
            ((LineSeries)PacketsGraph.Series[1]).ItemsSource = interfaceSamples.Items;

            StartBridgeUpdate();
            StartEthernetUpdate();

            if (updater == null)
            {
                updater = new RosItemUpdater<RouterOS.Interfaces.Interface>(API, RosId.Parse(InterfaceId, API));
                updater.RecordUpdated += new RosItemUpdater.ItemRecord(updater_RecordUpdated);
                EnableKeys(updater.GetMembers().Select((member) => { return member.Name; }));
            }
        }

        protected override void StopUpdate()
        {
            base.StopUpdate();

            if (updater != null)
            {
                updater.Dispose();
                updater = null;
            }

            if (interfaceSamples != null)
            {
                interfaceSamples.Dispose();
                interfaceSamples = null;
            }

            StopBridgeUpdate();
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            string name = null;
            string type = null;
            string id = null;

            NavigationContext.QueryString.TryGetValue("name", out name);
            NavigationContext.QueryString.TryGetValue("type", out type);
            NavigationContext.QueryString.TryGetValue("id", out id);

            if (name == null || type == null || id == null)
            {
                NavigationService.GoBack();
                return;
            }

            InterfaceName = name;
            InterfaceType = type;
            InterfaceId = id;

            if (InterfaceName != null)
            {
                PanoramaView.Title = DeviceName + " // INTERFACE // " + InterfaceName;
                NameTitle.Visibility = Visibility.Visible;
                Name.Text = InterfaceName;
            }

            if (InterfaceType != null)
            {
                TypeTitle.Visibility = Visibility.Visible;
                Type.Visibility = Visibility.Visible;
                Type.Text = InterfaceType;
            }

            switch (InterfaceType)
            {
                case "bridge":
                    MakeVisible(BridgePortsView);
                    MakeVisible(BridgeHostsView);
                    EnableKeys(RosObject.GetMembers(typeof(RouterOS.Interfaces.Bridges.Bridge)).Select((item) => { return item.Name; }));
                    break;

                case "ether":
                    EnableKeys(RosObject.GetMembers(typeof(RouterOS.Interfaces.Ethernets.Ethernet)).Select((item) => { return item.Name; }));
                    break;

                default:
                    EnableKeys(RosObject.GetMembers(typeof(RouterOS.Interfaces.Interface)).Select((item) => { return item.Name; }));
                    break;
            }
        }

        void interfaceSamples_Updated(Interfaces.Interface value, Interfaces.Interface lastValue)
        {
            if (lastValue != null)
                value.Update(lastValue);
        }

        private void StartEthernetUpdate()
        {
            if (InterfaceType != "ether")
                return;

            if (updater == null)
            {
                updater = new RosItemUpdater<RouterOS.Interfaces.Ethernets.Ethernet>(API, RosId.Parse(InterfaceId, API));
                updater.RecordUpdated += new RosItemUpdater.ItemRecord(updater_RecordUpdated);
                EnableKeys(updater.GetMembers().Select((member) => { return member.Name; }));
            }
        }

        private void StartBridgeUpdate()
        {
            if (InterfaceType != "bridge")
                return;

            if (updater == null)
            {
                updater = new RosItemUpdater<RouterOS.Interfaces.Bridges.Bridge>(API, RosId.Parse(InterfaceId, API));
                updater.RecordUpdated += new RosItemUpdater.ItemRecord(updater_RecordUpdated);
                EnableKeys(updater.GetMembers().Select((member) => { return member.Name; }));
            }

            if (bridgePorts == null)
            {
                MakeVisible(BridgePortsView);
                bridgePorts = new RosItemCollection<BridgePortInfo>(API, new EqualQuery("bridge", InterfaceName));
                BridgePortsListBox.ItemsSource = bridgePorts.Items;
            }

            if (bridgeHosts == null)
            {
                MakeVisible(BridgeHostsView);
                bridgeHosts = new RosItemCollection<BridgeHostInfo>(API, new EqualQuery("bridge", InterfaceName));
                BridgeHostsListBox.ItemsSource = bridgeHosts.Items;
            }
        }

        private void StopBridgeUpdate()
        {
            if (bridgePorts != null)
            {
                bridgePorts.Dispose();
                bridgePorts = null;
            }

            if (bridgeHosts != null)
            {
                bridgeHosts.Dispose();
                bridgeHosts = null;
            }
        }

        void updater_RecordUpdated(Record record)
        {
            if (record["name"] != null)
            {
                if (InterfaceName != record["name"])
                {
                    PanoramaView.Title = DeviceName + " // INTERFACE // " + record["name"];
                    InterfaceName = record["name"];
                    StopBridgeUpdate();
                    StartBridgeUpdate();
                }
            }

            ProcessRecord(record);

            if (record.Contains("status"))
                Status.Text = "status";
            else if (record.Contains("running") && record["running"] == "true")
                Status.Text = "running";
            else if (record.Contains("disabled") && record["disabled"] == "true")
                Status.Text = "disabled";
            else if (record.Contains("invalid") && record["invalid"] == "true")
                Status.Text = "invalid";
            else
                Status.Text = "not connected";
        }

        public new void EnableEdit(object sender, EventArgs e)
        {
            base.EnableEdit(sender, e);
        }

        public new void CancelEdit(object sender, EventArgs e)
        {
            base.CancelEdit(sender, e);
        }

        public new void SaveEdit(object sender, EventArgs e)
        {
            base.SaveEdit(sender, e);
        }
    }
}