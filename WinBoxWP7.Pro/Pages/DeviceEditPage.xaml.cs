﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

namespace RouterOS.Pages
{
    public partial class DeviceEditPage : PhoneApplicationPage
    {
        public static readonly string Uri = "/Pages/DeviceEditPage.xaml";

        private DeviceInfo info;

        public SshConnectionCollection SshConnections
        {
            get { return SshConnectionCollection.SshConnections; }
        }

        public DeviceEditPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            string uid;

            SSHConnection.ItemsSource = SshConnections;

            if (NavigationContext.QueryString.TryGetValue("uid", out uid))
            {
                info = DeviceCollection.Devices.Find(new Guid(uid));
                if (info == null)
                    throw new KeyNotFoundException("uid");
                NewTitle.Visibility = Visibility.Collapsed;
                EditTitle.Visibility = Visibility.Visible;

                if (info.UseSSH)
                {
                    SshConnectionInfo sshConnection = SshConnectionCollection.SshConnections.Find(info.SshConnection);

                    if (sshConnection != null)
                    {
                        SSHConnection.SelectedIndex = SshConnectionCollection.SshConnections.IndexOf(sshConnection);
                    }
                }

                if (NavigationContext.QueryString.ContainsKey("clone"))
                {
                    NewTitle.Visibility = Visibility.Visible;
                    EditTitle.Visibility = Visibility.Collapsed;

                    info = info.Clone();

                    NavigationService.RemoveBackEntry();
                }
            }
            else
            {
                info = new DeviceInfo();
                NewTitle.Visibility = Visibility.Visible;
                EditTitle.Visibility = Visibility.Collapsed;

                if (NavigationContext.QueryString.ContainsKey("name"))
                    info.Name = NavigationContext.QueryString["name"];
                if (NavigationContext.QueryString.ContainsKey("host"))
                    info.Host = NavigationContext.QueryString["host"];
                if (NavigationContext.QueryString.ContainsKey("mac"))
                    info.MacAddress = RouterOS.MACAddress.Parse(NavigationContext.QueryString["mac"]);
            }

            this.DataContext = info;
        }

        private void ButtonSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(DeviceName.Text))
            {
                MessageBox.Show("Sorry. Invalid Name!");
                return;
            }
            if (string.IsNullOrWhiteSpace(DeviceHost.Text))
            {
                MessageBox.Show("Sorry. Invalid Address!");
                return;
            }
            if (string.IsNullOrWhiteSpace(DeviceUser.Text))
            {
                MessageBox.Show("Sorry. Invalid User!");
                return;
            }
            if (ushort.Parse(DevicePort.Text) == 0)
            {
                MessageBox.Show("Sorry. Invalid API Port!");
                return;
            }

            if (UseSSH.IsChecked.GetValueOrDefault())
            {
                if (SSHConnection.SelectedItem == null)
                {
                    MessageBox.Show("SSH Connection not selected");
                    return;
                }
            }

            info.Host = DeviceHost.Text;
            info.Name = DeviceName.Text;
            info.User = DeviceUser.Text;
            info.Password = DevicePassword.Password;
            info.Port = ushort.Parse(DevicePort.Text);

            if (UseSSH.IsChecked.GetValueOrDefault())
            {
                info.UseSSH = true;
                info.SshConnection = SshConnectionCollection.SshConnections[SSHConnection.SelectedIndex].Uid;
            }
            else
            {
                info.UseSSH = false;
            }
            
            DeviceCollection.Devices.Update(info);

            if (NewTitle.Visibility == System.Windows.Visibility.Visible && App.IsTrial && DeviceCollection.Devices.Count >= 3)
            {
                DeviceCollection.Devices.Remove(info);
                App.TrialMessage("TRIAL allows to use up to 3 devices.");
            }

            NavigationService.GoBack();
        }

        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            NavigationService.GoBack();
        }

        private void ButtonSshConnections_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri(RouterOS.Pages.SshConnectionListPage.Uri, UriKind.Relative));
        }

        private void ButtonDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Delete anyway?", "WinBox Lite", MessageBoxButton.OKCancel) != MessageBoxResult.OK)
                return;

            DeviceCollection.Devices.Remove(info);
            NavigationService.GoBack();
        }

        private void ButtonClone_Click(object sender, EventArgs e)
        {
            if (DeviceCollection.Devices.Find(info.Uid) != null)
            {
                NavigationService.Navigate(new Uri(Uri + "?uid=" + info.Uid.ToString() + "&clone=true", UriKind.Relative));
            }
        }
    }
}