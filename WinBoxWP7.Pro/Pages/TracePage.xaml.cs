﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

namespace RouterOS.Pages
{
    public partial class TracePage : RouterOS.API.BaseApplicationPage
    {
        public static readonly string Uri = "/Pages/TracePage.xaml";

        public TracePage()
        {
            InitializeComponent();
        }
    }
}