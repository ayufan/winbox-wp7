﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using RouterOS.Serialization;
using CommonLib;

namespace RouterOS.Pages
{
    public partial class InterfaceListPage : RouterOS.API.ItemApplicationPage
    {
        public static readonly string Uri = "/Pages/InterfaceListPage.xaml";

        public static Uri GetUri(Guid uid)
        {
            return new Uri(Uri + "?uid=" + uid.ToString(), UriKind.Relative);
        }

        private RosItemCollection<Interface> interfaces;

        public InterfaceListPage()
        {
            InitializeComponent();
        }

        ~InterfaceListPage()
        {
            if (interfaces != null)
            {
                interfaces.Dispose();
            }
        }

        protected override void StartUpdate()
        {
            base.StartUpdate();

            if (interfaces == null)
            {
                interfaces = new RosItemCollection<Interface>(API);
                ConnectedInterfaces.ItemsSource = interfaces.GetUserCollection((item) => { return item.Running.GetValueOrDefault() && !item.Dynamic.GetValueOrDefault(); });
                DynamicInterfaces.ItemsSource = interfaces.GetUserCollection((item) => { return item.Dynamic.GetValueOrDefault(); });
                DisabledInterfaces.ItemsSource = interfaces.GetUserCollection((item) => { return item.Disabled.GetValueOrDefault() && !item.Dynamic.GetValueOrDefault(); });
                InvalidInterfaces.ItemsSource = interfaces.GetUserCollection((item) => { return item.Invalid.GetValueOrDefault() && !item.Dynamic.GetValueOrDefault(); });
                DisconnectedInterfaces.ItemsSource = interfaces.GetUserCollection((item) => { return !item.Running.GetValueOrDefault() && !item.Disabled.GetValueOrDefault() && !item.Invalid.GetValueOrDefault() && !item.Dynamic.GetValueOrDefault(); });
            }
                
            interfaces.Start();
        }

        protected override void PauseUpdate()
        {
            base.PauseUpdate();
        }

        private void ShowInterface(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Interface info = sender.GetTag<Interface>();
            if (info == null)
                return;

            // Uri uri = new Uri("/InterfaceView.xaml?uid=" + Info.Uid.ToString() + "&name=" + HttpUtility.UrlEncode(info.Name) + "&id=" + HttpUtility.UrlEncode(info.Id.Value) + "&type=" + HttpUtility.UrlEncode(info.Type), UriKind.Relative);
            // NavigationService.Navigate(uri);
        }
        
        public new void EditItem(object sender, RoutedEventArgs e)
        {
            base.EditItem(sender, e);
        }

        public new void RemoveItem(object sender, RoutedEventArgs e)
        {
            base.RemoveItem(sender, e);
        }

        public new void CommentItem(object sender, RoutedEventArgs e)
        {
            base.CommentItem(sender, e);
        }

        public new void EnableItem(object sender, System.Windows.Input.GestureEventArgs e)
        {
            base.EnableItem(sender, e);
        }

        public new void DisableItem(object sender, System.Windows.Input.GestureEventArgs e)
        {
            base.DisableItem(sender, e);
        }
    }
}