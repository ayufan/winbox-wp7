﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using CommonLib;

namespace RouterOS.Pages
{
    public partial class SshConnectionListPage : PhoneApplicationPage
    {
        public static readonly string Uri = "/Pages/SshConnectionListPage.xaml";

        public SshConnectionCollection SshConnections
        {
            get { return SshConnectionCollection.SshConnections; }
        }

        public SshConnectionListPage()
        {
            InitializeComponent();

            Loaded += SshConnectionList_Loaded;
        }

        void SshConnectionList_Loaded(object sender, RoutedEventArgs e)
        {
            this.DataContext = this.DataContext;
        }

        private void SshConnection_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            SshConnectionInfo info = sender.GetTag<SshConnectionInfo>();
            if (info == null)
                return;

            this.GetFrame().NavigationService.Navigate(new Uri(Pages.SshConnectionEditPage.Uri + "?uid=" + info.Uid.ToString(), UriKind.Relative));
        }

        private void ButtonAdd_Click(object sender, EventArgs e)
        {
            this.GetFrame().NavigationService.Navigate(new Uri(Pages.SshConnectionEditPage.Uri, UriKind.Relative));
        }
    }
}