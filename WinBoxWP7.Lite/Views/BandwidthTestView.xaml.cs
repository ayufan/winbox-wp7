﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using RouterOS.Serialization;
using RouterOS.Formatters;
using RouterOS.Tools;
using Coding4Fun.Phone.Controls;
using CommonLib;

namespace RouterOS.Views
{
    public partial class BandwidthTestView : RouterOS.API.UserControl
    {
        public static readonly GenericView View = new GenericView(typeof(BandwidthTestView), "btest");

        public static readonly string[] TxSpeeds = new string[] 
        {
            "unlimited",
            "128k",
            "512k",
            "1M",
            "2M",
            "5M",
            "10M",
            "20M",
            "50M",
            "100M",
            "200M",
            "500M"
        };

        private RosItemStats<Tools.BandwidthTest> responses;

        public BandwidthTestView()
        {
            InitializeComponent();

            Protocol.ItemsSource = ClassExtensions.GetValues<BandwidthTestProtocol>();
            Direction.ItemsSource = ClassExtensions.GetValues<BandwidthTestDirection>();
            LocalTxSpeed.ItemsSource = TxSpeeds;
            RemoteTxSpeed.ItemsSource = TxSpeeds;
        }

        ~BandwidthTestView()
        {
            if (responses != null)
            {
                responses.ErrorOccurred -= ErrorOccurred;
                responses.Dispose();
                responses = null;
            }
        }

        protected override void StartUpdate()
        {
            base.StartUpdate();
        }

        protected override void PauseUpdate()
        {
            base.PauseUpdate();
        }

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            if (responses == null)
            {
                BandwidthTestOptions options = new BandwidthTestOptions();
                options.Address = Address.AddressName;
                options.Protocol = (BandwidthTestProtocol)Protocol.SelectedItem;
                options.Direction = (BandwidthTestDirection)Direction.SelectedItem;
                options.RemoteTxSpeed = (int)Bits.Parse(RemoteTxSpeed.SelectedItem.ToString()).Count;
                if (options.RemoteTxSpeed.Value < 0)
                    options.RemoteTxSpeed = null;
                options.LocalTxSpeed = (int)Bits.Parse(LocalTxSpeed.SelectedItem.ToString()).Count;
                if (options.LocalTxSpeed.Value < 0)
                    options.LocalTxSpeed = null;

                if (string.IsNullOrEmpty(options.Address))
                {
                    MessageBox.Show("Fill the address.");
                    return;
                }

                Address.SaveAddress();

                // send ping command
                responses = new RosItemStats<Tools.BandwidthTest>(API, options);
                responses.Updated += responses_Updated;
                responses.Completed += responses_Completed;
                responses.ErrorOccurred += ErrorOccurred;
                responses.Start();

                // attach resources
                if(TrafficGraph.Content == null)
                    TrafficGraph.Content = new Controls.RxTxGraphControl();
                ((Controls.RxTxGraphControl)TrafficGraph.Content).ItemsSource = responses.Items;

                // update controls state
                IsActive.IsIndeterminate = true;
                AdvancedOptions.IsExpanded = false;
                StatusText.Visibility = System.Windows.Visibility.Visible;
                RxSpeedText.Visibility = System.Windows.Visibility.Visible;
                TxSpeedText.Visibility = System.Windows.Visibility.Visible;
            }
        }

        private void Stop_Click(object sender, RoutedEventArgs e)
        {
            responses_Completed();
        }

        private void ShowAdvanced_Click(object sender, RoutedEventArgs e)
        {
            AdvancedOptions.IsExpanded = true;
        }

        private void HideAdvanced_Click(object sender, RoutedEventArgs e)
        {
            AdvancedOptions.IsExpanded = false;
        }

        void responses_Updated(Tools.BandwidthTest value, Tools.BandwidthTest lastValue)
        {
            if (value.Status != null)
            {
                StatusText.Text = "Status: " + value.Status;
                StatusText.Visibility = System.Windows.Visibility.Visible;
            }

            if (value.Direction == BandwidthTestDirection.Receive || value.Direction == BandwidthTestDirection.Both)
            {
                RxSpeedText.Text = string.Format("RX:{0} 10s:{1} Avg:{2}", value.RxCurrent.GetNiceString(),
                    value.Rx10sAverage.GetNiceString(),
                    value.RxTotalAverage.GetNiceString());
                RxSpeedText.Visibility = System.Windows.Visibility.Visible;
            }

            if (value.Direction == BandwidthTestDirection.Transmit || value.Direction == BandwidthTestDirection.Both)
            {
                TxSpeedText.Text = string.Format("TX:{0} 10s:{1} Avg:{2}", value.TxCurrent.GetNiceString(),
                    value.Tx10sAverage.GetNiceString(),
                    value.TxTotalAverage.GetNiceString());
                TxSpeedText.Visibility = System.Windows.Visibility.Visible;
            }
        }

        void responses_Completed()
        {
            if (responses != null)
            {
                responses.Updated -= responses_Updated;
                responses.Completed -= responses_Completed;
                responses.ErrorOccurred -= ErrorOccurred;
                responses.Dispose();
                responses = null;
            }

            StatusText.Visibility = System.Windows.Visibility.Collapsed;
            IsActive.IsIndeterminate = false;
        }
    }
}
