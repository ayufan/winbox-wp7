﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.Threading;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.GamerServices;
using RouterOS.Pages;
using RouterOS.Views;
using RouterOS.API;
using RouterOS.Serialization;

namespace RouterOS
{
    public partial class DevicePage : RouterOS.API.BaseApplicationPage
    {
        private System.Collections.ObjectModel.ObservableCollection<RouterOS.Tools.Resource> resourceSamples = new System.Collections.ObjectModel.ObservableCollection<Tools.Resource>();
        private const int MaxSamples = 60;

        public DevicePage()
        {
            InitializeComponent();

            Loaded += (sender, evnt) => { Resource.ResourceUpdated += Resource_ResourceUpdated; };
            Unloaded += (sender, evnt) => { Resource.ResourceUpdated -= Resource_ResourceUpdated; };
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            PivotTitle.Title = DeviceName;
        }

        void Resource_ResourceUpdated(RouterOS.Tools.Resource value)
        {
            resourceSamples.Add(value);
            while (resourceSamples.Count > MaxSamples)
                resourceSamples.RemoveAt(0);
        }

        private void ButtonIdentity_Click(object sender, EventArgs e)
        {
            if (Resource.Identity == null)
                return;

            if (App.IsTrial)
            {
                App.TrialMessage("TRIAL doesn't allow to change identity of the router.");
                return;
            }

            Guide.BeginShowKeyboardInput(PlayerIndex.One,
                DeviceName, "Enter new comment:", Resource.Identity.Name, this.EndIdentity, null);
        }

        private void EndIdentity(IAsyncResult result)
        {
            string newIdentity = Guide.EndShowKeyboardInput(result);

            if (Resource.Identity == null)
                return;

            if (Resource.Identity.Name == newIdentity)
                return;

            ThreadPool.QueueUserWorkItem((obj) =>
            {
                try
                {
                    Resource.Identity.Name = newIdentity;
                    Resource.Identity.Save(API);
                }
                catch (Exception ex)
                {
                    Deployment.Current.Dispatcher.BeginInvoke(() =>
                    {
                        MessageBox.Show(ex.Message, DeviceName, MessageBoxButton.OK);
                    });
                }
            });
        }

        private void RestoreButton_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri(FileRestorePickerPage.Uri + "?uid=" + Info.Uid.ToString(), UriKind.Relative));
        }

        private void BackupButton_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (MessageBox.Show("Do you want to create backup of router?", "BACKUP", MessageBoxButton.OKCancel) != MessageBoxResult.OK)
                return;

            CustomCommand cmd = new CustomCommand()
            {
                Scope = new string[] { "system", "backup", "save" }
            };

            API.ExecuteCommand(cmd, this.CheckResult);
        }

        private void RebootButton_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (MessageBox.Show("Do you want to reboot router?", "REBOOT", MessageBoxButton.OKCancel) != MessageBoxResult.OK)
                return;

            CustomCommand cmd = new CustomCommand()
            {
                Scope = new string[] { "system", "reboot" }
            };

            API.ExecuteCommand(cmd, this.CheckResult);
        }

        private void ShutdownButton_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (MessageBox.Show("Do you want to shutdown router?", "SHUTDOWN", MessageBoxButton.OKCancel) != MessageBoxResult.OK)
                return;

            CustomCommand cmd = new CustomCommand()
            {
                Scope = new string[] { "system", "shutdown" }
            };

            API.ExecuteCommand(cmd, this.CheckResult);
        }

        private void NavigateToTools(GenericView selected)
        {
            NavigationService.Navigate(GenericPage.GetUriAndSelect(Info.Uid, "tools", selected, 
                PingView.View, TraceView.View, BandwidthTestView.View));
        }

        private void NavigateToWireless(GenericView selected)
        {
            NavigationService.Navigate(GenericPage.GetUriAndSelect(Info.Uid, "wireless", selected, WirelessRegistrationTableView.View, WirelessScanView.View));
        }

        private void WirelessRegistrationTableButton_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigateToWireless(WirelessRegistrationTableView.View);
        }

        private void ScanButton_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigateToWireless(WirelessScanView.View);
        }

        private void PingButton_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigateToTools(PingView.View);
        }

        private void TraceButton_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigateToTools(TraceView.View);
        }

        private void DhcpLeaseButton_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(GenericPage.GetUri(Info.Uid, "dhcp", IpDhcpLeaseListView.View));
        }

        private void BandwithTestButton_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigateToTools(BandwidthTestView.View);
        }

        private void InterfaceButton_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(InterfaceListPage.GetUri(Info.Uid));
        }

        private void PivotTitle_LoadedPivotItem(object sender, PivotItemEventArgs e)
        {
            if (e.Item.Content != null)
                return;

            if (e.Item == GraphsItem)
            {
                ResourceGraphView graphView = new ResourceGraphView();
                graphView.ItemsSource = resourceSamples;
                e.Item.Content = graphView;
            }
        }
    }
}