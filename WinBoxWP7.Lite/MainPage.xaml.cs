﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Tasks;
using Coding4Fun.Phone.Controls.Data;

namespace RouterOS
{
    public partial class MainPage : PhoneApplicationPage
    {
        public MainPage()
        {
            InitializeComponent();

            Version.Text = PhoneHelper.GetAppAttribute("Version");
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            while (NavigationService.CanGoBack)
                NavigationService.RemoveBackEntry();

            if (App.IsTrial)
                LinkBuy.Visibility = System.Windows.Visibility.Visible;
            else
                LinkBuy.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void ButtonAdd_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri(RouterOS.Pages.DeviceEditPage.Uri, UriKind.Relative));
        }

        private void ButtonSshConnections_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri(RouterOS.Pages.SshConnectionListPage.Uri, UriKind.Relative));
        }

        private void ButtonRate_Click(object sender, EventArgs e)
        {
            new MarketplaceReviewTask().Show();
        }

        private void LinkBuy_Click(object sender, RoutedEventArgs e)
        {
            new MarketplaceDetailTask().Show();
        }

        private void Email_Click(object sender, RoutedEventArgs e)
        {
            EmailComposeTask emailComposeTask = new EmailComposeTask();
            emailComposeTask.Subject = "WinBox Lite";
            emailComposeTask.To = "ayufan@ayufan.eu";
            emailComposeTask.Show();
        }

        private void WebSite_Click(object sender, RoutedEventArgs e)
        {
            WebBrowserTask webBrowserTask = new WebBrowserTask();
            webBrowserTask.Uri = new Uri("http://ayufan.eu", UriKind.Absolute);
            webBrowserTask.Show();
        }

        private void LinkRate_Click(object sender, RoutedEventArgs e)
        {
            ButtonRate_Click(sender, e);
        }

        private void Pivot_LoadedPivotItem(object sender, PivotItemEventArgs e)
        {
            if (e.Item.Content != null)
                return;

            if (e.Item == DevicesItem)
                e.Item.Content = new Views.DeviceListView();
            else if (e.Item == DiscoverItem)
                e.Item.Content = new Views.DeviceDiscoveryView();
        }
    }
}