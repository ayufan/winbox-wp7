﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace RouterOS.Controls
{
    public partial class AddressSelectionControl : UserControl
	{
        public static readonly DependencyProperty AddressNameProperty = DependencyProperty.Register("AddressName", typeof(string), typeof(AddressSelectionControl), new PropertyMetadata(""));

        public string AddressName
        {
            get { return (string)GetValue(AddressNameProperty); }
            set { SetValue(AddressNameProperty, value); }
        }

        public AddressCollection Addresses
        {
            get { return AddressCollection.Addresses; }
        }

		public AddressSelectionControl()
		{
			InitializeComponent();

            if (Addresses.Count != 0)
                AddressName = Addresses[0];
		}

        private void Address_Select(object sender, RoutedEventArgs e)
        {
            string addressName = AddressName;

            if (!string.IsNullOrEmpty(addressName))
            {
                int index = Addresses.IndexOf(addressName);

                if (index < 0)
                {
                    Addresses.Insert(0, addressName);
                    index = 0;
                }

                AddressList.SelectedItem = addressName;
            }

            AddressList.Open();
        }

        public void SaveAddress()
        {
            string addressName = AddressName;

            int index = Addresses.IndexOf(addressName);

            if (index != 0)
            {
                if (Addresses.Count != 0)
                {
                    AddressList.SelectedIndex = 0;
                    Addresses.Remove(addressName);
                    Addresses.Insert(0, addressName);
                }
                else
                {
                    Addresses.Add(addressName);
                }
                AddressList.SelectedIndex = 0;
            }
        }

        private void Address_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Address.Text = AddressList.SelectedItem as string;
        }
	}
}