﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using RouterOS.Views;

namespace RouterOS.Pages
{
    public partial class GenericPage : RouterOS.API.BaseApplicationPage
    {
        public static readonly string Uri = "/Pages/GenericPage.xaml";
        private bool wasNaviagedTo;

        public GenericPage()
        {
            InitializeComponent();

            PivotTitle.Loaded += new RoutedEventHandler(PivotTitle_Loaded);
        }

        void PivotTitle_Loaded(object sender, RoutedEventArgs e)
        {
            if (State.ContainsKey("SelectedPivotItem"))
                PivotTitle.SelectedIndex = (int)State["SelectedPivotItem"];
        }

        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);

            State["SelectedPivotItem"] = PivotTitle.SelectedIndex;
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            if (!wasNaviagedTo)
            {
                wasNaviagedTo = true;

                string title = null;
                if (NavigationContext.QueryString.TryGetValue("title", out title))
                    PivotTitle.Title = title;

                string selectedText = null;
                if (NavigationContext.QueryString.TryGetValue("selected", out selectedText))
                    State["SelectedPivotItem"] = int.Parse(selectedText);

                for (int i = 0; ; ++i)
                {
                    string viewTitle;
                    if (NavigationContext.QueryString.TryGetValue("viewTitle" + i.ToString(), out viewTitle))
                    {
                        PivotTitle.Items.Add(new PivotItem() { Header = viewTitle });
                    }
                    else
                    {
                        break;
                    }
                }
            }

            base.OnNavigatedTo(e);
        }

        public static Uri GetUriAndSelect(Guid uid, string title, GenericView? selected, params GenericView[] views)
        {
            List<string> query = new List<string>();
            query.Add("uid=" + uid.ToString());
            query.Add("title=" + HttpUtility.UrlEncode(title));
            if (selected != null)
                query.Add("selected=" + Array.IndexOf(views, selected.Value).ToString());
            for (int i = 0; i < views.Length; ++i)
                query.Add(string.Format("viewType{0}={1}&viewTitle{0}={2}", i, views[i].ViewType, views[i].ViewTitle));
            return new Uri(Uri + "?" + string.Join("&", query), UriKind.Relative);
        }

        public static Uri GetUri(Guid uid, string title, params GenericView[] views)
        {
            return GetUriAndSelect(uid, title, null, views);
        }

        private void PivotTitle_LoadedPivotItem(object sender, PivotItemEventArgs e)
        {
            if (e.Item.Content != null)
            {
                return;
            }

            Pivot pivot = (Pivot)sender;
            int index = pivot.Items.IndexOf(e.Item);

            string viewTypeName;

            if (NavigationContext.QueryString.TryGetValue("viewType" + index.ToString(), out viewTypeName))
            {
                e.Item.Content = Activator.CreateInstance(Type.GetType(viewTypeName));
            }
        }
    }
}

namespace RouterOS.Views
{
    public struct GenericView
    {
        public Type ViewType { get; set; }
        public string ViewTitle { get; set; }

        public GenericView(Type type, string title)
            : this()
        {
            ViewType = type;
            ViewTitle = title;
        }
    }
}