﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace RouterOS.API
{
    public class Response : IEnumerable
    {
        private Connection m_Connection;
        private string m_Ret;
        private Record[] m_Records;

        public Connection Connection
        {
            get { return m_Connection; }
            internal set { m_Connection = value; }
        }

        public string Ret
        {
            get { return m_Ret; }
        }

        public Record[] Records
        {
            get { return m_Records; }
        }

        public int RecordCount
        {
            get { return m_Records != null ? m_Records.Length : 0; }
        }

        public Response(Record message, Record[] records)
        {
            if (message == null)
            {
                throw new InvalidOperationException("Failed to execute command");
            }

            if (message.Type != RecordType.Done)
            {
                throw new InvalidOperationException(message["message"]);            
            }

            m_Connection = message.Connection;
            if (message.Contains("ret"))
                m_Ret = message["ret"];
            m_Records = records;
        }

        public Response(Record message)
            : this(message, null)
        {
        }

        public IEnumerator GetEnumerator()
        {
            if (m_Records == null)
                return null;
            return m_Records.GetEnumerator();
        }
    };
}
