﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace RouterOS.API
{
    public struct CommandTag
    {
        private Connection connection;
        private int tag;
        private string text;

        public int Tag
        {
            get { return tag; }
        }
        public Connection Connection
        {
            get { return connection; }
        }
        public bool HasValue
        {
            get { return connection != null && tag > 0; }
        }

        public CommandTag(Connection connection, int tag)
        {
            this.tag = tag;
            this.connection = connection;
            this.text = null;
        }

        public CommandTag(Connection connection, string tag)
        {
            this.tag = int.Parse(tag);
            this.connection = connection;
            this.text = tag;
        }

        public override string ToString()
        {
            if (text == null)
                text = tag.ToString();
            return text;
        }

        public override int GetHashCode()
        {
            return tag;
        }

        public void Cancel()
        {
            if (!HasValue)
                return;
            connection.Cancel(this);
            connection = null;
            tag = 0;
        }

        public static implicit operator bool (CommandTag tag)
        {
            return tag.tag != 0;
        }

        public static bool operator ==(CommandTag a, CommandTag b)
        {
            return a.connection == b.connection && a.tag == b.tag;
        }
        public static bool operator !=(CommandTag a, CommandTag b)
        {
            return a.connection != b.connection || a.tag != b.tag;
        }

        public override bool Equals(object obj)
        {
            if (obj is CommandTag)
                return this == (CommandTag)obj;
            if (obj == null)
                return tag != 0;
            return false;
        }
    }
}
