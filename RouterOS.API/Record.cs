﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RouterOS.API
{
    /// <summary>
    /// Record status.
    /// </summary>
    public enum RecordType
    {
        Undefined,

        /// <summary>
        /// Response finished successfully.
        /// </summary>
        Done,

        /// <summary>
        /// Response contains record data.
        /// </summary>
        Record,

        /// <summary>
        /// Response finished with error.
        /// </summary>
        Error,

        /// <summary>
        /// Response finished with CRITICAL error.
        /// </summary>
        Fatal
    };

    /// <summary>
    /// Contains one record data.
    /// </summary>
    public class Record
    {
        internal Connection m_Connection;
        internal Dictionary<CommandKey, string> m_KeyValues = new Dictionary<CommandKey, string>();
        internal RecordType m_Type;
        internal CommandTag m_Tag;

        private static byte[] typeDone = Encoding.UTF8.GetBytes("!done");
        private static byte[] typeRecord = Encoding.UTF8.GetBytes("!re");
        private static byte[] typeError = Encoding.UTF8.GetBytes("!trap");
        private static byte[] typeFatal = Encoding.UTF8.GetBytes("!fatal");
        private static byte[] typeTag = Encoding.UTF8.GetBytes(".tag=");

        /// <summary>
        /// Connection from which came record data.
        /// </summary>
        public Connection Connection
        {
            get { return m_Connection; }
            internal set { m_Connection = value; }
        }

        /// <summary>
        /// Returns named key value, ie. "interface", "address".
        /// </summary>
        /// <param name="key">RouterOS record key name.</param>
        /// <returns>Value for key.</returns>
        public string this[CommandKey key]
        {
            get { return m_KeyValues[key]; }
        }

        /// <summary>
        /// Returns list of keys contained in record.
        /// </summary>
        public IEnumerable<CommandKey> Keys
        {
            get { return m_KeyValues.Keys; }
        }

        /// <summary>
        /// Returns collection of keys and values contained in record.
        /// </summary>
        public Dictionary<CommandKey, string> KeyValues
        {
            get { return m_KeyValues; }
        }

        /// <summary>
        /// Checks if record contains specified key.
        /// </summary>
        /// <param name="key">RouterOS record key name.</param>
        /// <returns>true if found.</returns>
        public bool Contains(CommandKey key)
        {
            return m_KeyValues.ContainsKey(key);
        }

        public bool TryGetValue(CommandKey key, out string text)
        {
            return m_KeyValues.TryGetValue(key, out text);
        }

        /// <summary>
        /// Returns record identifier (.id).
        /// </summary>
        public RecordId? ItemId
        {
            get
            {
                if (Contains(".id"))
                    return RecordId.Parse(this[".id"], m_Connection);
                return null;
            }
        }

        public bool IsDead
        {
            get { return Contains(".dead") && bool.Parse(this[".dead"]); }
        }

        /// <summary>
        /// Checks if record contains data about item.
        /// </summary>
        public bool IsItem
        {
            get { return Contains(".id"); }
        }

        /// <summary>
        /// Checks if record contains data about values, not item.
        /// </summary>
        public bool IsValue
        {
            get { return !Contains(".id"); }
        }

        /// <summary>
        /// Returns record type.
        /// </summary>
        public RecordType Type
        {
            get { return m_Type; }
        }

        /// <summary>
        /// Returns tag identyfing command from which originated record.
        /// </summary>
        public CommandTag Tag
        {
            get { return m_Tag; }
        }

        /// <summary>
        /// Constructs null object.
        /// </summary>
        public Record()
        {
        }

        public void Dump()
        {
#if TRACE_DETAIL
            System.Diagnostics.Debug.WriteLine("[{0} / {1}] BEGIN", m_Type, m_Tag.ToString());

            foreach (KeyValuePair<string, string> keyValue in KeyValues)
            {
                System.Diagnostics.Debug.WriteLine("[{0} / {1}] {2} => {3}", m_Type, m_Tag.ToString(),
                    keyValue.Key, keyValue.Value);
            }

            System.Diagnostics.Debug.WriteLine("[{0} / {1}] END", m_Type, m_Tag.ToString());
#endif
        }

        /// <summary>
        /// Constructs object and fills with response.
        /// </summary>
        /// <param name="connection">RouterOS connection object.</param>
        /// <param name="lines">List of response lines.</param>
        public Record(Connection connection, IEnumerable<string> lines)
        {
            // parse response
            foreach (string line in lines)
            {
                // ignore empty lines
                if (string.IsNullOrEmpty(line))
                    continue;

                if (m_Type == RecordType.Undefined)
                {
                    // check response type
                    switch (line)
                    {
                        case "!done":
                            m_Type = RecordType.Done;
                            break;

                        case "!re":
                            m_Type = RecordType.Record;
                            break;

                        case "!fatal":
                            m_Type = RecordType.Fatal;
                            break;

                        case "!trap":
                            m_Type = RecordType.Error;
                            break;

                        default:
                            throw new ArgumentException("invalid response type: " + line);
                    }
                    continue;
                }
                else
                {
                    if (line.StartsWith("="))
                    {
                        string keyValue = line.Substring(1);
                        int index = keyValue.IndexOf('=');
                        if (index >= 0)
                            KeyValues[keyValue.Substring(0, index)] = keyValue.Substring(index + 1);
                    }
                    else if (line.StartsWith(".tag="))
                    {
                        m_Tag = new CommandTag(connection, line.Substring(".tag=".Length));
                    }
                    else
                    {
                        throw new ArgumentException("undefined type for: " + line);
                    }
                }
            }
            m_Connection = connection;

            Dump();
        }

        /// <summary>
        /// Constructs object and fills with response.
        /// </summary>
        /// <param name="connection">RouterOS connection object.</param>
        /// <param name="lines">List of response lines.</param>
        public Record(Connection connection, byte[] data, ref int offset, int endOffset)
        {
            int typeSize = data.ReadEncodedLength(ref offset, ref endOffset, true);
            if (typeDone.ArrayEqual(data, offset, typeSize))
                m_Type = RecordType.Done;
            else if (typeError.ArrayEqual(data, offset, typeSize))
                m_Type = RecordType.Error;
            else if (typeRecord.ArrayEqual(data, offset, typeSize))
                m_Type = RecordType.Record;
            else if (typeFatal.ArrayEqual(data, offset, typeSize))
                m_Type = RecordType.Fatal;
            else
                throw new ArgumentException("invalid response type: " + Encoding.UTF8.GetString(data, offset, typeSize));

            offset += typeSize;

            while (offset < endOffset)
            {
                typeSize = data.ReadEncodedLength(ref offset, ref endOffset, true);
                if (typeSize < 0)
                    break;

                if (typeSize == 0)
                {
                    // request complete
                    m_Connection = connection;
                    Dump();
                    return;
                }

                if (data[offset] == '=')
                {
                    for (int index = 1; index < typeSize; ++index)
                    {
                        if (data[offset + index] == '=')
                        {
                            CommandKey key = new CommandKey(data, offset + 1, index - 1);
                            string value = Encoding.UTF8.GetString(data, offset + 1 + index, typeSize - index - 1);
                            KeyValues[key] = value;
                            break;
                        }
                    }
                }
                else if (typeSize > typeTag.Length && typeTag.ArrayEqual(data, offset, typeTag.Length))
                {
                    m_Tag = new CommandTag(connection, Encoding.UTF8.GetString(data, offset + typeTag.Length, typeSize - typeTag.Length));
                }
                else
                {
                    throw new ArgumentException("undefined type for: " + Encoding.UTF8.GetString(data, offset, typeSize));
                }
                offset += typeSize;
            }

            throw new ArgumentException("request not complete");
        }

        public static bool IsRequestComplete(byte[] data, int offset, int endOffset)
        {
            while (offset < endOffset)
            {
                int typeSize = data.ReadEncodedLength(ref offset, ref endOffset);
                if (typeSize < 0)
                    return false;
                if (typeSize == 0)
                    return true;
                offset += typeSize;
            }

            return false;
        }
    }
}
