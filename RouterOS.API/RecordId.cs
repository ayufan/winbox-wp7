﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RouterOS.API;

namespace RouterOS
{
    public struct RecordId
    {
        public Int64 Index { get; internal set; }
        public Connection Connection { get; internal set; }

        public RecordId(int index)
            : this()
        {
            Index = index;
        }

        public static RecordId Parse(string value, Connection conn)
        {
            if (String.IsNullOrEmpty(value) || value[0] != '*')
                throw new ArgumentException();
            if (conn == null)
                throw new ArgumentException();
            return new RecordId()
            {
                Index = Convert.ToInt64(value.Substring(1), 16),
                Connection = conn
            };
        }

        public override string ToString()
        {
            return "*" + Index.ToString("X");
        }

        public static implicit operator RecordId(int value)
        {
            return new RecordId() { Index = value };
        }

        public static implicit operator string(RecordId value)
        {
            return value.ToString();
        }

        public static bool operator ==(RecordId a, RecordId b)
        {
            return a.Index == b.Index && a.Connection == b.Connection;
        }

        public static bool operator !=(RecordId a, RecordId b)
        {
            return a.Index != b.Index || a.Connection != b.Connection;
        }

        public override int GetHashCode()
        {
            return Index.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj is RecordId)
            {
                return this == (RecordId)obj;
            }
            return false;
        }
    }
}
