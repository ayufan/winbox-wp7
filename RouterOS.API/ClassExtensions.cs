﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.IO;
using System.Text;

namespace RouterOS.API
{
    internal static class ClassExtensions
    {
        internal static bool ArrayEqual(this byte[] a, byte[] b, int bOffset, int bSize)
        {
            if (bSize != a.Length)
                return false;
            for (int i = 0; i < bSize; ++i)
                if (a[i] != b[i + bOffset])
                    return false;
            return true;
        }

        internal static int ReadEncodedLength(this byte[] buffer, ref int startOffset, ref int endOffset)
        {
            return ReadEncodedLength(buffer, ref startOffset, ref endOffset, false);
        }

        internal static int ReadEncodedLength(this byte[] buffer, ref int startOffset, ref int endOffset, bool printDebug)
        {
            if (startOffset + 1 > endOffset)
                return -1;

            int offset = startOffset;

            // read string length
            int len = buffer[offset++];
            if ((len & 0x80) == 0x00)
            {
            }
            else if ((len & 0xC0) == 0x80)
            {
                if (offset + 1 > endOffset)
                    return -1;
                len &= ~(int)0xC0;
                len = (len << 8) + buffer[offset++];
            }
            else if ((len & 0xE0) == 0xC0)
            {
                if (offset + 2 > endOffset)
                    return -1;
                len &= ~(int)0xE0;
                len = (len << 8) + buffer[offset++];
                len = (len << 8) + buffer[offset++];
            }
            else if ((len & 0xF0) == 0xE0)
            {
                if (offset + 3 > endOffset)
                    return -1;
                len &= ~(int)0xF0;
                len = (len << 8) + buffer[offset++];
                len = (len << 8) + buffer[offset++];
                len = (len << 8) + buffer[offset++];
            }
            else
            {
                if (offset + 4 > endOffset)
                    return -1;
                len = buffer[offset++];
                len = (len << 8) + buffer[offset++];
                len = (len << 8) + buffer[offset++];
                len = (len << 8) + buffer[offset++];
            }

            startOffset = offset;

            if (startOffset + len > endOffset)
                return -1;

#if TRACE
            if (printDebug)
            {
                if (len > 0)
                    System.Diagnostics.Debug.WriteLine(">> " + Encoding.UTF8.GetString(buffer, startOffset, len));
                else
                    System.Diagnostics.Debug.WriteLine(">>------------------------");
            }
#endif

            return len;
        }

        internal static void WriteEncodedString(this Stream stream, string text)
        {
            if (text == null)
            {
                stream.WriteByte(0);
#if TRACE
                System.Diagnostics.Debug.WriteLine("<<------------------------");
#endif
                return;
            }

            byte[] data = Encoding.UTF8.GetBytes(text);
            int size = data.Length;

#if TRACE
            System.Diagnostics.Debug.WriteLine("<< " + text);
#endif

            if (size < 0x80)
            {
                stream.WriteByte((byte)size);
            }
            else if (size < 0x4000)
            {
                stream.WriteByte((byte)((size >> 8) | 0x80));
                stream.WriteByte((byte)size);
            }
            else if (size < 0x200000)
            {
                stream.WriteByte((byte)((size >> 16) | 0xC0));
                stream.WriteByte((byte)(size >> 8));
                stream.WriteByte((byte)size);
            }
            else if (size < 0x10000000)
            {
                stream.WriteByte((byte)((size >> 24) | 0xC0));
                stream.WriteByte((byte)(size >> 16));
                stream.WriteByte((byte)(size >> 8));
                stream.WriteByte((byte)size);
            }
            else
            {
                stream.WriteByte((byte)(0xF0));
                stream.WriteByte((byte)(size >> 24));
                stream.WriteByte((byte)(size >> 16));
                stream.WriteByte((byte)(size >> 8));
                stream.WriteByte((byte)size);
            }

            stream.Write(data, 0, data.Length);
        }


        internal static TimeSpan ConvertToTimeSpan(string value)
        {
            int days = 0, hours = 0, minutes = 0, seconds = 0, milliseconds = 0;

            int weeksOffset = value.IndexOf('w');
            if (weeksOffset >= 0)
            {
                days += 7 * int.Parse(value.Substring(0, weeksOffset));
                value = value.Substring(weeksOffset + 1);
            }

            int daysOffset = value.IndexOf('d');
            if (daysOffset >= 0)
            {
                days += int.Parse(value.Substring(0, daysOffset));
                value = value.Substring(daysOffset + 1);
            }

            int hoursOffset = value.IndexOf('h');
            if (hoursOffset >= 0)
            {
                hours += int.Parse(value.Substring(0, hoursOffset));
                value = value.Substring(hoursOffset + 1);
            }

            int minutesOffset = value.IndexOf('h');
            if (minutesOffset >= 0)
            {
                minutes += int.Parse(value.Substring(0, minutesOffset));
                value = value.Substring(minutesOffset + 1);
            }

            int secondsOffset = value.IndexOf('h');
            if (secondsOffset >= 0)
            {
                seconds += int.Parse(value.Substring(0, secondsOffset));
                value = value.Substring(secondsOffset + 1);
            }

            if (hours == 0 && minutes == 0 && seconds == 0)
            {
                string[] dotsep = value.Split('.');
                string[] hms = dotsep[0].Split(':');

                if (hms.Length == 3)
                {
                    hours += int.Parse(hms[0]);
                    minutes += int.Parse(hms[1]);
                    seconds += int.Parse(hms[2]);
                }

                if (dotsep.Length > 1)
                    milliseconds += int.Parse(dotsep[1]);
            }

            return new TimeSpan(days, hours, minutes, seconds, milliseconds);
        }

        internal static string ConvertFromTimeSpan(TimeSpan ts)
        {
            string ret = "";

            if (ts.Days > 7)
            {
                ret += (ts.Days / 7) + "w";
            }

            if ((ts.Days % 7) > 0)
            {
                ret += (ts.Days % 7) + "d";
            }

            ret += String.Format("{0}:{1}:{2}", ts.Hours, ts.Minutes, ts.Seconds);

            if (ts.Milliseconds > 0)
            {
                ret += "." + ts.Milliseconds;
            }
            return ret;
        }
    }
}
