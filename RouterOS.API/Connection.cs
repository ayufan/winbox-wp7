﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.IO;
using System.Diagnostics;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using System.Linq;

namespace RouterOS.API
{
    /// <summary>
    /// Asynchronous response callback.
    /// </summary>
    /// <param name="conn">Connection which generated asynchronous response.</param>
    /// <param name="response">Received response.</param>
    public delegate void AsyncResponseCallback(Connection conn, Record response);

    public delegate void ConnectionExceptionCallback(Connection conn, Exception ex);
    public delegate void ConnectionCallback(Connection conn);

    /// <summary>
    /// Encapsulates MikroTik RouterOS interface using API.
    /// </summary>
    public abstract class Connection : IDisposable
    {
        private string m_Host;
        private ushort m_Port = 8728;
        private string m_Login = "admin";
        private string m_Password = "";
        private Dictionary<int, AsyncResponseCallback> m_Callbacks = new Dictionary<int, AsyncResponseCallback>();
        private int m_CallbackIndex = 1;
        private bool m_Ready;
        private Guid m_Uid = Guid.NewGuid();

        private Dictionary<string, bool> m_Packages = new Dictionary<string, bool>();
        private string m_Version;
        private string m_BoardName;
        private string m_ArchitectureName;
        private string m_Platform;

        public event ConnectionCallback Connecting;
        public event ConnectionExceptionCallback Established;
        public event ConnectionExceptionCallback Connected;
        public event ConnectionCallback Authenticating;
        public event ConnectionExceptionCallback Authenticated;
        public event ConnectionExceptionCallback Disconnected;
        public event ConnectionExceptionCallback ErrorOccurred;

        /// <summary>
        /// Router hostname.
        /// </summary>
        public string Host
        {
            get { return m_Host; }
            set { m_Host = value; }
        }

        /// <summary>
        /// API service port.
        /// </summary>
        public ushort Port
        {
            get { return m_Port; }
            set { m_Port = value; }
        }

        /// <summary>
        /// Router username.
        /// </summary>
        public string Login
        {
            get { return m_Login; }
            set { m_Login = value; }
        }

        /// <summary>
        /// Router password.
        /// </summary>
        public string Password
        {
            get { return m_Password; }
            set { m_Password = value; }
        }

        public string ArchitectureName
        {
            get { return m_ArchitectureName; }
        }

        public string BoardName
        {
            get { return m_BoardName; }
        }

        public string Version
        {
            get { return m_Version; }
        }

        public string Platform
        {
            get { return m_Platform; }
        }

        /// <summary>
        /// Indicates if object is connected and logged into RouterOS.
        /// </summary>
        public abstract bool IsConnecting
        {
            get;
        }

        /// <summary>
        /// Indicates if object is connected and logged into RouterOS.
        /// </summary>
        public abstract bool IsConnected
        {
            get;
        }

        /// <summary>
        /// Indicates if object is connected and logged into RouterOS.
        /// </summary>
        public bool IsReady
        {
            get { return IsConnected && m_Ready; }
        }

        protected abstract Stream BeginStream();
        protected abstract void EndStream(Stream stream);

        public IEnumerable<string> Packages
        {
            get { return m_Packages.Keys; }
        }

        public IEnumerable<string> EnabledPackages
        {
            get { return m_Packages.TakeWhile((item) => { return item.Value; }).Select((item) => { return item.Key; }); }
        }

        public bool IsEnabled(string package)
        {
            bool status = false;
            if (m_Packages.TryGetValue(package, out status))
                return status;
            return false;
        }

        protected CommandTag WriteCommand(Command command, AsyncResponseCallback callback)
        {
            Stream stream = BeginStream();
            lock (stream)
            {
                // build command
                command.Build(stream);

                CommandTag tag = new CommandTag();

                if (callback != null || !command.IsAsynchronous)
                {
                    tag = new CommandTag(this, ++m_CallbackIndex);
                    stream.WriteEncodedString(".tag=" + tag);

                    if (callback != null)
                    {
                        lock (m_Callbacks)
                        {
                            m_Callbacks[tag.Tag] = callback;
                        }
                    }
                }

                stream.WriteEncodedString(null);
                EndStream(stream);
                return tag;
            }
        }

        /// <summary>
        /// Executes asynchronous command.
        /// </summary>
        /// <param name="command">Command to be executed.</param>
        /// <param name="callback">Asynchronous callback to be executed.</param>
        /// <returns>Asynchronous operation identifier.</returns>
        public CommandTag ExecuteCommand(Command command, AsyncResponseCallback callback)
        {
            return WriteCommand(command, callback);
        }

        /// <summary>
        /// Executes command synchronously.
        /// </summary>
        /// <param name="command">Command to be executed.</param>
        /// <returns>Execution status and command records.</returns>
        public Response ExecuteCommand(Command command)
        {
            List<Record> records = new List<Record>();
            Record message = null;
            Exception ex = null;
            AutoResetEvent resetEvent = new AutoResetEvent(false);

            // send command, synchronous
            CommandTag tag = WriteCommand(command, (conn, record) =>
            {
                if (record.Type == RecordType.Done)
                {
                    if (message == null)
                        message = record;
                    resetEvent.Set();
                    return;
                }

                if (record.Type == RecordType.Error || record.Type == RecordType.Fatal)
                {
                    if (record.Contains("message"))
                        ex = new InvalidOperationException(record["message"]);
                    else
                        ex = new InvalidOperationException(record.Type.ToString());
                    return;
                }

                records.Add(record);
            });

            if (!tag.HasValue)
                return null;

            // watch for errors
            resetEvent.WaitOne();
            Clear(tag);

            // throw exception
            if (ex != null)
                throw ex;

            return new Response(message, records.ToArray());
        }

        protected void Clear(CommandTag tag)
        {
            lock (m_Callbacks)
            {
                m_Callbacks.Remove(tag.Tag);
            }
        }

        public void Cancel(CommandTag tagId)
        {
            if (tagId == null)
                return;
            if (!IsConnected)
                return;
            try
            {
                WriteCommand(new CancelCommand(tagId), null);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[CONN] Cancel: {0}", ex);
            }
        }

        public void Cancel(ref CommandTag tagId)
        {
            if (!tagId.HasValue)
                return;
            if (!IsConnected)
                return;
            try
            {
                CancelCommand cmd = new CancelCommand(tagId);
                tagId = new CommandTag();
                WriteCommand(cmd, null);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[CONN] Cancel: {0}", ex);
            }
        }

        /// <summary>
        /// Execute command synchronously returning only one record.
        /// </summary>
        /// <param name="command">Command to be executed.</param>
        /// <returns>First record of executed command or null if no record found.</returns>
        public Record ExecuteOneRecord(Command command)
        {
            Response response = ExecuteCommand(command);
            if (response.Records.Length != 0)
                return response.Records[0];
            return null;
        }

        protected void ProcessBuffer(ref byte[] buffer, ref int bufferSize)
        {
            int offset = 0;
            int endOffset = offset + bufferSize;

            while (offset < endOffset)
            {
                if (!Record.IsRequestComplete(buffer, offset, endOffset))
                    break;

                try
                {
                    Record record = new Record(this, buffer, ref offset, endOffset);
                    BlockDecoded(record);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("[DATA] " + ex.Message);
                }
            }

            if (offset > 0)
            {
                Array.Copy(buffer, offset, buffer, 0, endOffset - offset);
                bufferSize = endOffset - offset;
            }
        }

        private void BlockDecoded(Record record)
        {
            if (!record.Tag.HasValue)
                return;

            // execute callback
            AsyncResponseCallback callback;
            lock (m_Callbacks)
            {
                if (!m_Callbacks.TryGetValue(record.Tag.Tag, out callback))
                {
                    Debug.WriteLine("[WARN] No Callback for: {0}", record.Tag);
                    return;
                }

                // remove data
                if (record.Type == RecordType.Done)
                {
                    m_Callbacks.Remove(record.Tag.Tag);
                }
            }

            // dispatch execution
            callback(this, record);
        }

        protected void DoLogin()
        {
            // get challenge login
            OnAuthenticating();

            try
            {
                // try to log in
                Response login = ExecuteCommand(new ChallengeLogin());
                ExecuteCommand(new Login(m_Login, login.Ret, m_Password));
                m_Ready = true;

                // notify
                OnAuthenticated(null);
            }
            catch (Exception ex)
            {
                OnAuthenticated(ex);
                throw ex;
            }

            // get version
            Response resource = ExecuteCommand(new GetAllCommand(new string[] { "system", "resource" }));
            if (resource.RecordCount == 1)
            {
                Record record = resource.Records[0];
                record.TryGetValue("version", out m_Version);
                record.TryGetValue("board-name", out m_BoardName);
                record.TryGetValue("architecture-name", out m_ArchitectureName);
                record.TryGetValue("platform", out m_Platform);
            }

            Response packages = ExecuteCommand(new GetAllCommand(new string[] { "system", "package" }));

            m_Packages.Clear();

            foreach (Record package in packages)
            {
                m_Packages[package["name"]] = !bool.Parse(package["disabled"]);
            }
        }

        protected virtual void OnConnecting()
        {
            Debug.WriteLine("[CONN] OnConnecting");

            // connected
            if (Connecting != null)
                Connecting(this);
        }

        protected virtual void OnAuthenticating()
        {
            Debug.WriteLine("[CONN] OnAuthenticating");

            if (Authenticating != null)
                Authenticating(this);
        }

        protected virtual void OnAuthenticated(Exception ex)
        {
            Debug.WriteLine("[CONN] OnAuthenticated: {0}", ex);

            if (Authenticated != null)
                Authenticated(this, ex);
        }

        protected virtual void OnEstablished(Exception ex)
        {
            Debug.WriteLine("[CONN] OnEstablished: {0}", ex);

            // connected
            if (Established != null)
                Established(this, ex);
        }

        protected virtual void OnConnected(Exception ex)
        {
            Debug.WriteLine("[CONN] OnConnected: {0}", ex);

            // connected
            if (Connected != null)
                Connected(this, ex);
        }

        protected virtual void OnDisconnected(Exception ex)
        {
            Debug.WriteLine("[CONN] OnDisconnected {0}", ex);

            // close all sessions
            lock (m_Callbacks)
            {
                Record record = new Record();

                foreach (var callback in Interlocked.Exchange(ref m_Callbacks, new Dictionary<int, AsyncResponseCallback>()))
                {
                    record.m_Connection = this;
                    record.m_Tag = new CommandTag(this, callback.Key);

                    // signal with error message
                    record.m_Type = RecordType.Error;
                    record.m_KeyValues["message"] = ex != null ? ex.Message : "session closed";
                    try { callback.Value(this, record); }
                    catch (Exception) { }

                    // signal with done
                    record.m_Type = RecordType.Done;
                    record.m_KeyValues.Clear();
                    try { callback.Value(this, record); }
                    catch (Exception) { }
                }
            }

            // disconnected
            if (Disconnected != null)
                Disconnected(this, ex);
        }

        protected virtual void OnErrorOccurred(Exception ex)
        {
            Debug.WriteLine("[CONN] OnErrorOccurred {0}", ex);

            // connected
            if (ErrorOccurred != null)
                ErrorOccurred(this, ex);
        }

        public abstract void Disconnect();

        protected virtual void Clear()
        {
            lock (m_Callbacks)
            {
                m_Callbacks = new Dictionary<int, AsyncResponseCallback>();
            }
        }

        public virtual void ScheduleConnect()
        {
            ThreadPool.QueueUserWorkItem((obj) => { try { Connect(); } catch (Exception) { } });
        }

        /// <summary>
        /// Explicitly connect to RouterOS.
        /// </summary>
        public abstract void Connect();

        /// <summary>
        /// Destroy connection.
        /// </summary>
        public virtual void Dispose()
        {
            Clear();

            GC.SuppressFinalize(this);
        }

        private Dictionary<KeyValuePair<Type, object>, object> m_ObjectCache = new Dictionary<KeyValuePair<Type, object>, object>();

        public void ClearCache()
        {
            m_ObjectCache = new Dictionary<KeyValuePair<Type, object>, object>();
        }

        public bool GetObjectFromCache(Type type, object name, out object obj)
        {
            KeyValuePair<Type, Object> key = new KeyValuePair<Type, object>(type, name);
            return m_ObjectCache.TryGetValue(key, out obj);
        }

        public void AddObjectToCache(Type type, object name, object obj)
        {
            KeyValuePair<Type, Object> key = new KeyValuePair<Type, object>(type, name);
            m_ObjectCache[key] = obj;
        }
    }
}
