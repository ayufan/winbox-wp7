﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RouterOS.API
{
    public struct CommandKey
    {
        private static Dictionary<int, string> hashToKey = new Dictionary<int, string>();

        private int hash;
        private string text;

        public int Hash
        {
            get { return hash; }
        }

        public CommandKey(int hash)
        {
            this.hash = hash;
            hashToKey.TryGetValue(this.hash, out this.text);
        }

        public CommandKey(byte[] data)
            : this(data, 0, data.Length)
        {
        }

        public CommandKey(byte[] data, int offset, int size)
        {
            this.hash = ComputeHash(data, offset, size);

            if (!hashToKey.TryGetValue(this.hash, out this.text))
            {
                this.text = Encoding.UTF8.GetString(data, offset, size);
                hashToKey[this.hash] = this.text;
            }
        }

        public CommandKey(string text)
        {
            this.hash = ComputeHash(text);

            if (!hashToKey.TryGetValue(this.hash, out this.text))
            {
                this.text = text;
                hashToKey[this.hash] = this.text;
            }
        }

        public static int GetHash(byte[] data)
        {
            return new CommandKey(data).Hash;
        }

        public static int GetHash(byte[] data, int offset, int size)
        {
            return new CommandKey(data, offset, size).Hash;
        }

        public static int GetHash(string text)
        {
            return new CommandKey(text).Hash;
        }

        public override string ToString()
        {
            if (text == null)
                return "undefined";
            return text;
        }

        public override int GetHashCode()
        {
            return hash;
        }

        public override bool Equals(object obj)
        {
            if (obj is CommandKey && (CommandKey)obj == this)
                return true;
            if (obj is string && this.hash == ComputeHash((string)obj))
                return true;
            if (obj == null)
                return false;
            return base.Equals(obj);
        }

        public static implicit operator CommandKey(string text)
        {
            return new CommandKey(text);
        }

        public static implicit operator string(CommandKey key)
        {
            return key.ToString();
        }

        public static bool operator ==(CommandKey a, CommandKey b)
        {
            return a.hash == b.hash;
        }

        public static bool operator !=(CommandKey a, CommandKey b)
        {
            return a.hash != b.hash;
        }

        public static int ComputeHash(byte[] data, int offset, int size)
        {
            unchecked
            {
                const int p = 16777619;
                int hash = (int)2166136261;

                for (int i = 0; i < size; i++)
                    hash = (hash ^ data[offset + i]) * p;

                hash += hash << 13;
                hash ^= hash >> 7;
                hash += hash << 3;
                hash ^= hash >> 17;
                hash += hash << 5;
                return hash;
            }
        }

        public static int ComputeHash(byte[] data)
        {
            return ComputeHash(data, 0, data.Length);
        }

        public static int ComputeHash(string data)
        {
            unchecked
            {
                const int p = 16777619;
                int hash = (int)2166136261;

                for (int i = 0; i < data.Length; i++)
                    hash = (hash ^ (byte)data[i]) * p;

                hash += hash << 13;
                hash ^= hash >> 7;
                hash += hash << 3;
                hash ^= hash >> 17;
                hash += hash << 5;
                return hash;
            }
        }
    }
}
