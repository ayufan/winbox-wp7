﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace RouterOS.API
{
    public abstract class Command
    {
        public abstract bool Build(Stream stream);

        public virtual bool IsAsynchronous
        {
            get { return false; }
        }
    }

    public class ChallengeLogin : Command
    {
        public override bool Build(Stream stream)
        {
            stream.WriteEncodedString("/login");
            return true;
        }
    }

    public sealed class Login : ChallengeLogin
    {
        private string m_Name;
        private string m_Response;

        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }

        public string Response
        {
            get { return m_Response; }
            set { m_Response = value; }
        }

        public void BuildResponse(string challenge, string password)
        {
            // save password
            byte[] data = new byte[1 + password.Length + challenge.Length / 2];
            Encoding.UTF8.GetBytes(password, 0, password.Length, data, 1);

            // get challenge
            for (int i = 0; i < challenge.Length; i += 2)
                data[1 + password.Length + i / 2] = Convert.ToByte(challenge.Substring(i, 2), 16);

            // get response
            byte[] response = new MD5Managed().ComputeHash(data);
            m_Response = BitConverter.ToString(response).Replace("-", "").ToLower();
        }

        public override bool Build(Stream stream)
        {
            if(!base.Build(stream))
                return false;
            if (m_Name != null)
                stream.WriteEncodedString("=name=" + m_Name);
            if (m_Response != null)
                stream.WriteEncodedString("=response=00" + m_Response);
            return true;
        }

        public Login()
        {
        }

        public Login(string name, string challenge, string password)
        {
            m_Name = name;
            BuildResponse(challenge, password);
        }
    }

    public abstract class ScopeCommand : Command
    {
        private string[] m_Scope;

        public string[] Scope
        {
            get { return m_Scope; }
            set { m_Scope = value; }
        }

        /// <summary>
        /// Command action, ie. getall, listen, set.
        /// </summary>
        public abstract string Action
        {
            get;
        }

        public override bool Build(Stream stream)
        {
            if(Action != null)
                stream.WriteEncodedString(String.Format("/{0}/{1}", String.Join("/", m_Scope), Action));
            else
                stream.WriteEncodedString(String.Format("/{0}", String.Join("/", m_Scope)));
            return true;
        }
    }

    public abstract class ValueCommand : ScopeCommand
    {
        private Dictionary<CommandKey, string> m_KeyValues = new Dictionary<CommandKey, string>();

        public string this[CommandKey key]
        {
            get { return m_KeyValues[key]; }
            set { m_KeyValues[key] = value; }
        }

        public bool Contains(CommandKey key)
        {
            return m_KeyValues.ContainsKey(key);
        }

        public IDictionary<CommandKey, string> KeyValues
        {
            get { return m_KeyValues; }
        }

        public override bool Build(Stream stream)
        {
            if (!base.Build(stream))
                return false;
            foreach (KeyValuePair<CommandKey, string> keyValue in m_KeyValues)
                stream.WriteEncodedString(String.Format("={0}={1}", keyValue.Key.ToString(), keyValue.Value));
            return true;
        }
    }

    public abstract class QueryCommand : ValueCommand
    {
        private QueryList m_Query = new QueryList();

        public QueryList Query
        {
            get { return m_Query; }
        }

        public override bool Build(Stream stream)
        {
            if (!base.Build(stream))
                return false;
            if (m_Query != null)
                m_Query.Build(stream);
            return true;
        }
    }

    public class GetAllCommand : QueryCommand
    {
        private List<CommandKey> m_PropList = new List<CommandKey>();

        public List<CommandKey> PropList
        {
            get { return m_PropList; }
        }

        public override string Action
        {
            get { return "getall"; }
        }

        public bool Statistics
        {
            get { return Contains("stats") && bool.Parse(this["stats"]); }
            set { this["stats"] = value.ToString(); }
        }

        public bool Detail
        {
            get { return Contains("detail") && bool.Parse(this["detail"]); }
            set { this["detail"] = value.ToString(); }
        }

        public TimeSpan Interval
        {
            get { return Contains("interval") ? ClassExtensions.ConvertToTimeSpan(this["interval"]) : TimeSpan.Zero; }
            set { this["interval"] = ClassExtensions.ConvertFromTimeSpan(value); }
        }

        public override bool Build(Stream stream)
        {
            if (!base.Build(stream))
                return false;
            if (m_PropList.Count != 0)
                stream.WriteEncodedString(".proplist=" + String.Join(",", m_PropList.ToArray()));
            return true;
        }

        public GetAllCommand()
        {
        }

        public GetAllCommand(string[] scope, params Query[] query)
        {
            Scope = scope;
            Query.AddRange(query);
        }

        public GetAllCommand(string[] scope, RecordId objectId, params Query[] query)
        {
            Scope = scope;
            Query.AddRange(query);
            Query.AddItem(objectId);
        }
    }

    public static class CommandHelpers
    {
        public static Response GetAll(this Connection connection, string[] scope, params Query[] query)
        {
            return connection.ExecuteCommand(new GetAllCommand(scope, query));
        }

        public static Response GetAll(this Connection connection, string[] scope, RecordId objectId, params Query[] query)
        {
            return connection.ExecuteCommand(new GetAllCommand(scope, objectId, query));
        }

        public static CommandTag GetAll(this Connection connection, string[] scope, AsyncResponseCallback callback, params Query[] query)
        {
            return connection.ExecuteCommand(new GetAllCommand(scope, query), callback);
        }

        public static CommandTag GetAll(this Connection connection, string[] scope, TimeSpan interval, AsyncResponseCallback callback, params Query[] query)
        {
            return connection.ExecuteCommand(new GetAllCommand(scope, query) { Interval = interval }, callback);
        }

        public static CommandTag GetAll(this Connection connection, string[] scope, RecordId objectId, AsyncResponseCallback callback, params Query[] query)
        {
            return connection.ExecuteCommand(new GetAllCommand(scope, objectId, query), callback);
        }

        public static CommandTag GetAll(this Connection connection, string[] scope, RecordId objectId, TimeSpan interval, AsyncResponseCallback callback, params Query[] query)
        {
            return connection.ExecuteCommand(new GetAllCommand(scope, objectId, query) { Interval = interval }, callback);
        }

        public static CommandTag Listen(this Connection connection, string[] scope, AsyncResponseCallback callback, params Query[] query)
        {
            return connection.ExecuteCommand(new ListenCommand(scope, query), callback);
        }

        public static CommandTag Listen(this Connection connection, string[] scope, RecordId objectId, AsyncResponseCallback callback, params Query[] query)
        {
            return connection.ExecuteCommand(new ListenCommand(scope, objectId, query), callback);
        }
    }

    public sealed class SetCommand : ValueCommand
    {
        public override string Action
        {
            get { return "set"; }
        }

        public SetCommand()
        {
        }

        public SetCommand(string[] scope)
        {
            Scope = scope;
        }
    }

    public sealed class AddCommand : ValueCommand
    {
        public override string Action
        {
            get { return "add"; }
        }

        public AddCommand()
        {
        }

        public AddCommand(string[] scope)
        {
            Scope = scope;
        }
    }
    
    public abstract class IdsCommand : ScopeCommand
    {
        private RecordId[] m_Items;

        public RecordId[] Items
        {
            get { return m_Items; }
            set { m_Items = value; }
        }

        public override bool Build(Stream stream)
        {
            if (!base.Build(stream))
                return false;

            if (m_Items != null)
            {
                stream.WriteEncodedString("=numbers=" + string.Join(",", m_Items));
                return true;
            }
            return false;
        }
        
        public IdsCommand()
        {
        }
    }

    public sealed class MoveCommand : IdsCommand
    {
        private RecordId m_BeforeItem;

        public RecordId BeforeItem
        {
            get { return m_BeforeItem; }
            set { m_BeforeItem = value; }
        }

        public override string Action
        {
            get { return "move"; }
        }

        public override bool Build(Stream stream)
        {
            if (!base.Build(stream))
                return false;

            if (m_BeforeItem != null)
            {
                stream.WriteEncodedString("=destination=" + m_BeforeItem.ToString());
                return true;
            }
            return false;
        }
        
        public MoveCommand()
        {
        }

        public MoveCommand(string[] scope, RecordId beforeItem, params RecordId[] items)
        {
            Scope = scope;
            Items = items;
        }
    }

    public sealed class RemoveCommand : IdsCommand
    {
        public override string Action
        {
            get { return "remove"; }
        }

        public RemoveCommand()
        {
        }

        public RemoveCommand(string[] scope, params RecordId[] items)
        {
            Scope = scope;
            Items = items;
        }
    }

    public sealed class EnableCommand : IdsCommand
    {
        public override string Action
        {
            get { return "enable"; }
        }

        public EnableCommand()
        {
        }

        public EnableCommand(string[] scope, params RecordId[] items)
        {
            Scope = scope;
            Items = items;
        }
    }
    
    public sealed class DisableCommand : IdsCommand
    {
        public override string Action
        {
            get { return "disable"; }
        }

        public DisableCommand()
        {
        }

        public DisableCommand(string[] scope, params RecordId[] items)
        {
            Scope = scope;
            Items = items;
        }
    }

    public sealed class UnsetCommand : ScopeCommand
    {
        private List<RecordId> m_Items = new List<RecordId>();
        private List<string> m_ValueNames = new List<string>();

        public UnsetCommand(string[] scope)
        {
            Scope = scope;
        }

        public List<RecordId> Items
        {
            get { return m_Items; }
        }

        public List<string> ValueNames
        {
            get { return m_ValueNames; }
        }

        public override string Action
        {
            get { return "unset"; }
        }

        public override bool Build(Stream stream)
        {
            if (!base.Build(stream))
                return false;

            if (m_Items != null && m_ValueNames != null)
            {
                stream.WriteEncodedString("=numbers=" + String.Join(",", m_Items));
                stream.WriteEncodedString("=value-name=" + String.Join(",", m_ValueNames.ToArray()));
                return true;
            }

            return false;
        }
    }

    public sealed class ListenCommand : QueryCommand
    {
        public override string Action
        {
            get { return "listen"; }
        }

        public bool Detail
        {
            get { return Contains("detail") && bool.Parse(this["detail"]); }
            set { this["detail"] = value.ToString(); }
        }

        public ListenCommand()
        {
        }

        public ListenCommand(string[] scope, params Query[] query)
        {
            Scope = scope;
            Query.AddRange(query);
        }

        public ListenCommand(string[] scope, RecordId objectId, params Query[] query)
        {
            Scope = scope;
            Query.AddRange(query);
            Query.AddItem(objectId);
        }

        public override bool IsAsynchronous
        {
            get { return true; }
        }
    }

    public sealed class CancelCommand : Command
    {
        private CommandTag m_Tag;

        public CommandTag Tag
        {
            get { return m_Tag; }
            set { m_Tag = value; }
        }

        public override bool Build(Stream stream)
        {
            stream.WriteEncodedString("/cancel");
            stream.WriteEncodedString("=tag=" + m_Tag.ToString());
            return true;
        }

        public CancelCommand(CommandTag tag)
        {
            m_Tag = tag;
        }

        public override bool IsAsynchronous
        {
            get { return true; }
        }
    }

    public class CustomCommand : ValueCommand
    {
        public override string Action
        {
            get { return null; }
        }

        public CustomCommand()
        {
        }
    }
}
