﻿using System;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Threading;
using System.Diagnostics;
using Renci.SshNet;
using Renci.SshNet.Common;

namespace RouterOS.API
{
    public class SshStreamConnection : Connection
    {
        private SshClient sshClient;
        private ShellStream sshStream;
        private byte[] m_ReaderBuffer = new byte[4096];
        private int m_ReaderSize = 0;

        public string SshHost { get; set; }
        public string SshUser { get; set; }
        public string SshPassword { get; set; }
        public ushort SshPort { get; set; }

        public event EventHandler<HostKeyEventArgs> HostKeyReceived;

        public override bool IsConnecting
        {
            get { return sshClient != null && sshStream == null; }
        }

        public override bool IsConnected
        {
            get { return sshClient != null && sshStream != null; }
        }

        public Stream Stream
        {
            get { return sshStream; }
        }

        public SshStreamConnection()
        {
        }

        protected override Stream BeginStream()
        {
            return sshStream;
        }

        protected override void EndStream(Stream stream)
        {
            stream.Flush();
        }

        protected override void Clear()
        {
            base.Clear();

            lock (this)
            {
                if (sshStream != null)
                {
                    lock (sshStream)
                    {
                        sshStream.DataReceived -= OnDataReceived;
                        sshStream.ErrorOccurred -= OnErrorOccurred;
                        sshStream.Dispose();
                        sshStream = null;
                    }
                }

                if (sshClient != null)
                {
                    lock (sshClient)
                    {
                        sshClient.HostKeyReceived -= OnHostKeyReceived;
                        sshClient.Dispose();
                        sshClient = null;
                    }
                }
            }
        }

        public override void Connect()
        {
            lock (this)
            {
                if (IsConnected)
                    throw new ArgumentException("ssh already connected");
                if (IsConnecting)
                    throw new ArgumentException("ssh is connecting");

                Clear();

                OnConnecting();

                // open ssh connection
                try
                {
                    sshClient = new SshClient(SshHost, SshPort, SshUser, SshPassword);
                    sshClient.HostKeyReceived += OnHostKeyReceived;
                    sshClient.Connect();

                    // notify about established connection
                    OnEstablished(null);

                    sshClient.ErrorOccurred += OnErrorOccurred;
                    sshClient.HostKeyReceived -= OnHostKeyReceived;
                }
                catch (Exception ex)
                {
                    Clear();
                    OnEstablished(ex);
                    OnConnected(ex);
                    throw ex;
                }

                try
                {
                    sshStream = sshClient.CreateDirectTcpipStream(Host, Port, new IPEndPoint(IPAddress.Loopback, Port));
                    sshStream.DataReceived += OnDataReceived;
                    sshStream.ErrorOccurred += OnErrorOccurred;

                    // do login
                    DoLogin();
                    OnConnected(null);
                }
                catch (Exception ex)
                {
                    Clear();
                    OnConnected(ex);
                    throw ex;
                }
            }
        }

        private void OnHostKeyReceived(object sender, Renci.SshNet.Common.HostKeyEventArgs e)
        {
            if (HostKeyReceived != null)
                HostKeyReceived(sender, e);
        }

        private void OnErrorOccurred(object sender, Renci.SshNet.Common.ExceptionEventArgs e)
        {
            Debug.WriteLine("[SSH] " + e.Exception.Message);
            base.OnErrorOccurred(e.Exception);
            OnDisconnected(e.Exception);
        }

        private void OnDataReceived(object sender, Renci.SshNet.Common.ShellDataEventArgs e)
        {
            if (e.Data == null)
                return;

            if (sshStream == null)
                return;

            lock (sshStream)
            {
                if (e.Data.Length == 0)
                {
                    OnDisconnected(null);
                    return;
                }

                if (m_ReaderSize + e.Data.Length > m_ReaderBuffer.Length)
                    Array.Resize(ref m_ReaderBuffer, m_ReaderSize + e.Data.Length + 4096);

                Array.Copy(e.Data, 0, m_ReaderBuffer, m_ReaderSize, e.Data.Length);
                m_ReaderSize += e.Data.Length;

                ProcessBuffer(ref m_ReaderBuffer, ref m_ReaderSize);
            }
        }

        public override void Disconnect()
        {
            lock (this)
            {
                ShellStream stream = Interlocked.Exchange(ref sshStream, null);
                SshClient client = Interlocked.Exchange(ref sshClient, null);

                if (stream != null)
                {
                    lock (stream)
                    {
                        stream.ErrorOccurred -= OnErrorOccurred;
                        stream.DataReceived -= OnDataReceived;
                        // stream.Dispose();
                    }
                }

                if (client != null)
                {
                    lock (client)
                    {
                        client.ErrorOccurred -= OnErrorOccurred;
                        client.HostKeyReceived -= HostKeyReceived;
                        if (client.IsConnected)
                            client.Disconnect();
                        // client.Dispose();
                    }
                }

                OnDisconnected(null);
            }
        }
    }
}
