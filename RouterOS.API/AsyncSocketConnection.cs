﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.IO;
using System.Diagnostics;
using System.Threading;
using System.Windows;
using System.Windows.Threading;

namespace RouterOS.API
{
    /// <summary>
    /// Encapsulates MikroTik RouterOS interface using API.
    /// </summary>
    public sealed class AsyncSocketConnection : Connection
    {
        private Socket m_Connection;
        private MemoryStream m_Stream = new MemoryStream();

        /// <summary>
        /// Indicates if object is connected and logged into RouterOS.
        /// </summary>
        public override bool IsConnecting
        {
            get { return m_Connection != null && !m_Connection.Connected; }
        }

        /// <summary>
        /// Indicates if object is connected and logged into RouterOS.
        /// </summary>
        public override bool IsConnected
        {
            get { return m_Connection != null && m_Connection.Connected; }
        }

        protected override void Clear()
        {
            base.Clear();

            lock (this)
            {
                m_Connection = null;
            }
        }

        /// <summary>
        /// Explicitly connect to RouterOS.
        /// </summary>
        public override void Connect()
        {
            AutoResetEvent connectEvent = new AutoResetEvent(false);
            Exception connectException = null;

            lock (this)
            {
                if (IsConnected)
                    throw new InvalidOperationException("already connected");

                if (IsConnecting)
                    throw new InvalidOperationException("already connecting");

                Clear();

                // notify about connecting
                OnConnecting();

                // open connection
                SocketAsyncEventArgs args = new SocketAsyncEventArgs();
                args.RemoteEndPoint = new DnsEndPoint(Host, Port);

                args.Completed += (sender, e) =>
                {
                    // error occurred
                    if (e.SocketError != SocketError.Success)
                    {
                        lock (this)
                        {
                            m_Connection = null;
                        }
                        OnEstablished(new SocketException((Int32)e.SocketError));
                        connectException = new SocketException((Int32)e.SocketError);
                        connectEvent.Set();
                        return;
                    }

                    try
                    {
                        OnEstablished(null);
                        m_Stream = new MemoryStream();
                        ThreadPool.QueueUserWorkItem(ReadFromSocket, args.ConnectSocket);
                        DoLogin();
                    }
                    catch (Exception ex)
                    {
                        connectException = ex;
                    }
                    finally
                    {
                        connectEvent.Set();
                    }
                };

                // connect to routeros
                m_Connection = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                m_Connection.ReceiveBufferSize = 32768;
                m_Connection.ConnectAsync(args);
            }

            // wait for connection
            connectEvent.WaitOne();

            OnConnected(connectException);

            if (connectException != null)
                throw connectException;
        }

        protected override void OnDisconnected(Exception ex)
        {
            lock (this)
            {
                m_Connection = null;
            }
            base.OnDisconnected(ex);
        }

        private void ReadFromSocket(object obj)
        {
            Socket socket = (Socket)obj;

            try
            {
                byte[] buffer = new byte[socket.ReceiveBufferSize];
                int size = 0;

                AutoResetEvent resetEvent = new AutoResetEvent(false);

                while (true)
                {
                    if (size + 4096 > buffer.Length)
                        Array.Resize(ref buffer, buffer.Length + 4096);

                    SocketAsyncEventArgs args = new SocketAsyncEventArgs();
                    args.Completed += (sender, e) => resetEvent.Set();
                    args.SetBuffer(buffer, size, buffer.Length - size);
                    socket.ReceiveAsync(args);

                    resetEvent.WaitOne();

                    if (args.SocketError != SocketError.Success)
                    {
                        if (socket == m_Connection)
                        {
                            OnErrorOccurred(new SocketException((int)args.SocketError));
                            OnDisconnected(new SocketException((int)args.SocketError));
                        }
                        return;
                    }

                    size += args.BytesTransferred;

                    if (args.BytesTransferred == 0)
                    {
                        if (socket == m_Connection)
                        {
                            OnDisconnected(null);
                        }
                        return;
                    }

                    ProcessBuffer(ref buffer, ref size);
                }
            }
            catch (Exception ex)
            {
                if (m_Connection == socket)
                {
                    OnErrorOccurred(ex);
                    OnDisconnected(ex);
                }
            }
        }

        protected override Stream BeginStream()
        {
            return m_Stream;
        }

        protected override void EndStream(Stream stream)
        {
            if (stream != m_Stream)
                return;
            if (m_Stream.Length == 0)
                return;

            Socket socket = m_Connection;

            // push to socket
            if (socket != null && socket.Connected)
            {
                SocketAsyncEventArgs args = new SocketAsyncEventArgs();
                args.SetBuffer(m_Stream.ToArray(), 0, (int)stream.Length);
                socket.SendAsync(args);
            }
            else
            {
                throw new SocketException((int)SocketError.NotConnected);
            }

            // clear stream
            m_Stream.SetLength(0);
        }

        /// <summary>
        /// Disconnect from RouterOS.
        /// </summary>
        public override void Disconnect()
        {
            lock (this)
            {
                Socket connection = Interlocked.Exchange(ref m_Connection, null);

                if (connection != null)
                {
                    if (connection.Connected)
                    {
                        connection.Shutdown(SocketShutdown.Both);
                        connection = null;
                    }
                    else
                    {
                        connection.Close();
                    }

                    OnDisconnected(null);
                }
            }
        }

        /// <summary>
        /// Create connection to RouterOS using specified parameters.
        /// </summary>
        /// <param name="host">RouterOS hostname.</param>
        /// <param name="port">API service port.</param>
        /// <param name="login">RouterOS username.</param>
        /// <param name="password">RouterOS password.</param>
        public AsyncSocketConnection(string host, ushort port, string login, string password)
        {
            Host = host;
            Port = port;
            Login = login;
            Password = password;
            Connect();
        }

        /// <summary>
        /// Create connection to RouterOS using specified parameters.
        /// </summary>
        /// <param name="host">RouterOS hostname.</param>
        /// <param name="login">RouterOS username.</param>
        /// <param name="password">RouterOS password.</param>
        public AsyncSocketConnection(string host, string login, string password)
            : this(host, 8728, login, password)
        {
        }

        /// <summary>
        /// Create connection object, needs to be filled.
        /// </summary>
        public AsyncSocketConnection()
        {
        }
    }
}
