﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.IO;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Threading;

namespace RouterOS.API
{
    public class StreamConnection : Connection
    {
        private Stream m_Stream;
        private byte[] m_ReaderBuffer = new byte[4096];
        private int m_ReaderSize = 0;

        public Stream Stream
        {
            get { return m_Stream; }
            set
            {
                if (IsReady)
                    throw new InvalidProgramException();
                m_Stream = value;
            }
        }
        
        /// <summary>
        /// Indicates if object is connected and logged into RouterOS.
        /// </summary>
        public override bool IsConnecting
        {
            get { return m_Stream == null; }
        }

        /// <summary>
        /// Indicates if object is connected and logged into RouterOS.
        /// </summary>
        public override bool IsConnected
        {
            get { return m_Stream != null; }
        }

        protected override void Clear()
        {
            base.Clear();
            if (m_Stream != null)
            {
                m_Stream.Close();
            }
            m_ReaderSize = 0;
        }

        /// <summary>
        /// Explicitly connect to RouterOS.
        /// </summary>
        public override void Connect()
        {
            if (IsConnected)
                throw new SocketException();

            Clear();
            OnConnected(null);
        }

        protected void PushData(byte[] buffer)
        {
            if (m_ReaderSize + buffer.Length > m_ReaderBuffer.Length)
                Array.Resize(ref m_ReaderBuffer, m_ReaderSize + buffer.Length + 4096);

            Array.Copy(buffer, 0, m_ReaderBuffer, m_ReaderSize, buffer.Length);
            m_ReaderSize += buffer.Length;

            ProcessBuffer(ref m_ReaderBuffer, ref m_ReaderSize);
        }

        protected override Stream BeginStream()
        {
            return m_Stream;
        }

        protected override void EndStream(Stream stream)
        {
        }
        
        /// <summary>
        /// Disconnect from RouterOS.
        /// </summary>
        public override void Disconnect()
        {
            Clear();
        }

        /// <summary>
        /// Create connection to RouterOS using specified parameters.
        /// </summary>
        public StreamConnection(Stream stream)
        {
            m_Stream = stream;
            Connect();
        }

        /// <summary>
        /// Create connection object, needs to be filled.
        /// </summary>
        public StreamConnection()
        {
        }
    }
}
