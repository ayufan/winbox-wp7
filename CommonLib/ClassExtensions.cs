﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.IO.IsolatedStorage;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;

namespace CommonLib
{
    public static class ClassExtensions
    {
        public static T GetObjectOfType<T>(this IsolatedStorageSettings settings, string key) where T : class,new()
        {
            T value;
            if (!settings.TryGetValue<T>(key, out value))
            {
                value = new T();
                settings.Add(key, value);
            }
            return value;
        }

        public static T GetTag<T>(this object value) where T : class
        {
            FrameworkElement element = value as FrameworkElement;
            if (element == null)
                return null;
            T tag = element.Tag as T;
            return tag;
        }

        public static Page GetFrame(this UserControl control)
        {
            FrameworkElement parent = control;
            do
            {
                if (parent is Page)
                    return parent as Page;
                parent = parent.Parent as FrameworkElement;
            }
            while (parent != null);
            return null;
        }

        public static IEnumerable<T> GetValues<T>()
        {
            Type enumType = typeof(T);
            if (!enumType.IsEnum)
            {
                throw new ArgumentException("Type '" + enumType.Name + "' is not an enum");
            }

            IEnumerable<FieldInfo> fields = enumType.GetFields().Where(field => field.IsLiteral);
            return fields.Select(field => field.GetValue(enumType)).Select(value => (T)value);
        }
    }
}
