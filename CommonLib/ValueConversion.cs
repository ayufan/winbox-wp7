﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Data;

namespace CommonLib
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public sealed class ValueConversionAttribute : Attribute
    {
        // Fields
        private Type _parameterType;
        private Type _sourceType;
        private Type _targetType;
        // Methods
        public ValueConversionAttribute(Type sourceType, Type targetType)
        {
            if (sourceType == null)
            {
                throw new ArgumentNullException("sourceType");
            }
            if (targetType == null)
            {
                throw new ArgumentNullException("targetType");
            }
            this._sourceType = sourceType;
            this._targetType = targetType;
        }
        public override int GetHashCode()
        {
            return (this._sourceType.GetHashCode() + this._targetType.GetHashCode());
        }
        // Properties
        public Type ParameterType
        {
            get
            {
                return this._parameterType;
            }
            set
            {
                this._parameterType = value;
            }
        }
        public Type SourceType
        {
            get
            {
                return this._sourceType;
            }
        }
        public Type TargetType
        {
            get
            {
                return this._targetType;
            }
        }
    }

    [ValueConversion(typeof(bool?), typeof(String))]
    public class DisabledToTextConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (((bool?)value).GetValueOrDefault())
                return "Enable";
            else
                return "Disable";
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if ((string)value == "Enable")
                return true;
            else
                return false;
        }
    }
}
