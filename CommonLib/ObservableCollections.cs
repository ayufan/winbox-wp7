﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace CommonLib
{
    public class UpdatableObservableCollection<T> : ObservableCollection<T>
    {
        public virtual int FastIndexOf(T item)
        {
            return IndexOf(item);
        }

        public void Update(T item)
        {
            int index = FastIndexOf(item);
            if (index >= 0)
            {
                NotifyCollectionChangedEventArgs e = new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, item, item, index);
                OnCollectionChanged(e);
            }
            else
            {
                Add(item);
            }
        }

        public void Update(T newItem, T oldItem)
        {
            if (oldItem != null)
            {
                int index = FastIndexOf(oldItem);

                if (index >= 0)
                {
                    SetItem(index, newItem);
                    return;
                }
            }

            Add(newItem);
        }

        public void UpdateByHash(T newItem)
        {
            int index = IndexOfHash(newItem);

            if (index >= 0)
            {
                SetItem(index, newItem);
                return;
            }

            Add(newItem);
        }

        public void ReplaceAt(int index, T newItem)
        {
            if (index >= 0)
                SetItem(index, newItem);
            else
                Add(newItem);
        }

        public int IndexOfHash(T item)
        {
            int itemHash = item.GetHashCode();

            for(int i = Count; i--> 0; )
            {
                if (this[i].GetHashCode() == itemHash)
                    return i;
            }
            return -1;
        }
    }

    public class SortedObservableCollection<T> : ObservableCollection<T>
    {
        public int IndexOf(string text)
        {
            for (int i = 0; i < Count; ++i)
                if (this[i].ToString() == text)
                    return i;
            return -1;
        }

        public T Find(string text)
        {
            for (int i = 0; i < Count; ++i)
                if (this[i].ToString() == text)
                    return this[i];
            return default(T);
        }

        public void Update(T item)
        {
            int index = IndexOf(item);
            if (index >= 0)
            {
                NotifyCollectionChangedEventArgs e = new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, item, item, index);
                OnCollectionChanged(e);
            }
            else
            {
                string itemText = item.ToString();

                for (int i = 0; i < Count; ++i)
                {
                    if (itemText.CompareTo(this[i].ToString()) < 0)
                    {
                        Insert(i, item);
                        return;
                    }
                }

                Add(item);
            }
        }
    }

    public class FastObservableCollection<T> : UpdatableObservableCollection<T>
    {
        private Dictionary<T, int> indices = new Dictionary<T, int>();

        public override int FastIndexOf(T item)
        {
            int index = 0;
            if (indices.TryGetValue(item, out index))
                return index;
            return -1;
        }

        protected override void InsertItem(int index, T item)
        {
            base.InsertItem(index, item);

            for (int i = index; i < Count; ++i)
                indices[this[index]] = index;
        }

        protected override void RemoveItem(int index)
        {
            indices.Remove(this[index]);

            base.RemoveItem(index);

            for (int i = index; i < Count; ++i)
                indices[this[index]] = index;
        }

        protected override void SetItem(int index, T item)
        {
            indices.Remove(this[index]);
            base.SetItem(index, item);
            indices[item] = index;
        }
    }

    public class UserObservableCollection<T> : FastObservableCollection<T>
    {
        public delegate bool IsItem(T item);

        private ObservableCollection<T> baseCol;
        private IsItem isItem;

        public UserObservableCollection(ObservableCollection<T> baseCol, IsItem isItem)
        {
            this.isItem = isItem;
            this.baseCol = baseCol;
            this.baseCol.CollectionChanged += baseCol_CollectionChanged;

            foreach (T item in baseCol)
            {
                if (isItem(item))
                    Add(item);
            }
        }

        private void baseCol_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (T item in e.NewItems)
                        if (isItem(item))
                            Add(item);
                    break;

                case NotifyCollectionChangedAction.Replace:
                    for (int i = 0; i < e.NewItems.Count; ++i)
                    {
                        T oldItem = (T)e.OldItems[i];
                        T newItem = (T)e.NewItems[i];

                        int index = FastIndexOf(oldItem);
                        if (isItem(newItem))
                            Update(newItem);
                        else
                            Remove(newItem);
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (T item in e.OldItems)
                        Remove(item);
                    break;

                case NotifyCollectionChangedAction.Reset:
                    Clear();
                    break;
            }
        }
    }
}
