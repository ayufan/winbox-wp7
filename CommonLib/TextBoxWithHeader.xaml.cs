﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace CommonLib
{
    public partial class TextBoxWithHeader : UserControl
    {
        public TextBoxWithHeader()
        {
            InitializeComponent();
            if (string.IsNullOrEmpty(this.Name))
                Header = "textBoxWithHeader";
            else
                Header = this.Name;
            textBlockHeader.Foreground = (Brush)Application.Current.Resources["PhoneSubtleBrush"];
        }

        public string Header
        {
            get
            {
                return textBlockHeader.Text;
            }
            set
            {
                textBlockHeader.Text = value;
            }
        }

        public string Text
        {
            get
            {
                return textBoxValue.Text;
            }
            set
            {
                textBoxValue.Text = value;
            }
        }

        private void UserControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            LayoutRoot.Width = this.Width;
            textBoxValue.Width = this.Width;
        }
    }
}
