﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RouterOS.API;
using RouterOS.Serialization;

namespace RouterOS
{
    public enum ARPMode
    {
        [RosValue("disabled")]
        Disabled,

        [RosValue("enabled")]
        Enabled,

        [RosValue("proxy-arp")]
        ProxyArp,

        [RosValue("reply-only")]
        ReplyOnly
    }

    public interface IInterface
    {
        RecordId? Id { get; }
        string Name { get; }
        string Type { get; }
    }

    [RosObject("interface", CanDisable = true, CanEnable = true, CanGet = true, CanSet = true, CanListen = true)]
    public class Interface : RosItemObject<Interface>, IInterface
    {
        [RosValue("name", IsKey=true)]
        public string Name { get; set; }

        [RosValue("type", IsDynamic = true)]
        public string Type { get; set; }

        [RosValue("bytes", IsDynamic = true)]
        public DblValue<int,int>? Bytes { get; set; }

        [RosValue("packets", IsDynamic = true)]
        public DblValue<int,int>? Packets { get; set; }

        [RosValue("drops", IsDynamic = true)]
        public DblValue<int,int>? Drops { get; set; }

        [RosValue("errors", IsDynamic = true)]
        public DblValue<int,int>? Errors { get; set; }

        [RosValue("mtu")]
        public int? Mtu { get; set; }

        [RosValue("l2mtu", IsDynamic = true)]
        public int? L2Mtu { get; set; }

        [RosValue("dynamic", IsDynamic = true)]
        public override bool? Dynamic { get; set; }

        [RosValue("disabled")]
        public override bool? Disabled { get; set; }

        [RosValue("invalid", IsDynamic = true)]
        public override bool? Invalid { get; set; }

        [RosValue("running", IsDynamic = true)]
        public override bool? Running { get; set; }

        [RosValue("comment", Default = "")]
        public override string Comment { get; set; }

        [RosValue("slave", IsDynamic=true)]
        public override bool? Slave { get; set; }

        [RosValue("rx-byte", IsDynamic = true, IsStatistics= true)]
        public long RxByte { get; set; }

        [RosValue("tx-byte", IsDynamic = true, IsStatistics = true)]
        public long TxByte { get; set; }

        [RosValue("rx-packet", IsDynamic = true, IsStatistics = true)]
        public long RxPacket { get; set; }

        [RosValue("tx-packet", IsDynamic = true, IsStatistics = true)]
        public long TxPacket { get; set; }

        public override string ToString()
        {
            return Name;
        }

        public DateTime Date { get { return LastUpdateTime; } }
        public double RxBytePS { get; set; }
        public double TxBytePS { get; set; }
        public double RxPacketPS { get; set; }
        public double TxPacketPS { get; set; }

        public void Update(Interface other)
        {
            double diffTime = (LastUpdateTime - other.LastUpdateTime).TotalSeconds;
            RxBytePS = (RxByte - other.RxByte) / diffTime;
            TxBytePS = (TxByte - other.TxByte) / diffTime;
            RxPacketPS = (RxPacket - other.RxPacket) / diffTime;
            TxPacketPS = (TxPacket - other.TxPacket) / diffTime;
        }

        public static Interface Parse(string name, Connection conn)
        {
            Interface value = new Interface();
            value.Name = name;
            return value;
        }
    }
}
