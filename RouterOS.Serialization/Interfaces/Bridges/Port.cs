﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

using RouterOS.Serialization;

namespace RouterOS.Interfaces.Bridges
{
    [RosObject("interface bridge port", CanAdd = true, CanDisable = true, CanEnable = true, CanGet = true, CanSet = true, CanRemove = true, CanListen = true)]
    public class Port : RosItemObject<Port>
    {
        [RosValue("bridge")]
        public Ref<Bridge> Bridge { get; set; }

        [RosValue("interface")]
        public Ref<Interface> Interface { get; set; }

        [RosValue("comment")]
        public override string Comment { get; set; }

        [RosValue("role")]
        public string Role { get; set; }
    
        [RosValue("disabled")]
        public override bool? Disabled { get; set; }

        [RosValue("dynamic", IsDynamic = true)]
        public override bool? Dynamic { get; set; }

        [RosValue("inactive", IsDynamic = true)]
        public override bool? Inactive { get; set; }
    }
}
