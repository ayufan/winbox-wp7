﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RouterOS.Serialization;

namespace RouterOS.Interfaces.Bridges
{
    public enum BridgeProtocolMode
    {
        [RosValue("none")]
        None,

        [RosValue("stp")]
        STP,

        [RosValue("rstp")]
        RSTP
    }

    [RosObject("interface bridge", CanAdd = true, CanDisable = true, CanListen = true, CanEnable = true, CanGet = true, CanRemove = true, CanSet = true)]
    public class Bridge : RosItemObject<Bridge>
    {
        [RosValue("name", IsKey = true)]
        public string Name { get; set; }

        [RosValue("arp")]
        public ARPMode? ARP { get; set; }

        [RosValue("admin-mac")]
        public MACAddress? AdminMacAddress { get; set; }

        [RosValue("mac-address", IsDynamic = true)]
        public MACAddress? MacAddress { get; set; }

        [RosValue("auto-mac")]
        public bool? AutoMacAddress { get; set; }

        [RosValue("mtu", Default = "1500")]
        public int? Mtu { get; set; }

        [RosValue("l2mtu", Default = "65536")]
        public int? L2Mtu { get; set; }

        [RosValue("running", IsDynamic = true)]
        public override bool? Running { get; set; }

        [RosValue("comment", Default = "")]
        public override string Comment { get; set; }

        [RosValue("ageing-time")]
        public TimeSpan? AgeingTime { get; set; }

        [RosValue("max-message-age")]
        public TimeSpan? MaxMessageAge { get; set; }

        [RosValue("forward-delay")]
        public TimeSpan? ForwardDelay { get; set; }

        [RosValue("priority")]
        public int? Priority { get; set; }

        [RosValue("transmit-hold-count")]
        public int? TransmitHoldCount { get; set; }

        [RosValue("protocol-mode")]
        public BridgeProtocolMode? ProtocolMode { get; set; }

        public string Type
        {
            get { return "bridge"; }
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
