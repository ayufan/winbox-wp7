﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

using RouterOS.Serialization;

namespace RouterOS.Interfaces.Bridges
{
    [RosObject("interface bridge host", CanGet = true, CanListen = true)]
    public class Host : RosItemObject<Host>
    {
        [RosValue("bridge", IsDynamic = true)]
        public Ref<Bridge> Bridge { get; set; }

        [RosValue("on-interface", IsDynamic = true)]
        public Ref<Interface> OnInterface { get; set; }

        [RosValue("mac-address", IsDynamic = true)]
        public MACAddress MacAddress { get; set; }

        [RosValue("age", IsDynamic = true)]
        public TimeSpan? Age { get; set; }

        [RosValue("local", IsDynamic = true)]
        public bool? local { get; set; }

        [RosValue("external-fdb", IsDynamic = true)]
        public bool? ExternalFdb { get; set; }
    }
}
