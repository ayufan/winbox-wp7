﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RouterOS.API;
using System.Net;
using RouterOS.Serialization;

namespace RouterOS.Interfaces.Ethernets
{
    [RosObject("interface ethernet", CanAdd = true, CanDisable = true, CanEnable = true, CanGet = true, CanRemove = true, CanSet = true, CanUnset = true, CanListen = true)]
    public class Ethernet : RosItemObject<Ethernet>, IInterface
    {
        [RosValue("name", IsKey=true)]
        public string Name { get; set; }

        [RosValue("arp")]
        public ARPMode? ARP { get; set; }

        [RosValue("speed")]
        public Bits? Speed { get; set; }
        
        [RosValue("bandwidth")]
        public string Bandwidth { get; set; }

        [RosValue("auto-negotiation")]
        public bool? AutoNegotiation { get; set; }

        [RosValue("full-duplex")]
        public bool? FullDuplex { get; set; }

        [RosValue("mdix-enable", IsDynamic=true)]
        public bool? MdixEnable { get; set; }

        [RosValue("mac-address")]
        public MACAddress? MacAddress { get; set; }
        
        [RosValue("master-port")]
        public string MasterPort { get; set; }
        
        [RosValue("mtu", Default="1500")]
        public int? Mtu { get; set; }

        [RosValue("l2mtu", IsDynamic = true)]
        public int L2Mtu { get; set; }

        [RosValue("comment", Default = "")]
        public override string Comment { get; set; }

        public string Type
        {
            get { return "ether"; }
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
