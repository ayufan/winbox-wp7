﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RouterOS.API;
using System.Net;
using RouterOS.Serialization;

namespace RouterOS.Interfaces
{
    [RosObject("interface gre", CanAdd = true, CanDisable = true, CanEnable = true, CanGet = true, CanRemove = true, CanSet = true, CanUnset = true, CanListen = true)]
    public class GreInterface : RosItemObject<GreInterface>, IInterface
    {
        [RosValue("name", IsKey=true)]
        public string Name { get; set; }

        [RosValue("arp")]
        public ARPMode? ARP { get; set; }
        
        [RosValue("mtu", Default="1500")]
        public int Mtu { get; set; }

        [RosValue("l2mtu", IsDynamic = true)]
        public int L2Mtu { get; set; }

        [RosValue("keepalive")]
        public int? KeepAlive { get; set; }

        [RosValue("local-address")]
        public IPAddress LocalAddress { get; set; }

        [RosValue("remote-address")]
        public IPAddress RemoteAddress { get; set; }

        [RosValue("comment", Default = "")]
        public override string Comment { get; set; }

        public string Type
        {
            get { return "gre-tunnel"; }
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
