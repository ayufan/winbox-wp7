﻿using System;
using RouterOS.Serialization;

namespace RouterOS.Scripting
{
    [RosObject("system script", CanAdd=true, CanGet=true,
        CanListen=true, CanRemove=true, CanSet=true, CanUnset=true)]
    public class Script : RosItemObject<Script>
    {
        [RosValue("name")]
        public string Name { get; set; }

        [RosValue("owner", IsDynamic=true)]
        public string Owner { get; set; }

        [RosValue("policy")]
        public ItemList<string> Policy { get; set; }

        [RosValue("run-count", IsDynamic=true)]
        public int? RunCount { get; set; }

        [RosValue("last-started", IsDynamic=true)]
        public DateTime? LastStarted { get; set; }

        [RosValue("source")]
        public string Source { get; set; }

        [RosValue("invalid", IsDynamic=true)]
        public override bool? Invalid { get; set; }

        public string NameWithStatus
        {
            get { return StatusText.Length != 0 ? String.Format("{0} [{1}]", Name, StatusText) : Name; }
        }

        public string StatusText
        {
            get
            {
                if (Invalid.GetValueOrDefault()) 
                    return "invalid";
                return "";
            }
        }

        private class RunCommand : RouterOS.API.ValueCommand
        {
            public override string Action
            {
                get { return "run"; }
            }
        }

        public RouterOS.API.CommandTag Run(RouterOS.API.Connection connection, RouterOS.API.AsyncResponseCallback callback)
        {
            if (!IsAdded)
                throw new ArgumentException("cannot run not added script");

            RunCommand cmd = new RunCommand();
            cmd.Scope = new string[] { "system", "script" };
            cmd["number"] = Id;
            return connection.ExecuteCommand(cmd, callback);
        }
    }
}
