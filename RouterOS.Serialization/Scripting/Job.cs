﻿using System;
using RouterOS.Serialization;


namespace RouterOS.Scripting
{
    public enum JobType
    {
        [RosValue("api-login")]
        ApiLogin,

        [RosValue("command")]
        Command,

        [RosValue("login")]
        Login
    }

    [RosObject("system script job", CanAdd = true, CanSet = true, CanGet = true, CanRemove = true, CanListen=true)]
    public class Job : RosItemObject<Job>
    {
        [RosValue("comment")]
        public override String Comment
        {
            get;
            set;
        }

        [RosValue("started")]
        public DateTime? Started
        {
            get;
            set;
        }

        [RosValue("type")]
        public JobType? Type
        {
            get;
            set;
        }

        [RosValue("owner", IsDynamic = true)]
        public String Owner
        {
            get;
            set;
        }

        [RosValue("parent", IsDynamic = true)]
        public Int32? Parent
        {
            get;
            set;
        }

        [RosValue("policy", IsDynamic = true)]
        public ItemList<string> Policy
        {
            get;
            set;
        }

        [RosValue("script", IsDynamic = true)]
        public Ref<Script>? Script
        {
            get;
            set;
        }
    }
}
