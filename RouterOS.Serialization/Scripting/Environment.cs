﻿using System;
using RouterOS.Serialization;


namespace RouterOS.Scripting
{
    [RosObject("system script environment", CanSet = true, CanGet = true, CanRemove = true, CanListen=true)]
    public class Environment : RosItemObject<Environment>
    {
        [RosValue("comment")]
        public override String Comment
        {
            get;
            set;
        }

        [RosValue("value")]
        public String Value
        {
            get;
            set;
        }

        [RosValue("name", IsDynamic = true)]
        public String Name
        {
            get;
            set;
        }

        [RosValue("user", IsDynamic = true)]
        public String User
        {
            get;
            set;
        }
    }
}
