﻿using System;
using RouterOS.Serialization;

namespace RouterOS.Wireless
{
    public enum AuthenticationType
    {
        [RosValue("wpa-eap")]
        WpaEap,

        [RosValue("wpa-psk")]
        WpaPsk,

        [RosValue("wpa2-eap")]
        Wpa2Eap,

        [RosValue("wpa2-psk")]
        Wpa2Psk,

    }

    public enum WmmSupportEnum
    {
        [RosValue("disabled")]
        Disabled,

        [RosValue("enabled")]
        Enabled,

        [RosValue("required")]
        Required,

    }
    public enum Band
    {
        [RosValue("2ghz-b")]
        B_2GHz,

        [RosValue("2ghz-b/g")]
        BG_2GHz,

        [RosValue("2ghz-b/g/n")]
        BGN_2GHz,

        [RosValue("2ghz-onlyg")]
        OnlyG_2GHz,

        [RosValue("2ghz-onlyn")]
        OnlyN_2GHz,

        [RosValue("5ghz-a")]
        A_5GHz,

        [RosValue("5ghz-a/n")]
        AN_5GHz,

        [RosValue("5ghz-onlyn")]
        OnlyN_5GHz
    }

    public enum ChannelWidth
    {
        [RosValue("10mhz")]
        Half,

        [RosValue("20/40mhz-ht-above")]
        HT_Above,

        [RosValue("20/40mhz-ht-below")]
        HT_Below,

        [RosValue("20mhz")]
        Normal,

        [RosValue("40mhz-turbo")]
        Turbo,

        [RosValue("5mhz")]
        Quarter
    }
    public enum EapMethod
    {
        [RosValue("eap-tls")]
        EapTls,

        [RosValue("passthrough")]
        Passthrough,
    }

    public enum GroupCiphers
    {
        [RosValue("tkip")]
        Tkip,

        [RosValue("aes-ccm")]
        AesCcm,
    }

    public enum ManagementProtection
    {
        [RosValue("allowed")]
        Allowed,

        [RosValue("disabled")]
        Disabled,

        [RosValue("required")]
        Required,
    }

    public enum SecurityMode
    {
        [RosValue("dynamic-keys")]
        DynamicKeys,

        [RosValue("none")]
        None,

        [RosValue("static-keys-optional")]
        StaticKeysOptional,

        [RosValue("static-keys-required")]
        StaticKeysRequired
    }

    public enum RadiusMacMode
    {
        [RosValue("as-username")]
        AsUsername,

        [RosValue("as-username-and-password")]
        AsUsernameAndPassword,

    }

    public enum TransmitKey
    {
        [RosValue("key-0")]
        Key0,

        [RosValue("key-1")]
        Key1,

        [RosValue("key-2")]
        Key2,

        [RosValue("key-3")]
        Key3,

    }

    public enum TlsMode
    {
        [RosValue("dont-verify-certificate")]
        DontVerifyCertificate,

        [RosValue("no-certificates")]
        NoCertificates,

        [RosValue("verify-certificate")]
        VerifyCertificate,

    }

    public enum WirelessEncryption
    {
        [RosValue("104bit-wep")]
        E104bitWep,

        [RosValue("40bit-wep")]
        E40bitWep,

        [RosValue("aes-ccm")]
        AesCcm,

        [RosValue("none")]
        None,

        [RosValue("tkip")]
        Tkip
    }

    public enum WdsModeEnum
    {
        [RosValue("disabled")]
        Disabled,

        [RosValue("dynamic")]
        Dynamic,

        [RosValue("dynamic-mesh")]
        DynamicMesh,

        [RosValue("static")]
        Static,

        [RosValue("static-mesh")]
        StaticMesh,

    }

    public enum WirelessProtocol
    {
        [RosValue("unspecified")]
        Unspecified,

        [RosValue("any")]
        Any,

        [RosValue("802.11")]
        E80211,

        [RosValue("nstreme")]
        Nstreme,

        [RosValue("nv2")]
        Nv2,

        [RosValue("nv2-nstreme")]
        Nv2Nstreme,

        [RosValue("nv2-nstreme-802.11")]
        Nv2Nstreme80211,
    }

    public enum TxPowerModeEnum
    {
        [RosValue("all-rates-fixed")]
        AllRatesFixed,

        [RosValue("card-rates")]
        CardRates,

        [RosValue("default")]
        Default,

        [RosValue("manual-table")]
        ManualTable,

    }

    public enum TdmaOverrideRateEnum
    {
        [RosValue("12mbps")]
        E12mbps,

        [RosValue("18mbps")]
        E18mbps,

        [RosValue("24mbps")]
        E24mbps,

        [RosValue("36mbps")]
        E36mbps,

        [RosValue("48mbps")]
        E48mbps,

    }

    public enum RateSetEnum
    {
        [RosValue("configured")]
        Configured,

        [RosValue("default")]
        Default,

    }

    public enum PreambleModeEnum
    {
        [RosValue("both")]
        Both,

        [RosValue("long")]
        Long,

        [RosValue("short")]
        Short,

    }

    public enum PeriodicCalibrationEnum
    {
        [RosValue("default")]
        Default,

        [RosValue("disabled")]
        Disabled,

        [RosValue("enabled")]
        Enabled,

    }

    public enum Nv2SecurityEnum
    {
        [RosValue("disabled")]
        Disabled,

        [RosValue("enabled")]
        Enabled,

    }

    public enum Nv2QosEnum
    {
        [RosValue("default")]
        Default,

        [RosValue("frame-priority")]
        FramePriority,

    }


    public enum ModeEnum
    {
        [RosValue("alignment-only")]
        AlignmentOnly,

        [RosValue("ap-bridge")]
        ApBridge,

        [RosValue("bridge")]
        Bridge,

        [RosValue("nstreme-dual-slave")]
        NstremeDualSlave,

        [RosValue("station")]
        Station,

        [RosValue("wds-slave")]
        WdsSlave,

        [RosValue("station-pseudobridge")]
        StationPseudobridge,

        [RosValue("station-wds")]
        StationWds,

        [RosValue("station-pseudobridge-clone")]
        StationPseudobridgeClone,
    }

    public enum HwProtectionModeEnum
    {
        [RosValue("cts-to-self")]
        CtsToSelf,

        [RosValue("none")]
        None,

        [RosValue("rts-cts")]
        RtsCts,

    }

    public enum HtGuardIntervalEnum
    {
        [RosValue("any")]
        Any,

        [RosValue("long")]
        Long,

        [RosValue("both")]
        Both,

        [RosValue("short")]
        Short,

    }

    public enum FrequencyModeEnum
    {
        [RosValue("manual-txpower")]
        ManualTxpower,

        [RosValue("regulatory-domain")]
        RegulatoryDomain,

        [RosValue("superchannel")]
        Superchannel,

    }

    public enum DistanceEnum
    {
        [RosValue("dynamic")]
        Dynamic,

        [RosValue("indoors")]
        Indoors,

    }

    public enum DfsModeEnum
    {
        [RosValue("no-radar-detect")]
        NoRadarDetect,

        [RosValue("none")]
        None,

        [RosValue("radar-detect")]
        RadarDetect,

    }

    public enum BridgeModeEnum
    {
        [RosValue("disabled")]
        Disabled,

        [RosValue("enabled")]
        Enabled,

    }

    public enum AntennaModeEnum
    {
        [RosValue("ant-a")]
        AntA,

        [RosValue("ant-b")]
        AntB,

        [RosValue("rxa-txb")]
        RxaTxb,

        [RosValue("txa-rxb")]
        TxaRxb,

    }

    public enum AdaptiveNoiseImmunityEnum
    {
        [RosValue("ap-and-client-mode")]
        ApAndClientMode,

        [RosValue("client-mode")]
        ClientMode,

        [RosValue("none")]
        None,

    }
}
