﻿using System;
using RouterOS.Serialization;


namespace RouterOS.Wireless
{
    [RosObject("interface wireless scan", IsCommand = true)]
    public class Scan : CommandKeyedObject
    {
        [RosValue("address", IsDynamic = true)]
        public MACAddress? Address { get; set; }

        [RosValue("ssid", IsDynamic = true)]
        public string Ssid { get; set; }

        [RosValue("band", IsDynamic = true)]
        public Band? Band { get; set; }

        [RosValue("channel-width", IsDynamic = true)]
        public ChannelWidth? ChannelWidth { get; set; }

        [RosValue("freq", IsDynamic = true)]
        public int? Frequency { get; set; }

        [RosValue("sig", IsDynamic = true)]
        public int? Signal { get; set; }

        [RosValue("nf", IsDynamic = true)]
        public int? NoiseFloor { get; set; }

        [RosValue("snr", IsDynamic = true)]
        public int? SignalToNoise { get; set; }

        [RosValue("radio-name", IsDynamic = true)]
        public string RadioName { get; set; }

        public override int GetHashCode()
        {
            return Address.GetHashCode();
        }
    }

    public class ScanOptions : RosObject
    {
        [RosValue(".id")]
        public Ref<WirelessInterface>? Interface { get; set; }

        [RosValue("duration")]
        public TimeSpan? Duration { get; set; }

        [RosValue("freeze-frame-interval")]
        public TimeSpan? FreezeFrameInterval { get; set; }
    }
}
