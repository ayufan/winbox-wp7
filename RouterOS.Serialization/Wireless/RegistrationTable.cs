﻿using System;
using RouterOS.Serialization;


namespace RouterOS.Wireless
{

    [RosObject("interface wireless registration-table", CanGet = true, CanRemove = true, IsCommand=true, UseInterval=5000, CanListen=true)]
    public class RegistrationTable : RosItemObject<RegistrationTable>
    {
        [RosValue("ap", IsDynamic = true)]
        public Boolean? Ap
        {
            get;
            set;
        }

        [RosValue("authentication-type", IsDynamic = true)]
        public AuthenticationType? AuthenticationType
        {
            get;
            set;
        }

        [RosValue("client-tx-limit", IsDynamic = true)]
        public Bytes? ClientTxLimit
        {
            get;
            set;
        }

        [RosValue("compression", IsDynamic = true)]
        public Boolean? Compression
        {
            get;
            set;
        }

        [RosValue("encryption", IsDynamic = true)]
        public WirelessEncryption? Encryption
        {
            get;
            set;
        }

        [RosValue("frames", IsDynamic = true)]
        public uint[] Frames
        {
            get;
            set;
        }

        [RosValue("framing-limit", IsDynamic = true)]
        public uint[] FramingLimit
        {
            get;
            set;
        }

        [RosValue("group-encryption", IsDynamic = true)]
        public WirelessEncryption? GroupEncryption
        {
            get;
            set;
        }

        [RosValue("hw-frames", IsDynamic = true)]
        public uint[] HwFrames
        {
            get;
            set;
        }

        [RosValue("last-activity", IsDynamic = true)]
        public TimeSpan? LastActivity
        {
            get;
            set;
        }

        [RosValue("mac-address", IsDynamic = true)]
        public MACAddress? MacAddress
        {
            get;
            set;
        }

        [RosValue("nstreme", IsDynamic = true)]
        public bool? Nstreme
        {
            get;
            set;
        }

        [RosValue("packed-bytes", IsDynamic = true)]
        public Bytes[] PackedBytes
        {
            get;
            set;
        }

        [RosValue("packets", IsDynamic = true)]
        public uint[] Packets
        {
            get;
            set;
        }

        [RosValue("routeros-version", IsDynamic = true)]
        public Version RouterosVersion
        {
            get;
            set;
        }

        [RosValue("rx-rate", IsDynamic = true)]
        public String RxRate
        {
            get;
            set;
        }

        [RosValue("signal-to-noise", IsDynamic = true)]
        public String SignalToNoise
        {
            get;
            set;
        }

        [RosValue("signal-strength", IsDynamic = true)]
        public String SignalStrength
        {
            get;
            set;
        }

        [RosValue("tx-signal-strength", IsDynamic = true)]
        public String TxSignalStrength
        {
            get;
            set;
        }

        [RosValue("uptime", IsDynamic = true)]
        public TimeSpan? Uptime
        {
            get;
            set;
        }

        [RosValue("wmm-enabled", IsDynamic = true)]
        public bool? WmmEnabled
        {
            get;
            set;
        }

        [RosValue("ack-timeout", IsDynamic = true)]
        public Int32? AckTimeout
        {
            get;
            set;
        }

        [RosValue("ap-tx-limit", IsDynamic = true)]
        public Bytes? ApTxLimit
        {
            get;
            set;
        }

        [RosValue("bytes", IsDynamic = true)]
        public Bytes[] Bytes
        {
            get;
            set;
        }

        [RosValue("comment", IsDynamic = true)]
        public override String Comment
        {
            get;
            set;
        }

        [RosValue("distance", IsDynamic = true)]
        public string Distance
        {
            get;
            set;
        }

        [RosValue("frame-bytes", IsDynamic = true)]
        public Bytes[] FrameBytes
        {
            get;
            set;
        }

        [RosValue("hw-frame-bytes", IsDynamic = true)]
        public Bytes[] HwFrameBytes
        {
            get;
            set;
        }

        [RosValue("interface", IsDynamic = true)]
        public Ref<WirelessInterface> Interface
        {
            get;
            set;
        }

        [RosValue("last-ip", IsDynamic = true)]
        public System.Net.IPAddress LastIp
        {
            get;
            set;
        }

        [RosValue("management-protection", IsDynamic = true)]
        public bool? ManagementProtection
        {
            get;
            set;
        }

        [RosValue("p-throughput", IsDynamic = true)]
        public Int32? PThroughput
        {
            get;
            set;
        }

        [RosValue("packed-frames", IsDynamic = true)]
        public Bytes[] PackedFrames
        {
            get;
            set;
        }

        [RosValue("radio-name", IsDynamic = true)]
        public String RadioName
        {
            get;
            set;
        }

        [RosValue("rx-ccq", IsDynamic = true)]
        public string RxCcq
        {
            get;
            set;
        }

        [RosValue("signal-strength-ch0", IsDynamic = true)]
        public String SignalStrengthCh0
        {
            get;
            set;
        }

        [RosValue("signal-strength-ch1", IsDynamic = true)]
        public String SignalStrengthCh1
        {
            get;
            set;
        }

        [RosValue("signal-strength-ch2", IsDynamic = true)]
        public String SignalStrengthCh2
        {
            get;
            set;
        }

        [RosValue("strength-at-rates", IsDynamic = true)]
        public String StrengthAtRates
        {
            get;
            set;
        }

        [RosValue("tx-ccq", IsDynamic = true)]
        public string TxCcq
        {
            get;
            set;
        }

        [RosValue("tx-rate", IsDynamic = true)]
        public String TxRate
        {
            get;
            set;
        }

        [RosValue("tx-signal-strength-ch0", IsDynamic = true)]
        public String TxSignalStrengthCh0
        {
            get;
            set;
        }

        [RosValue("tx-signal-strength-ch1", IsDynamic = true)]
        public String TxSignalStrengthCh1
        {
            get;
            set;
        }

        [RosValue("tx-signal-strength-ch2", IsDynamic = true)]
        public String TxSignalStrengthCh2
        {
            get;
            set;
        }

        [RosValue("wds", IsDynamic = true)]
        public bool? Wds
        {
            get;
            set;
        }

    }
}
