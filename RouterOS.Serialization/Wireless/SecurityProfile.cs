﻿using System;
using RouterOS.Serialization;

using RouterOS.API;

namespace RouterOS.Wireless
{	
    [RosObject("interface wireless security-profiles", CanAdd = true, CanSet = true, CanGet = true, CanRemove = true)]
    public class SecurityProfile : RosItemObject<SecurityProfile>
    {
        [RosValue("authentication-types")]
        public String AuthenticationTypes
        {
            get;
            set;
        }

        // Short description of the item
        [RosValue("comment")]
        public override String Comment
        {
            get;
            set;
        }

        [RosValue("eap-methods")]
        public EapMethod[] EapMethods
        {
            get;
            set;
        }

        [RosValue("group-ciphers")]
        public GroupCiphers[] GroupCiphers
        {
            get;
            set;
        }

        // How often to update group key
        [RosValue("group-key-update")]
        public TimeSpan? GroupKeyUpdate
        {
            get;
            set;
        }

        [RosValue("interim-update")]
        public TimeSpan? InterimUpdate
        {
            get;
            set;
        }

        [RosValue("management-protection")]
        public ManagementProtection? ManagementProtection
        {
            get;
            set;
        }

        [RosValue("management-protection-key")]
        public String ManagementProtectionKey
        {
            get;
            set;
        }

        // Security mode
        [RosValue("mode")]
        public SecurityMode? Mode
        {
            get;
            set;
        }

        // Descriptive name for the security profile
        [RosValue("name")]
        public String Name
        {
            get;
            set;
        }

        [RosValue("radius-eap-accounting")]
        public Boolean? RadiusEapAccounting
        {
            get;
            set;
        }

        [RosValue("radius-mac-accounting")]
        public Boolean? RadiusMacAccounting
        {
            get;
            set;
        }

        // Whether to use Radius server MAC authentication or not
        [RosValue("radius-mac-authentication")]
        public Boolean? RadiusMacAuthentication
        {
            get;
            set;
        }

        [RosValue("radius-mac-caching")]
        public string RadiusMacCaching
        {
            get;
            set;
        }

        [RosValue("radius-mac-format")]
        public String RadiusMacFormat
        {
            get;
            set;
        }

        [RosValue("radius-mac-mode")]
        public RadiusMacMode? RadiusMacMode
        {
            get;
            set;
        }

        // 1st encryption algorithm to use
        [RosValue("static-algo-0")]
        public WirelessEncryption? StaticAlgo0
        {
            get;
            set;
        }

        // 2nd encryption algorithm to use
        [RosValue("static-algo-1")]
        public WirelessEncryption? StaticAlgo1
        {
            get;
            set;
        }

        // 3rd encryption algorithm to use
        [RosValue("static-algo-2")]
        public WirelessEncryption? StaticAlgo2
        {
            get;
            set;
        }

        // 4th encryption algorithm to use
        [RosValue("static-algo-3")]
        public WirelessEncryption? StaticAlgo3
        {
            get;
            set;
        }

        // Hexadecimal key which will be used to encrypt packets with algo-0
        [RosValue("static-key-0")]
        public String StaticKey0
        {
            get;
            set;
        }

        // Hexadecimal key which will be used to encrypt packets with algo-1
        [RosValue("static-key-1")]
        public String StaticKey1
        {
            get;
            set;
        }

        // Hexadecimal key which will be used to encrypt packets with algo-2
        [RosValue("static-key-2")]
        public String StaticKey2
        {
            get;
            set;
        }

        // Hexadecimal key which will be used to encrypt packets with algo-3
        [RosValue("static-key-3")]
        public String StaticKey3
        {
            get;
            set;
        }

        // Algorithm to use if the sta-private-key is set
        [RosValue("static-sta-private-algo")]
        public WirelessEncryption? StaticStaPrivateAlgo
        {
            get;
            set;
        }

        // Private encryption key for the station
        [RosValue("static-sta-private-key")]
        public String StaticStaPrivateKey
        {
            get;
            set;
        }

        // Key to use for broadcast packets
        [RosValue("static-transmit-key")]
        public TransmitKey? StaticTransmitKey
        {
            get;
            set;
        }

        [RosValue("supplicant-identity")]
        public String SupplicantIdentity
        {
            get;
            set;
        }

        [RosValue("tls-certificate")]
        public String TlsCertificate
        {
            get;
            set;
        }

        [RosValue("tls-mode")]
        public TlsMode? TlsMode
        {
            get;
            set;
        }

        [RosValue("unicast-ciphers")]
        public String UnicastCiphers
        {
            get;
            set;
        }

        [RosValue("wpa-pre-shared-key")]
        public String WpaPreSharedKey
        {
            get;
            set;
        }

        [RosValue("wpa2-pre-shared-key")]
        public String Wpa2PreSharedKey
        {
            get;
            set;
        }

        public static SecurityProfile Default
        {
            get
            {
                return new SecurityProfile() { Id = 0 };
            }
        }

        public static SecurityProfile Parse(string str, Connection conn)
        {
            if (string.IsNullOrEmpty(str))
                return null;
            SecurityProfile obj = new SecurityProfile();
            obj.FindByName(conn, str);
            return obj;
        }

        public override string ToString()
        {
            return Name.ToString();
        }

    }
}
