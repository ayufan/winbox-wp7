﻿using System;
using System.Net;
using RouterOS.Serialization;


namespace RouterOS.Wireless
{
    [RosObject("interface wireless", CanDisable = true, CanEnable = true, CanGet = true, CanSet = true, CanListen = true, CanRemove = true, CanUnset = true)]
    public class WirelessInterface : RosItemObject<WirelessInterface>, IInterface
    {
        [RosValue("name", IsKey = true)]
        public string Name
        {
            get;
            set;
        }

        [RosValue("type")]
        public string Type
        {
            get;
            set;
        }

        [RosValue("adaptive-noise-immunity")]
        public AdaptiveNoiseImmunityEnum? AdaptiveNoiseImmunity
        {
            get;
            set;
        }

        [RosValue("allow-sharedkey")]
        public Boolean? AllowSharedkey
        {
            get;
            set;
        }

        // Antenna gain in dBi
        [RosValue("antenna-gain")]
        public Int32? AntennaGain
        {
            get;
            set;
        }

        // Select antenna to use for transmitting and for receiving.
        [RosValue("antenna-mode")]
        public AntennaModeEnum? AntennaMode
        {
            get;
            set;
        }

        [RosValue("area")]
        public string Area
        {
            get;
            set;
        }

        // Address Resolution Protocol
        [RosValue("arp")]
        public ARPMode? Arp
        {
            get;
            set;
        }

        // Defines set of used data rates, channel frequencies and widths.
        [RosValue("band")]
        public string Band
        {
            get;
            set;
        }

        // Basic rates in 802.11a or 802.11g standard
        [RosValue("basic-rates-a/g")]
        public string BasicRatesAG
        {
            get;
            set;
        }

        // Basic rates in 802.11b mode
        [RosValue("basic-rates-b")]
        public string BasicRatesB
        {
            get;
            set;
        }

        [RosValue("bridge-mode")]
        public BridgeModeEnum? BridgeMode
        {
            get;
            set;
        }

        // Time in ms which will be used to send data without stopping
        [RosValue("burst-time")]
        public string BurstTime
        {
            get;
            set;
        }

        [RosValue("channel-width")]
        public ChannelWidth? ChannelWidth
        {
            get;
            set;
        }

        // Short description of the item
        [RosValue("comment")]
        public override string Comment
        {
            get;
            set;
        }

        [RosValue("compression")]
        public Boolean? Compression
        {
            get;
            set;
        }

        // Limits a frequency and a transmit power to those which are allowed in the respective country
        [RosValue("country")]
        public string Country
        {
            get;
            set;
        }

        [RosValue("default-ap-tx-limit")]
        public Int32? DefaultApTxLimit
        {
            get;
            set;
        }

        // Accept or reject a client that wants to associate, but it's not in the access-list
        [RosValue("default-authentication")]
        public Boolean? DefaultAuthentication
        {
            get;
            set;
        }

        [RosValue("default-client-tx-limit")]
        public Int32? DefaultClientTxLimit
        {
            get;
            set;
        }

        // Defines whether to use default forwarding or not
        [RosValue("default-forwarding")]
        public Boolean? DefaultForwarding
        {
            get;
            set;
        }

        // Used for APs to dynamically select frequency at which this AP will operate
        [RosValue("dfs-mode")]
        public DfsModeEnum? DfsMode
        {
            get;
            set;
        }

        // Disable or not running check
        [RosValue("disable-running-check")]
        public Boolean? DisableRunningCheck
        {
            get;
            set;
        }

        // Defines whether item is ignored or used
        [RosValue("disabled")]
        public override Boolean? Disabled
        {
            get;
            set;
        }

        // Only above this value the client device is considered as disconnected
        [RosValue("disconnect-timeout")]
        public TimeSpan? DisconnectTimeout
        {
            get;
            set;
        }

        [RosValue("distance")]
        public DistanceEnum? Distance
        {
            get;
            set;
        }

        [RosValue("frame-lifetime")]
        public Int32? FrameLifetime
        {
            get;
            set;
        }

        // Channel frequency on which AP will operate.
        [RosValue("frequency")]
        public Int32? Frequency
        {
            get;
            set;
        }

        // Defines which frequency channels to allow
        [RosValue("frequency-mode")]
        public FrequencyModeEnum? FrequencyMode
        {
            get;
            set;
        }

        [RosValue("frequency-offset")]
        public string FrequencyOffset
        {
            get;
            set;
        }

        // Hide SSID string
        [RosValue("hide-ssid")]
        public Boolean? HideSsid
        {
            get;
            set;
        }

        [RosValue("ht-ampdu-priorities")]
        public string HtAmpduPriorities
        {
            get;
            set;
        }

        [RosValue("ht-amsdu-limit")]
        public Int32? HtAmsduLimit
        {
            get;
            set;
        }

        [RosValue("ht-amsdu-threshold")]
        public Int32? HtAmsduThreshold
        {
            get;
            set;
        }

        [RosValue("ht-basic-mcs")]
        public string HtBasicMcs
        {
            get;
            set;
        }

        [RosValue("ht-guard-interval")]
        public HtGuardIntervalEnum? HtGuardInterval
        {
            get;
            set;
        }

        [RosValue("ht-rxchains")]
        public string HtRxchains
        {
            get;
            set;
        }

        [RosValue("ht-supported-mcs")]
        public string HtSupportedMcs
        {
            get;
            set;
        }

        [RosValue("ht-txchains")]
        public string HtTxchains
        {
            get;
            set;
        }

        [RosValue("hw-fragmentation-threshold")]
        public string HwFragmentationThreshold
        {
            get;
            set;
        }

        [RosValue("hw-protection-mode")]
        public HwProtectionModeEnum? HwProtectionMode
        {
            get;
            set;
        }

        [RosValue("hw-protection-threshold")]
        public Int32? HwProtectionThreshold
        {
            get;
            set;
        }

        [RosValue("hw-retries")]
        public Int32? HwRetries
        {
            get;
            set;
        }

        [RosValue("l2mtu")]
        public UInt16? L2mtu
        {
            get;
            set;
        }

        // MAC address of the interface
        [RosValue("mac-address")]
        public MACAddress? MacAddress
        {
            get;
            set;
        }

        // Physical wireless interface used by virtual AP.
        [RosValue("master-interface")]
        public Ref<WirelessInterface>? MasterInterface
        {
            get;
            set;
        }

        // The maximum count of the stations
        [RosValue("max-station-count")]
        public Int32? MaxStationCount
        {
            get;
            set;
        }

        // Operate as AP or station 
        [RosValue("mode")]
        public ModeEnum? Mode
        {
            get;
            set;
        }

        // Maximum Transmit Unit
        [RosValue("mtu")]
        public Int32? Mtu
        {
            get;
            set;
        }

        // Value in dBm below whcih it's rather noise than a normal signal
        [RosValue("noise-floor-threshold")]
        public string NoiseFloorThreshold
        {
            get;
            set;
        }

        [RosValue("nv2-cell-radius")]
        public Int32? Nv2CellRadius
        {
            get;
            set;
        }

        [RosValue("nv2-noise-floor-offset")]
        public string Nv2NoiseFloorOffset
        {
            get;
            set;
        }

        [RosValue("nv2-preshared-key")]
        public string Nv2PresharedKey
        {
            get;
            set;
        }

        [RosValue("nv2-qos")]
        public Nv2QosEnum? Nv2Qos
        {
            get;
            set;
        }

        [RosValue("nv2-queue-count")]
        public Int32? Nv2QueueCount
        {
            get;
            set;
        }

        [RosValue("nv2-security")]
        public Nv2SecurityEnum? Nv2Security
        {
            get;
            set;
        }

        [RosValue("on-fail-retry-time")]
        public TimeSpan? OnFailRetryTime
        {
            get;
            set;
        }

        // To ensure performance of chip set over temperature and environmental changes, the software performs periodic calibration
        [RosValue("periodic-calibration")]
        public PeriodicCalibrationEnum? PeriodicCalibration
        {
            get;
            set;
        }

        [RosValue("periodic-calibration-interval")]
        public Int32? PeriodicCalibrationInterval
        {
            get;
            set;
        }

        // The synchronization field in a wireless packet
        [RosValue("preamble-mode")]
        public PreambleModeEnum? PreambleMode
        {
            get;
            set;
        }

        [RosValue("proprietary-extensions")]
        public string ProprietaryExtensions
        {
            get;
            set;
        }

        // A Descriptive name of the card
        [RosValue("radio-name")]
        public string RadioName
        {
            get;
            set;
        }

        // Rate set to use
        [RosValue("rate-set")]
        public RateSetEnum? RateSet
        {
            get;
            set;
        }

        // Set of channel frequencies that will be used by device.
        [RosValue("scan-list")]
        public string ScanList
        {
            get;
            set;
        }

        [RosValue("security-profile")]
        public Ref<SecurityProfile>? SecurityProfile
        {
            get;
            set;
        }

        // Name that identifies wireless network.
        [RosValue("ssid")]
        public string Ssid
        {
            get;
            set;
        }

        [RosValue("station-bridge-clone-mac")]
        public MACAddress? StationBridgeCloneMac
        {
            get;
            set;
        }

        // Rates to be supported in 802.11a or 802.11g standard
        [RosValue("supported-rates-a/g")]
        public string SupportedRatesAG
        {
            get;
            set;
        }

        // Rates to be supported in 802.11b standard
        [RosValue("supported-rates-b")]
        public string SupportedRatesB
        {
            get;
            set;
        }

        [RosValue("tdma-period-size")]
        public Int32? TdmaPeriodSize
        {
            get;
            set;
        }

        // Transmit power in dBm
        [RosValue("tx-power")]
        public string TxPower
        {
            get;
            set;
        }

        // Transmit power mode for the card
        [RosValue("tx-power-mode")]
        public TxPowerModeEnum? TxPowerMode
        {
            get;
            set;
        }

        // How often to update statistics in '/interface wireless registration-table'
        [RosValue("update-stats-interval")]
        public TimeSpan? UpdateStatsInterval
        {
            get;
            set;
        }

        [RosValue("wds-cost-range")]
        public string WdsCostRange // 0..4294967295
        {
            get;
            set;
        }

        // Default bridge for WDS interface
        [RosValue("wds-default-bridge")]
        public Ref<RouterOS.Interfaces.Bridges.Bridge>? WdsDefaultBridge
        {
            get;
            set;
        }

        [RosValue("wds-default-cost")]
        public Int32? WdsDefaultCost
        {
            get;
            set;
        }

        // Whether the AP will create WDS links with any other AP in this frequency
        [RosValue("wds-ignore-ssid")]
        public Boolean? WdsIgnoreSsid
        {
            get;
            set;
        }

        // WDS mode
        [RosValue("wds-mode")]
        public WdsModeEnum? WdsMode
        {
            get;
            set;
        }

        [RosValue("wireless-protocol")]
        public WirelessProtocol? WirelessProtocol
        {
            get;
            set;
        }

        [RosValue("wmm-support")]
        public WmmSupportEnum? WmmSupport
        {
            get;
            set;
        }

        [RosValue("running", IsDynamic = true)]
        public override bool? Running
        {
            get;
            set;
        }

        [RosValue("interface-type", IsDynamic = true)]
        public string InterfaceType
        {
            get;
            set;
        }

        public override string ToString()
        {
            return Name;
        }

        public static WirelessInterface Parse(string str, RouterOS.API.Connection conn)
        {
            if (string.IsNullOrEmpty(str))
                return null;
            WirelessInterface obj = new WirelessInterface();
            obj.FindByName(conn, str);
            return obj;
        }
    }
}
