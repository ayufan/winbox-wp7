﻿using System;
using System.Net;
using RouterOS.API;
using RouterOS.Serialization;


namespace RouterOS.Wireless
{
    [RosObject("interface wireless frequency-monitor", IsCommand = true)]
    public class FrequencyMonitor
    {
        [RosValue("freq", IsDynamic = true, IsKey = true)]
        public int? Frequency { get; set; }

        [RosValue("use", IsDynamic = true)]
        public int? Use { get; set; }

        [RosValue("nf", IsDynamic = true)]
        public int? NoiseFloor { get; set; }
    }

    public class FrequencyMonitorOptions : RosObject
    {
        [RosValue(".id")]
        public Ref<WirelessInterface>? Interface { get; set; }

        [RosValue("duration")]
        public TimeSpan? Duration { get; set; }

        [RosValue("freeze-frame-interval")]
        public TimeSpan? FreezeFrameInterval { get; set; }
    }
}
