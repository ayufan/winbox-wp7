﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using RouterOS.Serialization;


namespace RouterOS.Wireless
{

    [RosObject("interface wireless wds", CanAdd = true, CanSet = true, CanGet = true, CanRemove = true)]
    public class WDS : RosItemObject<WDS>
    {
        // Address Resolution Protocol
        [RosValue("arp")]
        public ARPMode? Arp
        {
            get;
            set;
        }

        // Short description of the item
        [RosValue("comment")]
        public override String Comment
        {
            get;
            set;
        }

        [RosValue("disable-running-check")]
        public Boolean? DisableRunningCheck
        {
            get;
            set;
        }

        // Defines whether item is ignored or used
        [RosValue("disabled")]
        public override Boolean? Disabled
        {
            get;
            set;
        }

        [RosValue("l2mtu")]
        public UInt16? L2mtu
        {
            get;
            set;
        }

        // Wireless interface which will be used by WDS
        [RosValue("master-interface")]
        public Ref<WirelessInterface>? MasterInterface
        {
            get;
            set;
        }

        // Maximum Transmit Unit
        [RosValue("mtu")]
        public Int32? Mtu
        {
            get;
            set;
        }

        // Interface name
        [RosValue("name")]
        public String Name
        {
            get;
            set;
        }

        // MAC address of the remote WDS host
        [RosValue("wds-address")]
        public MACAddress? WdsAddress
        {
            get;
            set;
        }

        [RosValue("dynamic", IsDynamic = true)]
        public override Boolean? Dynamic
        {
            get;
            set;
        }

        [RosValue("mac-address", IsDynamic = true)]
        public MACAddress? MacAddress
        {
            get;
            set;
        }

        [RosValue("running", IsDynamic = true)]
        public override Boolean? Running
        {
            get;
            set;
        }

        public static WDS Parse(string str, RouterOS.API.Connection conn)
        {
            if (string.IsNullOrEmpty(str))
                return null;
            WDS obj = new WDS();
            obj.FindByName(conn, str);
            return obj;
        }

        public override string ToString()
        {
            return Name.ToString();
        }

    }
}
