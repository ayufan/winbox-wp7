﻿using System;
using RouterOS.Serialization;


namespace RouterOS.Wireless
{
    [RosObject("interface wireless access-list", CanAdd = true, CanSet = true, CanMove = true, CanGet = true, CanRemove = true, CanUnset = true)]
    public class AccessList : RosItemObject<AccessList>
    {
        [RosValue("ap-tx-limit")]
        public Bits? ApTxLimit
        {
            get;
            set;
        }

        // Accept or not this client when ir tries to connect
        [RosValue("authentication")]
        public bool? Authentication
        {
            get;
            set;
        }

        [RosValue("client-tx-limit")]
        public Bits? ClientTxLimit
        {
            get;
            set;
        }

        // Short description of the item
        [RosValue("comment")]
        public override String Comment
        {
            get;
            set;
        }

        // Defines whether item is ignored or used
        [RosValue("disabled")]
        public override bool? Disabled
        {
            get;
            set;
        }

        // Forward or not the client's frames to other wireless client
        [RosValue("forwarding")]
        public bool? Forwarding
        {
            get;
            set;
        }

        // AP interface
        [RosValue("interface")]
        public Ref<WirelessInterface>? Interface
        {
            get;
            set;
        }

        // MAC address of the client
        [RosValue("mac-address")]
        public MACAddress? MacAddress
        {
            get;
            set;
        }

        [RosValue("management-protection-key")]
        public String ManagementProtectionKey
        {
            get;
            set;
        }

        // Encryption algorithm to use
        [RosValue("private-algo")]
        public WirelessEncryption? PrivateAlgo
        {
            get;
            set;
        }

        // Private key  of the client to validate during authentication
        [RosValue("private-key")]
        public String PrivateKey
        {
            get;
            set;
        }

        [RosValue("private-pre-shared-key")]
        public String PrivatePreSharedKey
        {
            get;
            set;
        }

        [RosValue("signal-range")]
        public String SignalRange
        {
            get;
            set;
        }

        [RosValue("time", CanUnset = true)]
        public String Time
        {
            get;
            set;
        }

        public string StatusText
        {
            get
            {
                if (Disabled.GetValueOrDefault())
                    return "D";
                else
                    return "";
            }
        }
    }
}
