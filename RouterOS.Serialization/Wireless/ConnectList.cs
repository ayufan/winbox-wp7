﻿using System;
using RouterOS.Serialization;


namespace RouterOS.Wireless
{
    [RosObject("interface wireless connect-list", CanAdd = true, CanSet = true, CanMove = true, CanGet = true, CanRemove = true, CanUnset = true)]
    public class ConnectList : RosItemObject<ConnectList>
    {
        [RosValue("area-prefix")]
        public String AreaPrefix
        {
            get;
            set;
        }

        // Short description of the item
        [RosValue("comment")]
        public override String Comment
        {
            get;
            set;
        }

        [RosValue("connect")]
        public Boolean? Connect
        {
            get;
            set;
        }

        // Defines whether item is ignored or used
        [RosValue("disabled")]
        public override Boolean? Disabled
        {
            get;
            set;
        }

        [RosValue("interface")]
        public Ref<WirelessInterface> Interface
        {
            get;
            set;
        }

        [RosValue("mac-address")]
        public MACAddress? MacAddress
        {
            get;
            set;
        }

        [RosValue("security-profile")]
        public Ref<SecurityProfile>? SecurityProfile
        {
            get;
            set;
        }

        [RosValue("signal-range")]
        public String SignalRange
        {
            get;
            set;
        }

        [RosValue("ssid")]
        public String Ssid
        {
            get;
            set;
        }

        [RosValue("wireless-protocol")]
        public String WirelessProtocol
        {
            get;
            set;
        }

        public string StatusText
        {
            get
            {
                if (Disabled.GetValueOrDefault())
                    return "D";
                else
                    return "";
            }
        }
    }
}
