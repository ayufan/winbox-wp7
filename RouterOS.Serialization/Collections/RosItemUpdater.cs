﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using RouterOS.API;
using System.Collections.Generic;
using System.Windows.Threading;

namespace RouterOS.Serialization
{
    public abstract class RosItemUpdater : IDisposable
    {
        public delegate void ItemRecord(Record record);

        public event ItemRecord RecordUpdated;

        public Record Record
        {
            get;
            protected set;
        }

        public abstract void Dispose();

        public abstract IEnumerable<RosValueMember> GetMembers();

        protected void OnRecordUpdated(Record record)
        {
            if (RecordUpdated != null)
                Deployment.Current.Dispatcher.BeginInvoke(RecordUpdated, record);
        }
    }

    public class RosItemUpdater<T> : RosItemUpdater where T : RosItemObject, new()
    {
        public delegate void ItemUpdated(T value);

        private Connection connection;
        private CommandTag getTag, listenTag, listenDeadTag;
        private T value = new T();
        private RecordId itemId;

        public event ItemUpdated Updated;
        public event ItemUpdated Dead;

        public T Value
        {
            get { return value; }
        }

        public RosItemUpdater(Connection connection, RecordId itemId)
        {
            if (connection == null)
                throw new NullReferenceException("no connection specified");
            this.connection = connection;
            this.itemId = itemId;
        }

        ~RosItemUpdater()
        {
            Stop();
        }

        public override IEnumerable<RosValueMember> GetMembers()
        {
            return RosObject.GetMembers(typeof(T));
        }

        private void ItemDeadUpdate(Connection conn, Record response)
        {
            if (response.Type == RecordType.Done)
            {
                if (response.Tag == listenDeadTag)
                    listenDeadTag = new CommandTag();
                return;
            }

            if (response.Type != RecordType.Record)
                return;

            if (!response.IsDead)
                return;

            Dispose();

            if (Dead != null)
                Deployment.Current.Dispatcher.BeginInvoke(Dead, value);
        }

        private void ItemUpdate(Connection conn, Record response)
        {
            if (response.Type == RecordType.Done)
            {
                if (response.Tag == getTag)
                    getTag = new CommandTag();
                if (response.Tag == listenTag)
                    listenTag = new CommandTag();
                return;
            }

            if (response.Type != RecordType.Record)
                return;
            if (!response.IsItem)
                return;
            response.CopyTo(value, conn);
            Record = response;
            if (Updated != null)
                Deployment.Current.Dispatcher.BeginInvoke(Updated, value);
            OnRecordUpdated(response);
        }

        public void Start()
        {
            RosObjectAttribute objectAttribute = RosItemObject.GetObjectAttribute(typeof(T));
            if (objectAttribute.CanGet)
            {
                if (!getTag.HasValue && (!listenTag.HasValue || !listenDeadTag.HasValue))
                {
                    getTag = connection.GetAll(objectAttribute.Scope, this.ItemUpdate, new ItemQuery(itemId));
                }
            }
            if (objectAttribute.CanListen)
            {
                if (!listenTag.HasValue)
                    listenTag = connection.Listen(objectAttribute.Scope, this.ItemUpdate, new ItemQuery(itemId));
                if (!listenDeadTag.HasValue)
                    listenDeadTag = connection.Listen(objectAttribute.Scope, this.ItemDeadUpdate, new ItemQuery(itemId), new EqualQuery(".dead", "true"));
            }
            else
            {
                if (!listenTag.HasValue)
                    listenTag = connection.GetAll(objectAttribute.Scope, TimeSpan.FromMilliseconds(objectAttribute.UseInterval), this.ItemUpdate, new ItemQuery(itemId));
            }
        }

        public void Stop()
        {
            if (getTag != null && connection != null)
            {
                connection.Cancel(ref getTag);
            }
            if (listenTag != null && connection != null)
            {
                connection.Cancel(ref listenTag);
            }
            if (listenDeadTag != null && connection != null)
            {
                connection.Cancel(ref listenDeadTag);
            }
        }

        public override void Dispose()
        {
            Stop();
            GC.SuppressFinalize(this);
        }
    }
}
