﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using RouterOS.API;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Threading;
using CommonLib;

namespace RouterOS.Serialization
{
    public class RosItemStats<T> : IEnumerable<T>, IDisposable where T : RosObject, new()
    {
        public delegate void ItemUpdated(T value, T lastValue);
        public delegate void ItemErrorOccurred(Record record);
        public delegate void ItemCompleted();

        private Connection connection;
        private Command cmd;
        private CommandTag tag;
        private UpdatableObservableCollection<T> items;
        private T lastItem;
        private int maxSamples;
        private RecordId itemId;
        private TimeSpan ts;
        private RosObject options;

        public event ItemUpdated Updated;
        public event ItemErrorOccurred ErrorOccurred;
        public event ItemCompleted Completed;

        public RosItemStats(Connection connection, RosObject options)
        {
            if (connection == null)
                throw new NullReferenceException("no connection specified");
            this.connection = connection;
            this.maxSamples = 0;
            this.items = new UpdatableObservableCollection<T>();
            this.options = options;
        }

        public RosItemStats(Connection connection, RecordId itemId, TimeSpan ts, int maxSamples)
        {
            if (connection == null)
                throw new NullReferenceException("no connection specified");
            this.connection = connection;
            this.maxSamples = maxSamples;
            this.items = new UpdatableObservableCollection<T>();
            this.itemId = itemId;
            this.ts = ts;
        }
        
        private void ItemUpdate(Connection conn, Record response)
        {
            if (response.Tag != tag)
                return;

            if (response.Type == RecordType.Error || response.Type == RecordType.Fatal)
            {
                if (ErrorOccurred != null)
                {
                    Deployment.Current.Dispatcher.BeginInvoke(() =>
                    {
                        if (ErrorOccurred != null)
                            ErrorOccurred(response);
                    });
                }
                return;
            }

            if (response.Type == RecordType.Done)
            {
                tag = new CommandTag();

                if (Completed != null)
                {
                    Deployment.Current.Dispatcher.BeginInvoke(() =>
                    {
                        if (Completed != null)
                            Completed();
                    });
                }
                return;
            }

            T newValue = new T();
            T oldValue = lastItem;
            response.CopyTo(newValue, conn);
            lastItem = newValue;

            Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                if (Updated != null)
                    Updated(newValue, oldValue);

                if (newValue is CommandKeyedObject)
                {
                    items.UpdateByHash(newValue);
                }
                else
                {
                    if (lastItem != null)
                        items.Add(newValue);
                    if (maxSamples != 0)
                    {
                        while (items.Count > maxSamples)
                            items.RemoveAt(0);
                    }
                }
            });
        }

        public void Start()
        {
            RosObjectAttribute objectAttribute = RosItemObject.GetObjectAttribute(typeof(T));

            if (objectAttribute.IsCommand)
            {
                if (tag)
                    return;
                CustomCommand customCmd = new CustomCommand();
                customCmd.Scope = objectAttribute.Scope;
                options.CopyTo(customCmd, connection);
                cmd = customCmd;
                tag = connection.ExecuteCommand(cmd, this.ItemUpdate);
            }
            else if (objectAttribute.CanGet)
            {
                if (tag)
                    return;

                cmd = new GetAllCommand(objectAttribute.Scope, new ItemQuery(itemId))
                {
                    Statistics = true,
                    Interval = ts
                };
                tag = connection.ExecuteCommand(cmd, this.ItemUpdate);
            }
        }

        public void Stop()
        {
            if (tag != null && connection != null)
            {
                connection.Cancel(ref tag);
            }
        }

        public void Dispose()
        {
            Stop();
            GC.SuppressFinalize(this);
        }

        public T this[int index]
        {
            get { return items[index]; }
        }

        public ObservableCollection<T> Items
        {
            get { return items; }
        }

        public int Count
        {
            get { return items.Count; }
        }

        public IEnumerator<T> GetEnumerator()
        {
            return items.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return items.GetEnumerator();
        }
    }
}
