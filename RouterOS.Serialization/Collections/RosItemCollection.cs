﻿using System;
using System.Net;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using RouterOS.API;
using System.Windows;
using System.Windows.Threading;
using System.Threading;
using CommonLib;

namespace RouterOS.Serialization
{
    public class RosItemCollection<T> : ICollection<T>, IDisposable where T : RosItemObject, new()
    {
        public delegate void ItemDelegate(T item);
        public delegate void ItemDone();

        public event ItemDelegate Added;
        public event ItemDelegate Removed;
        public event ItemDelegate Updated;
        public event ItemDone Completed;

        private UpdatableObservableCollection<T> items;
        private Dictionary<RecordId, T> itemToId;
        private Connection connection;
        private CommandTag getTag, listenTag, listenDeadTag;
        private Query[] query;
        private DateTime getStartTime;
        private TimeSpan? ts;

        public bool SendAdd { get; set; }
        public bool SendRemove { get; set; }
        public bool SendClear { get; set; }
        public bool LeaveForever { get; set; }

        public RosItemCollection(Connection connection, params Query[] query)
        {
            if (connection == null)
                throw new NullReferenceException("no connection specified");
            this.connection = connection;
            this.items = new FastObservableCollection<T>();
            this.query = query;
            this.itemToId = new Dictionary<RecordId, T>();

            SendAdd = true;
            SendRemove = true;
            SendClear = false;
            LeaveForever = false;
        }

        public RosItemCollection(Connection connection, TimeSpan ts, params Query[] query)
        {
            if (connection == null)
                throw new NullReferenceException("no connection specified");
            this.connection = connection;
            this.items = new FastObservableCollection<T>();
            this.query = query;
            this.itemToId = new Dictionary<RecordId, T>();
            this.ts = ts;

            SendAdd = true;
            SendRemove = true;
            SendClear = false;
            LeaveForever = false;
        }

        ~RosItemCollection()
        {
            Stop();
        }

        private T FindById(RecordId id)
        {
            T item;
            if (itemToId.TryGetValue(id, out item))
                return item;
            return null;
        }

        private T FindById(string idText)
        {
            if (string.IsNullOrEmpty(idText))
                return null;
            RecordId id = RecordId.Parse(idText, connection);
            return FindById(id);
        }

        private void ItemUpdate(Connection conn, Record response)
        {
            if (response.Type == RecordType.Done)
            {
                if (response.Tag == getTag)
                {
                    RosObjectAttribute objectAttribute = RosItemObject.GetObjectAttribute(typeof(T));
                    System.Diagnostics.Debug.WriteLine("[GETALL] ({0}) took {1}s", string.Join(" ", objectAttribute.Scope), DateTime.Now - getStartTime);

                    getTag = new CommandTag();
                    if (Completed != null)
                    {
                        Deployment.Current.Dispatcher.BeginInvoke(Completed);
                    }
                    return;
                }
                if (response.Tag == listenTag)
                {
                    listenTag = new CommandTag();
                }
                return;
            }

            if (response.Type != RecordType.Record)
                return;
            if (response.IsDead)
                return;
            if (!response.IsItem)
                return;
            if (response.Tag != getTag && response.Tag != listenTag)
                return;

            RecordId id = RecordId.Parse(response.ItemId, connection);
            T item = FindById(response.ItemId);
            if (item == null)
            {
                item = new T();
                lock (itemToId)
                {
                    itemToId[id] = item;
                }
            }

            response.CopyTo(item, conn);

            Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                items.Update(item);

                if (Updated != null)
                {
                    Updated(item);
                }
            });
        }

        private void ItemDeadUpdate(Connection conn, Record response)
        {
            if (response.Type == RecordType.Done)
            {
                if (response.Tag == listenDeadTag)
                    listenDeadTag = new CommandTag();
                return;
            }

            if (response.Type != RecordType.Record)
                return;
            if (!response.IsDead)
                return;
            if (!response.IsItem)
                return;

            T item = FindById(response.ItemId);
            if (item == null)
                return;

            if (LeaveForever)
            {
                if (Removed != null)
                    Deployment.Current.Dispatcher.BeginInvoke(Removed, item);
            }
            else
            {
                lock (itemToId)
                {
                    itemToId[item.Id.GetValueOrDefault()] = item;
                }
                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    items.Remove(item);
                    if (Removed != null)
                        Removed(item);
                });
            }
        }

        public T this[int index]
        {
            get { return items[index]; }
        }

        public ObservableCollection<T> Items
        {
            get { return items; }
        }

        public ObservableCollection<T> GetUserCollection(UserObservableCollection<T>.IsItem isItem)
        {
            return new UserObservableCollection<T>(Items, isItem);
        }

        public void Add(T item)
        {
            if (!item.Id.HasValue)
                throw new KeyNotFoundException("there's no id");

            if (SendAdd)
                item.Add(connection);

            lock (itemToId)
            {
                itemToId[item.Id.Value] = item;
            }

            Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                items.Update(item);
                if (Added != null)
                    Added(item);
            });
        }

        public void Clear()
        {
            if (SendClear)
            {
                foreach (T item in items)
                {
                    if (item.IsAdded)
                        item.Remove(connection);
                }
            }

            lock (itemToId)
            {
                itemToId.Clear();
            }
            Deployment.Current.Dispatcher.BeginInvoke(() => items.Clear());
        }

        public bool Contains(T item)
        {
            return itemToId.ContainsKey(item.Id.GetValueOrDefault());
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            throw new NotSupportedException();
        }

        public int Count
        {
            get { return items.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(T item)
        {
            if (!item.Id.HasValue)
                throw new KeyNotFoundException("there's no id");

            if (SendRemove)
            {
                if (items.Contains(item))
                {
                    item.Remove(connection);
                }
                else
                {
                    return false;
                }
            }

            lock (itemToId)
            {
                itemToId.Remove(item.Id.Value);
            }

            Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                if (items.Remove(item))
                {
                    if (Removed != null)
                        Removed(item);
                }
            });
            return false;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return items.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return items.GetEnumerator();
        }

        public void Start()
        {
            RosObjectAttribute objectAttribute = RosItemObject.GetObjectAttribute(typeof(T));

            if (objectAttribute.CanGet)
            {
                if (!getTag.HasValue && (!listenTag.HasValue || !listenDeadTag.HasValue))
                {
                    if(ts.HasValue)
                        getTag = connection.GetAll(objectAttribute.Scope, ts.Value, this.ItemUpdate, query);
                    else
                        getTag = connection.GetAll(objectAttribute.Scope, this.ItemUpdate, query);
                    getStartTime = DateTime.Now;
                }
            }
            if (objectAttribute.CanListen)
            {
                if (!listenTag.HasValue)
                    listenTag = connection.Listen(objectAttribute.Scope, this.ItemUpdate, query);
                if (!listenDeadTag.HasValue)
                    listenDeadTag = connection.Listen(objectAttribute.Scope, this.ItemDeadUpdate, new EqualQuery(".dead", "true"));
            }
        }

        public void Stop()
        {
            if (getTag.HasValue && connection != null)
            {
                connection.Cancel(ref getTag);
            }
            if (listenTag.HasValue && connection != null)
            {
                connection.Cancel(ref listenTag);
            }
            if (listenDeadTag.HasValue && connection != null)
            {
                connection.Cancel(ref listenDeadTag);
            }
        }

        public void Dispose()
        {
            Stop();
            GC.SuppressFinalize(this);
        }
    }
}
