﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using RouterOS.API;
using System.Threading;

namespace RouterOS.Serialization
{
    public class RosValueUpdater<T> : IDisposable where T:RosValueObject, new()
    {
        public delegate void ValueUpdated(T value);

        private Connection connection;
        private CommandTag getTag, listenTag;
        private T value = new T();

        public event ValueUpdated Updated;

        public T Value
        {
            get { return value; }
        }

        public RosValueUpdater(Connection connection)
        {
            if (connection == null)
                throw new NullReferenceException("no connection specified");
            this.connection = connection;
        }

        ~RosValueUpdater()
        {
            Stop();
        }

        public void Start()
        {
            RosObjectAttribute objectAttribute = RosItemObject.GetObjectAttribute(typeof(T));

            if (objectAttribute.CanGet)
            {
                if(!getTag && !listenTag)
                    getTag = connection.GetAll(objectAttribute.Scope, this.ItemUpdate);
            }
            if (objectAttribute.CanListen)
            {
                if(!listenTag)
                    listenTag = connection.Listen(objectAttribute.Scope, this.ItemUpdate);
            }
            else
            {
                if(!listenTag)
                    listenTag = connection.GetAll(objectAttribute.Scope, TimeSpan.FromMilliseconds(objectAttribute.UseInterval), this.ItemUpdate);
            }
        }

        public void Stop()
        {
            if (getTag != null && connection != null)
            {
                connection.Cancel(ref getTag);
            }

            if (listenTag != null && connection != null)
            {
                connection.Cancel(ref listenTag);
            }
        }

        private void ItemUpdate(Connection conn, Record response)
        {
            if (response.Tag != getTag && response.Tag != listenTag)
            {
                return;
            }

            if (response.Type == RecordType.Done)
            {
                if (response.Tag == getTag)
                    getTag = new CommandTag();
                if (response.Tag == listenTag)
                    listenTag = new CommandTag();
                return;
            }

            if (response.Type == RecordType.Error)
            {
                /*
                RosObjectAttribute objectAttribute = RosItemObject.GetObjectAttribute(typeof(T));

                if (response.Tag == getTag)
                {
                    if (objectAttribute.CanGet)
                        getTag = connection.GetAll(objectAttribute.Scope, this.ItemUpdate);
                }
                if (response.Tag == listenTag)
                {
                    if (objectAttribute.CanListen)
                        listenTag = connection.Listen(objectAttribute.Scope, this.ItemUpdate);
                    else
                        listenTag = connection.GetAll(objectAttribute.Scope, TimeSpan.FromMilliseconds(objectAttribute.UseInterval), this.ItemUpdate);
                }
                 * */
                return;
            }

            if (response.Type != RecordType.Record)
                return;
            if (response.IsItem)
                return;
            response.CopyTo(value, conn);
            if (Updated != null)
                Deployment.Current.Dispatcher.BeginInvoke(Updated, value);
        }

        public void Dispose()
        {
            Stop();
            GC.SuppressFinalize(this);
        }
    }
}
