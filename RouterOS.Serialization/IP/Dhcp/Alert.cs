﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RouterOS.Serialization;

namespace RouterOS.IP.Dhcp
{
    [RosObject("ip dhcp-server alert", CanAdd = true, CanDisable = true, CanEnable = true, CanGet = true, CanRemove = true, CanSet = true, CanUnset = true, CanListen = true)]
    public class Alert : RosItemObject<Alert>
    {
        [RosValue("alert-timeout")]
        public TimeSpan? AlertTimeout { get; set; }

        [RosValue("disabled")]
        public override bool? Disabled { get; set; }

        [RosValue("invalid", IsDynamic = true)]
        public override bool? Invalid { get; set; }

        [RosValue("unknown-server", IsDynamic = true)]
        public bool? UnknownServer { get; set; }

        [RosValue("comment")]
        public override string Comment { get; set; }

        [RosValue("interface", IsKey=true)]
        public Ref<Interface>? Interface { get; set; }

        [RosValue("on-alert")]
        public string OnAlert { get; set; }

        [RosValue("valid-server")]
        public bool? ValidServer { get; set; }
        
        public override string ToString()
        {
            return Interface.ToString();
        }
    }
}
