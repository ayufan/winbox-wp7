﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Net;
using RouterOS.Serialization;

namespace RouterOS.IP.Dhcp
{
    public enum BootpSupport
    {
        [RosValue("none")]
        None,
        [RosValue("dynamic")]
        Dynamic,
        [RosValue("static")]
        Static
    }

    public enum Authoritative
    {
        [RosValue("after-10sec-delay")]
        After10secDelay,
        [RosValue("after-2sec-delay")]
        After2secDelay,
        [RosValue("no")]
        No,
        [RosValue("yes")]
        Yes
    }
    
    [RosObject("ip dhcp-server", CanAdd = true, CanDisable = true, CanEnable = true, CanGet = true, CanRemove = true, CanSet = true, CanUnset = true, CanListen = true)]
    public class Server : RosItemObject<Server>
    {
        [RosValue("add-arp")]
        public bool? AddArp { get; set; }

        [RosValue("bootp-lease-time")]
        public TimeSpan? BootpLeaseTime { get; set; }

        [RosValue("interface", IsKey=true)]
        public Ref<Interface>? Interface { get; set; }

        [RosValue("relay")]
        public IPAddress Relay { get; set; }

        [RosValue("address-pool")]
        public Ref<Pool>? AddressPool { get; set; }

        [RosValue("bootp-support")]
        public BootpSupport? BootpSupport { get; set; }

        [RosValue("invalid", IsDynamic = true)]
        public override bool? Invalid { get; set; }

        [RosValue("src-address")]
        public IPAddress SrcAddress { get; set; }

        [RosValue("always-broadcast")]
        public bool? AlwaysBroadcast { get; set; }

        [RosValue("delay-threshold")]
        public TimeSpan? DelayThreshold { get; set; }

        [RosValue("lease-time", Formatter=typeof(RouterOS.Formatters.TimeSpanNoneFormatter))]
        public TimeSpan? LeaseTime { get; set; }

        [RosValue("use-radius")]
        public bool? UseRadius { get; set; }

        [RosValue("authoritative")]
        public Authoritative? Authoritative { get; set; }

        [RosValue("disabled")]
        public override bool? Disabled { get; set; }

        [RosValue("name")]
        public string Name { get; set; }

        public override string ToString()
        {
            return Name;
        }

        public string StatusText
        {
            get
            {
                string text = "";
                if (UseRadius.GetValueOrDefault())
                    text += "R";
                if (Invalid.GetValueOrDefault())
                    text += "I";
                if (Disabled.GetValueOrDefault())
                    text += "X";
                return text;
            }
        }
    }
}
