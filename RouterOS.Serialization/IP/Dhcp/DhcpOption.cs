﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RouterOS.Serialization;

namespace RouterOS.IP.Dhcp
{
    [RosObject("ip dhcp-server option", CanAdd = true, CanDisable = true, CanEnable = true, CanGet = true, CanRemove = true, CanSet = true, CanUnset = true, CanListen = true)]
    public class DhcpOption : RosItemObject<DhcpOption>
    {
        [RosValue("code")]
        public string Code { get; set; }

        [RosValue("name")]
        public string Name { get; set; }

        [RosValue("value")]
        public string Value { get; set; }
    }
}
