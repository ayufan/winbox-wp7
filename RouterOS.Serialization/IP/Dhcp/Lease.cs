﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

using RouterOS.Serialization;

namespace RouterOS.IP.Dhcp
{
    public enum LeaseStatus
    {
        [RosValue("bound")]
        Bound
    }

    [RosObject("ip dhcp-server lease", CanAdd = true, CanDisable = true, CanEnable = true, CanGet = true, CanRemove = true, CanSet = true, CanUnset = true, CanListen = true)]
    public class Lease : RosItemObject<Lease>
    {
        [RosValue("active-address", IsDynamic = true)]
        public IPAddress ActiveAddress { get; set; }

        [RosValue("agent-remote-id", IsDynamic = true)]
        public string AgentRemoteId { get; set; }

        [RosValue("dynamic", IsDynamic = true)]
        public override bool? Dynamic { get; set; }

        [RosValue("rate-limit")]
        public DblValue<Bits,Bits>? RateLimit { get; set; }

        [RosValue("active-client-id", IsDynamic = true)]
        public string ActiveClientId { get; set; }

        [RosValue("always-broadcast")]
        public bool? AlwaysBroadcast { get; set; }

        [RosValue("expires-after", IsDynamic = true)]
        public TimeSpan? ExpiresAfter { get; set; }

        [RosValue("server")]
        public Ref<Server>? Server { get; set; }

        [RosValue("active-mac-address", IsDynamic = true)]
        public MACAddress? ActiveMacAddress { get; set; }

        [RosValue("block-access")]
        public bool? BlockAccess { get; set; }

        [RosValue("host-name", IsDynamic = true)]
        public string HostName { get; set; }

        [RosValue("src-mac-address", IsDynamic = true)]
        public MACAddress? SrcMacAddress { get; set; }

        [RosValue("active-server", IsDynamic = true)]
        public Ref<Server>? ActiveServer { get; set; }

        [RosValue("blocked", IsDynamic = true)]
        public bool? Blocked { get; set; }

        [RosValue("last-seen", IsDynamic = true)]
        public TimeSpan? LastSeen { get; set; }

        [RosValue("status", IsDynamic = true)]
        public string Status { get; set; }

        [RosValue("address")]
        public IPAddress Address { get; set; }

        [RosValue("client-id")]
        public string ClientId { get; set; }

        [RosValue("lease-time")]
        public TimeSpan? LeaseTime { get; set; }

        [RosValue("use-src-mac")]
        public bool? UseSrcMac { get; set; }

        [RosValue("address-list")]
        public Ref<Firewall.AddressList>? AddressList { get; set; }

        [RosValue("comment")]
        public override string Comment { get; set; }

        [RosValue("mac-address")]
        public MACAddress? MacAddress { get; set; }

        [RosValue("agent-circuit-id", IsDynamic = true)]
        public string AgentCircuitId { get; set; }

        [RosValue("disabled")]
        public override bool? Disabled { get; set; }

        [RosValue("radius", IsDynamic = true)]
        public bool? Radius { get; set; }

        public Ref<Server>? CurrentServer
        {
            get
            {
                if (ActiveServer != null)
                    return ActiveServer;
                if (Server != null)
                    return Server;
                return null;
            }
        }

        public IPAddress CurrentAddress
        {
            get
            {
                if (ActiveAddress != null)
                    return ActiveAddress;
                if (Address != null)
                    return Address;
                return null;
            }
        }

        public string CurrentAddressWithStatus
        {
            get
            {
                IPAddress address = CurrentAddress;
                string status = StatusText;

                if (address != null)
                {
                    if (string.IsNullOrWhiteSpace(status))
                        return address.ToString();
                    else
                        return String.Format("{0} [{1}]", address, status);
                }

                return "(none)";
            }
        }

        public MACAddress? CurrentMacAddress
        {
            get
            {
                if (ActiveMacAddress != null)
                    return ActiveMacAddress;
                if (MacAddress != null)
                    return MacAddress;
                return null;
            }
        }

        public string StatusText
        {
            get
            {
                string text = "";
                if (Radius.GetValueOrDefault())
                    text += "R";
                if (Dynamic.GetValueOrDefault())
                    text += "D";
                if (Invalid.GetValueOrDefault())
                    text += "I";
                if (Disabled.GetValueOrDefault())
                    text += "X";
                return text;
            }
        }
    }
}
