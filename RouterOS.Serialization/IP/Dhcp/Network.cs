﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

using RouterOS.Serialization;

namespace RouterOS.IP.Dhcp
{
    [RosObject("ip dhcp-server network", CanAdd = true, CanDisable = true, CanEnable = true, CanGet = true, CanRemove = true, CanSet = true, CanUnset = true, CanListen = true)]
    public class Network : RosItemObject<Network>
    {
        [RosValue("address")]
        public string Address { get; set; }

        [RosValue("comment")]
        public override string Comment { get; set; }

        [RosValue("dns-server")]
        public ItemList<IPAddress> DnsServer { get; set; }

        [RosValue("gateway")]
        public ItemList<IPAddress> Gateway { get; set; }

        [RosValue("next-server")]
        public IPAddress NextServer { get; set; }

        [RosValue("wins-server")]
        public ItemList<IPAddress> WinsServer { get; set; }

        [RosValue("boot-file-name")]
        public string BootFileName { get; set; }

        [RosValue("dhcp-option")]
        public ItemList<Ref<DhcpOption>> DhcpOption { get; set; }

        [RosValue("domain")]
        public string Domain { get; set; }

        [RosValue("netmask")]
        public int? Netmask { get; set; }

        [RosValue("ntp-server")]
        public ItemList<IPAddress> NtpServer { get; set; }

        public string StatusText
        {
            get
            {
                string text = "";
                return text;
            }
        }
    }
}
