﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;

using RouterOS.Interfaces;
using RouterOS.Serialization;

namespace RouterOS.IP
{
    [RosObject("ip route", CanAdd = true, CanDisable = true, CanEnable = true, CanGet = true, CanRemove = true, CanSet = true, CanUnset = true, CanListen = true)]
    public class Route : RosItemObject<Route>
    {
        [RosValue("dst-address", Default="0.0.0.0/0")]
        public IPPrefix DstAddress { get; set; }

        [RosValue("gateway")]
        public ItemList<Ref<Interface>> Gateway { get; set; }

        [RosValue("pref-src")]
        public IPAddress PrefSource { get; set; }

        [RosValue("distance")]
        public int? Distance { get; set; }

        [RosValue("active", IsDynamic=true)]
        public bool? Active { get; set; }

        [RosValue("static", IsDynamic=true)]
        public bool? Static { get; set; }

        [RosValue("disabled")]
        public override bool? Disabled { get; set; }

        [RosValue("dynamic", IsDynamic=true)]
        public override bool? Dynamic { get; set; }

        public string StatusText
        {
            get
            {
                string text = "";
                if (Active.GetValueOrDefault())
                    text += "A";
                if (Static.GetValueOrDefault())
                    text += "S";
                if (Dynamic.GetValueOrDefault())
                    text += "D";
                if (Disabled.GetValueOrDefault())
                    text += "X";
                return text;
            }
        }
    }
}
