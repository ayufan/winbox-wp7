﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;

using RouterOS.Interfaces;
using RouterOS.Serialization;

namespace RouterOS
{
    namespace IP
    {
        [RosObject("ip address", CanAdd=true, CanDisable=true, CanEnable=true, CanGet=true, CanListen=true, CanRemove=true, CanSet=true, CanUnset=true)]
        public sealed class Address : RosItemObject<Address>
        {
            [RosValue("address")]
            public IPPrefix IP { get; set; }

            [RosValue("network")]
            public IPAddress Network { get; set; }

            [RosValue("broadcast")]
            public IPAddress Broadcast { get; set; }

            [RosValue("interface")]
            public Ref<Interface>? Interface { get; set; }

            [RosValue("actual-interface", IsDynamic=true)]
            public Ref<Interface>? ActualInterface { get; set; }

            [RosValue("invalid", IsDynamic=true)]
            public override bool? Invalid { get; set; }

            [RosValue("dynamic", IsDynamic=true)]
            public override bool? Dynamic { get; set; }

            [RosValue("disabled", Default="true")]
            public override bool? Disabled { get; set; }

            [RosValue("comment")]
            public override string Comment { get; set; }

            public string StatusText
            {
                get
                {
                    string text = "";
                    if (Dynamic.GetValueOrDefault())
                        text += "D";
                    if (Invalid.GetValueOrDefault())
                        text += "I";
                    if (Disabled.GetValueOrDefault())
                        text += "X";
                    return text;
                }
            }
        }
    }
}
