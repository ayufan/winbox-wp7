﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

using RouterOS.Interfaces;
using RouterOS.Serialization;

namespace RouterOS.IP
{
    [RosObject("ip arp", CanAdd = true, CanDisable = true, CanEnable = true, CanGet = true, CanRemove = true, CanSet = true, CanListen = true)]
    public sealed class ARP : RosItemObject<ARP>
    {
        [RosValue("address", IsKey=true)]
        public IPAddress Address { get; set; }

        [RosValue("comment", Default="")]
        public override string Comment { get; set; }

        [RosValue("disabled", Default="true")]
        public override bool? Disabled { get; set; }

        [RosValue("interface", IsKey=true)]
        public Ref<Interface>? Interface { get; set; }

        [RosValue("mac-address")]
        public MACAddress MacAddress { get; set; }

        [RosValue("dynamic", IsDynamic = true)]
        public override bool? Dynamic { get; set; }

        [RosValue("invalid", IsDynamic = true)]
        public override bool? Invalid { get; set; }

        [RosValue("DHCP", IsDynamic = true)]
        public bool? IsDHCP { get; set; }

        public string StatusText
        {
            get
            {
                string text = "";
                if (IsDHCP.GetValueOrDefault())
                    text += "L";
                if (Dynamic.GetValueOrDefault())
                    text += "D";
                if (Invalid.GetValueOrDefault())
                    text += "I";
                if (Disabled.GetValueOrDefault())
                    text += "X";
                return text;
            }
        }
    }
}
