﻿using System;
using System.Collections.Generic;
using System.Text;

using RouterOS.Serialization;

namespace RouterOS.IP
{
    [RosObject("ip service", CanAdd = true, CanDisable = true, CanEnable = true, CanGet = true, CanRemove = true, CanSet = true, CanListen = true)]
    public class Service : RosItemObject<Service>
    {
        [RosValue("name", ReadOnly = true, IsKey = true)]
        public string Name { get; set; }

        [RosValue("port")]
        public ushort? Port { get; set; }

        [RosValue("address")]
        public IPMask Address { get; set; }

        [RosValue("certificate")]
        public string Certificate { get; set; }

        [RosValue("disabled")]
        public override bool? Disabled { get; set; }
    }
}
