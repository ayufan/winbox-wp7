﻿using System;
using System.Collections.Generic;
using System.Text;

using RouterOS.Serialization;

namespace RouterOS.IP.Firewall.Connection
{
    [RosObject("ip firewall connection tracking", CanGet = true, CanSet = true, CanListen = true)]
    public class Tracking : RosValueObject<Tracking>
    {
        [RosValue("enabled", Default="true")]
        public bool? Enabled { get; set; }

        [RosValue("tcp-syn-sent-timeout")]
        public string TcpSynSentTimeout { get; set; }

        [RosValue("tcp-syn-received-timeout")]
        public string TcpSynReceivedTimeout { get; set; }
     
        [RosValue("tcp-established-timeout")]
        public string TcpSynEstablishedTimeout { get; set; }
     
        [RosValue("tcp-fin-wait-timeout")]
        public string TcpFinWaitTimeout { get; set; }

        [RosValue("tcp-fin-close-timeout")]
        public string TcpFinCloseTimeout { get; set; }

        [RosValue("tcp-last-close-timeout")]
        public string TcpLastCloseTimeout { get; set; }
        
        [RosValue("tcp-time-wait-timeout")]
        public string TcpTimeWaitTimeout { get; set; }
        
        [RosValue("tcp-close-timeout")]
        public string TcpCloseTimeout { get; set; }
        
        [RosValue("udp-timeout")]
        public string UdpTimeout { get; set; }

        [RosValue("udp-stream-timeout")]
        public string UdpStreamTimeout { get; set; }
        
        [RosValue("icmp-timeout")]
        public string IcmpTimeout { get; set; }
        
        [RosValue("generic-timeout")]
        public string GenericTimeout { get; set; }
        
        [RosValue("tcp-syncookie")]
        public bool? TcpSyncookie { get; set; }
        
        [RosValue("max-entries", IsDynamic=true)]
        public int? MaxEntries { get; set; }
        
        [RosValue("total-entries", IsDynamic=true)]
        public int? TotalEntries { get; set; }
    }
}
