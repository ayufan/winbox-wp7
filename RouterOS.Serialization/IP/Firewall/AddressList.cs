﻿using System;
using System.Collections.Generic;
using System.Text;

using RouterOS.Serialization;

namespace RouterOS.IP.Firewall
{
    [RosObject("ip firewall address-list", CanAdd = true, CanDisable = true, CanEnable = true, CanGet = true, CanRemove = true, CanSet = true, CanListen = true)]
    public sealed class AddressList : RosItemObject<AddressList>
    {
        [RosValue("list", IsKey = true)]
        public string List { get; set; }

        [RosValue("address", IsKey=true)]
        public IPMask? Address { get; set; }

        [RosValue("dynamic", IsDynamic=true)]
        public override bool? Dynamic { get; set; }

        [RosValue("disabled")]
        public override bool? Disabled { get; set; }

        [RosValue("comment", Default="")]
        public override string Comment { get; set; }

        public string StatusText
        {
            get
            {
                string text = "";
                if (Dynamic.GetValueOrDefault())
                    text += "D";
                if (Disabled.GetValueOrDefault())
                    text += "X";
                return text;
            }
        }
    }
}
