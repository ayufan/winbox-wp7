﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Net;
using RouterOS.Serialization;

namespace RouterOS.IP
{
    [RosObject("ip pool", CanAdd = true, CanDisable = true, CanEnable = true, CanGet = true, CanRemove = true, CanSet = true, CanUnset = true, CanListen = true)]
    public class Pool : RosItemObject<Pool>
    {
        [RosValue("name")]
        public string Name { get; set; }

        [RosValue("name")]
        public ItemList<IPRange> Ranges { get; set; }

        [RosValue("next-pool")]
        public ItemList<Ref<Pool>> NextPool { get; set; }
    }
}
