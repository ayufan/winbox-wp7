﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;
using RouterOS.API;

namespace RouterOS.Serialization
{
    public partial class RosObject
    {
        internal delegate void CopyTo(Record record, RosObject obj, Connection conn);

        private static Dictionary<Type, CopyTo> copyToTypes = new Dictionary<Type, CopyTo>();

        internal static CopyTo GetCopyTo(Type objType)
        {
            CopyTo copyTo;
            if (!copyToTypes.TryGetValue(objType, out copyTo))
            {
                lock (copyToTypes)
                {
                    if (!copyToTypes.TryGetValue(objType, out copyTo))
                    {
                        copyTo = CreateCopyTo(objType);
                        copyToTypes[objType] = copyTo;
                    }
                }
            }
            return copyTo;
        }

        private static CopyTo CreateCopyTo(Type objType)
        {
            var method = new DynamicMethod("", null, new Type[] { typeof(Record), typeof(RosObject), typeof(Connection) });
            var il = method.GetILGenerator();

            ConstructorInfo rosKeyCtor = typeof(CommandKey).GetConstructor(new Type[] { typeof(int) });
            MethodInfo tryGetValue = typeof(RouterOS.API.Record).GetMethod("TryGetValue");
            LocalBuilder obj = il.DeclareLocal(objType);
            LocalBuilder value = il.DeclareLocal(typeof(string));

            il.Emit(OpCodes.Ldarg_1);
            il.Emit(OpCodes.Castclass, objType);
            il.Emit(OpCodes.Stloc, obj);

            foreach (var member in RosObject.GetMembers(objType))
            {
                il.BeginExceptionBlock();

                CommandKey key = new CommandKey(member.Name);
                Type type = member.GetMemberType();

                Label setNull = il.DefineLabel();
                Label nextValue = il.DefineLabel();

                il.Emit(OpCodes.Ldarg_0);
                il.Emit(OpCodes.Ldc_I4, key.Hash);
                il.Emit(OpCodes.Newobj, rosKeyCtor);
                il.Emit(OpCodes.Ldloca, value);
                il.Emit(OpCodes.Callvirt, tryGetValue);
                il.Emit(OpCodes.Brfalse, setNull);

                il.Emit(OpCodes.Ldloc, obj);

                TypeFormatter.EmitConvertFromString(il, type, value);

                if (member.Member is PropertyInfo)
                {
                    PropertyInfo info = (PropertyInfo)member.Member;
                    il.Emit(OpCodes.Callvirt, info.GetSetMethod());
                }
                else if (member.Member is FieldInfo)
                {
                    throw new NotSupportedException();
                }

                il.Emit(OpCodes.Br, nextValue);

                il.MarkLabel(setNull);

                il.Emit(OpCodes.Ldloc, obj);

                if (type.IsClass)
                {
                    il.Emit(OpCodes.Ldnull);
                }
                else
                {
                    LocalBuilder local = il.DeclareLocal(type);
                    il.Emit(OpCodes.Ldloca, local);
                    il.Emit(OpCodes.Initobj, type);
                    il.Emit(OpCodes.Ldloc, local);
                }

                if (member.Member is PropertyInfo)
                {
                    PropertyInfo info = (PropertyInfo)member.Member;
                    il.Emit(OpCodes.Callvirt, info.GetSetMethod());
                }
                else if (member.Member is FieldInfo)
                {
                    throw new NotSupportedException();
                }

                il.MarkLabel(nextValue);

                il.BeginCatchBlock(typeof(Exception));

                il.EmitWriteLine("[PARSE EXCEPTION] For member: " + member.Name + " in " + objType.Name);

                il.EndExceptionBlock();
            }

            il.Emit(OpCodes.Ret);

            return (CopyTo)method.CreateDelegate(typeof(CopyTo));
        }
    }
}
