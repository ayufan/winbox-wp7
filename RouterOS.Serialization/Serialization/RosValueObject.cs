﻿using System;
using System.Collections.Generic;
using System.Text;
using RouterOS.API;

namespace RouterOS.Serialization
{
    public abstract class RosValueObject : RosObject
    {
        public void Get(Connection conn)
        {
            Record record = conn.ExecuteOneRecord(new GetAllCommand(Attributes.Scope));

            if (record != null)
            {
                record.CopyTo(this, conn);
            }
        }

        public void Save(Connection conn)
        {
            // try to update object
            if (SavedState.Count == 0)
            {
                if(Attributes.CanGet)
                    Get(conn);
            }

            // create command
            SetCommand command = new SetCommand(Attributes.Scope);

            // get changes
            this.CopyTo(command, conn);

            if (command.KeyValues.Count == 0)
                return;

            // execute command
            conn.ExecuteCommand(command);

            // save changes
            SaveChanges();
        }
    }

    public class RosValueObject<T> : RosValueObject where T : RosValueObject<T>, new()
    {
        #region GetAll - Sync
        public static T Get(Connection connection, params Query[] query)
        {
            var objectAttribute = GetObjectAttribute(typeof(T));
            if (!objectAttribute.CanGet)
                throw new NotSupportedException();

            Record record = connection.ExecuteOneRecord(new GetAllCommand(objectAttribute.Scope, query));
            if (record != null)
            {
                T obj = new T();
                record.CopyTo(obj, connection);
                return obj;
            }
            return null;
        }
        #endregion

        #region Get - Async
        public delegate void GetAllCallback(RecordType type, T obj);

        private struct GetAllObject
        {
            public GetAllCallback Callback;
            public T Object;

            public void OnEvent(Connection conn, Record record)
            {
                if (record.Type == RecordType.Record)
                {
                    Object = new T();
                    record.CopyTo(Object, conn);
                }
                else if (record.Type == RecordType.Done)
                {
                    Callback(record.Type, Object);
                }
                else
                {
                    Callback(record.Type, null);
                }
            }
        }

        public static object GetAll(Connection connection, GetAllCallback callback, params Query[] query)
        {
            var objectAttribute = GetObjectAttribute(typeof(T));
            if (!objectAttribute.CanGet)
                throw new NotSupportedException();

            GetAllCommand command = new GetAllCommand(objectAttribute.Scope, query);
            GetAllObject getAll = new GetAllObject() { Callback = callback };

            return connection.ExecuteCommand(command, getAll.OnEvent);
        }

        public static object GetAll(Connection connection, GetAllCallback callback, T queriedObject)
        {
            return GetAll(connection, callback, queriedObject.BuildQuery(connection));
        }
        #endregion

        #region Listen - Async
        public delegate void ListenCallback(ListenResults result);

        private struct ListenObject
        {
            public ListenCallback Callback;

            public void OnEvent(Connection conn, Record record)
            {
                ListenResults results = new ListenResults();
                results.Connection = conn;
                results.Tag = record.Tag;
                results.Type = record.Type;

                if (record.Type == RecordType.Record)
                {
                    results.Record = new T();
                    record.CopyTo(results.Record, conn);
                }

                Callback(results);
            }
        }

        public struct ListenResults
        {
            public Connection Connection;
            public RecordType Type;
            public object Tag;
            public T Record;
        }

        public static object Listen(Connection connection, ListenCallback callback, params Query[] query)
        {
            var objectAttribute = GetObjectAttribute(typeof(T));
            if (!objectAttribute.CanListen)
                throw new NotSupportedException();

            ListenCommand command = new ListenCommand(objectAttribute.Scope);
            command.Query.AddRange(query);

            ListenObject listen = new ListenObject();
            listen.Callback = callback;

            return connection.ExecuteCommand(command, listen.OnEvent);
        }

        public static object Listen(Connection connection, ListenCallback callback, T queriedObject)
        {
            return Listen(connection, callback, queriedObject.BuildQuery(connection));
        }
        #endregion
    }
}
