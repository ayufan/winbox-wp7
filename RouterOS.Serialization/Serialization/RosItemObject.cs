﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RouterOS.API;

namespace RouterOS.Serialization
{
    public class CommandKeyedObject : RosObject
    {
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    public class RosItemObject : RosObject
    {
        [RosValue(".id")]
        public RecordId? Id { get; set; }

        public bool IsNew
        {
            get { return Id.HasValue == false; }
        }

        public bool IsAdded
        {
            get { return Id.HasValue == true; }
        }

        public virtual bool? Disabled
        {
            get { return false; }
            set { }
        }

        public virtual bool? Invalid
        {
            get { return false; }
            set { }
        }

        public virtual bool? Dynamic
        {
            get { return false; }
            set { }
        }

        public virtual bool? Inactive
        {
            get { return false; }
            set { }
        }

        public virtual bool? Running
        {
            get { return false; }
            set { }
        }

        public virtual bool? Slave
        {
            get { return false; }
            set { }
        }

        public virtual string Comment
        {
            get { return null; }
            set { }
        }

        public Query[] BuildQuery()
        {
            List<Query> query = new List<Query>();
            foreach (var keyValue in GetMembers(true, false))
                query.Add(new EqualQuery(keyValue.Name, keyValue.GetValue(this).ToString()));
            return query.ToArray();
        }

        public Query[] BuildKeyQuery()
        {
            List<Query> query = new List<Query>();
            foreach (var member in GetKeyMembers())
                query.Add(new EqualQuery(member.Name, member.GetValue(this).ToString()));
            return query.ToArray();
        }

        public void Remove(Connection conn)
        {
            if (!Attributes.CanRemove)
                throw new InvalidOperationException("item can't be removed!");
            if (IsNew)
                throw new InvalidOperationException("cannot remove not added object");
            conn.ExecuteCommand(new RemoveCommand(Attributes.Scope, Id.Value));
        }

        public object Remove(Connection conn, AsyncResponseCallback callback)
        {
            if (!Attributes.CanRemove)
                throw new InvalidOperationException("item can't be removed!");
            if (IsNew)
                throw new InvalidOperationException("cannot remove not added object");
            return conn.ExecuteCommand(new RemoveCommand(Attributes.Scope, Id.Value), callback);
        }

        public void FindByQuery(Connection connection, CommandKey[] propList, Query[] query)
        {
            if (!Attributes.CanGet)
                throw new NotSupportedException();
            if (query != null && query.Length == 0)
                throw new InvalidOperationException();
            GetAllCommand command = new GetAllCommand(Attributes.Scope);
            if (query != null)
                command.Query.AddRange(query);
            if (propList != null)
                command.PropList.AddRange(propList);
            Response response = connection.ExecuteCommand(command);
            if (response.Records.Length == 0)
                throw new KeyNotFoundException();
            else if (response.Records.Length > 1)
                throw new OverflowException();
            response.Records[0].CopyTo(this, connection);
        }

        public void FindByQuery(Connection connection, params Query[] query)
        {
            FindByQuery(connection, null, query);
        }

        public void FindByName(Connection connection, string value)
        {
            FindByQuery(connection, null, new Query[] { new EqualQuery("name", value) });
        }

        public void FindByKeys(Connection conn)
        {
            if (IsAdded)
            {
                throw new InvalidOperationException("item is already found");
            }
            FindByQuery(conn, new CommandKey[] { ".id" }, BuildKeyQuery());
        }

        public void Get(Connection conn)
        {
            GetAllCommand command = new GetAllCommand(Attributes.Scope);

            // try to find item
            if (IsNew)
            {
                command.Query.AddRange(BuildKeyQuery());

                if (command.Query.Count == 0)
                    throw new Exception("Object doesn't contain any keys, so can't be updated!");
            }

            // got object of id
            else
            {
                command.Query.AddItem(Id.Value);
            }

            // save values
            Record record = conn.ExecuteOneRecord(command);
            if (record != null)
                record.CopyTo(this, conn);
        }

        public void Add(Connection conn)
        {
            if (!Attributes.CanAdd)
                throw new InvalidOperationException("item can't be added!");

            if (IsAdded)
                throw new InvalidOperationException("item is already added");

            AddCommand command = new AddCommand(Attributes.Scope);

            // build command
            foreach (var keyValue in GetMembers(false, false))
                command.KeyValues[keyValue.Name] = keyValue.GetValue(this).ToString(conn);

            // execute command
            Response response = conn.ExecuteCommand(command);

            // save object id
            if (IsNew && response.Ret != null)
                Id = RecordId.Parse(response.Ret, conn);

            // apply changes to internal list
            SaveChanges();
        }

        public void Set(Connection conn)
        {
            if (!Attributes.CanSet)
                throw new InvalidOperationException("item can't be set!");

            if (IsNew)
                throw new InvalidOperationException("item is not yet added");

            SetCommand setCommand = new SetCommand(Attributes.Scope);
            UnsetCommand unsetCommand = new UnsetCommand(Attributes.Scope);

            // build command
            foreach (var member in GetChangedMembers())
            {
                string value = member.GetValue(this).ToString(conn);

                if (member.Attribute.CanUnset)
                    if (value == member.Default)
                        unsetCommand.ValueNames.Add(member.Name);
                    else
                        value = null;
                else
                    setCommand.KeyValues[member.Name] = value;
            }

            // execute command
            if (Attributes.CanSet)
                conn.ExecuteCommand(setCommand);
            if (Attributes.CanUnset)
                conn.ExecuteCommand(unsetCommand);

            // save state
            SaveChanges();
        }

        public void Update(Connection conn)
        {
            if (IsNew)
            {
                bool hasEmptyKey = GetKeyMembers().Any(m => m.GetValue(this) == null);

                if (hasEmptyKey)
                {
                    Add(conn);
                    return;
                }

                FindByKeys(conn);
            }
            Set(conn);
        }

        public object Enable(Connection conn, AsyncResponseCallback callback)
        {
            if (!Attributes.CanEnable)
                throw new InvalidOperationException("item can't be enabled!");

            if (IsNew)
                throw new InvalidOperationException("item is not yet added");

            EnableCommand enableCommand = new EnableCommand(Attributes.Scope, Id.Value);
            return conn.ExecuteCommand(enableCommand, callback);
        }

        public object Disable(Connection conn, AsyncResponseCallback callback)
        {
            if (!Attributes.CanDisable)
                throw new InvalidOperationException("item can't be enabled!");

            if (IsNew)
                throw new InvalidOperationException("item is not yet added");

            DisableCommand enableCommand = new DisableCommand(Attributes.Scope, Id.Value);
            return conn.ExecuteCommand(enableCommand, callback);
        }

        public object SetComment(Connection conn, string newComment, AsyncResponseCallback callback)
        {
            if (!Attributes.CanSet)
                throw new InvalidOperationException("item can't be enabled!");

            if (IsNew)
                throw new InvalidOperationException("item is not yet added");

            SetCommand setCommand = new SetCommand(Attributes.Scope);
            setCommand[".id"] = Id.ToString();
            setCommand["comment"] = newComment;
            return conn.ExecuteCommand(setCommand, callback);
        }

        public override int GetHashCode()
        {
            if (Id.HasValue)
                return Id.Value.GetHashCode();
            return base.GetHashCode();
        }
    }

    public class RosItemObject<T> : RosItemObject where T : RosItemObject<T>, new()
    {
        #region GetAll - Sync
        private static T[] GetRecords(Response response)
        {
            T[] objects = new T[response.Records.Length];
            for (int i = 0; i < response.Records.Length; ++i)
            {
                objects[i] = new T();
                response.Records[i].CopyTo(objects[i], response.Connection);
            }
            return objects;
        }

        public static T[] GetAll(Connection connection, params Query[] query)
        {
            var objectAttribute = GetObjectAttribute(typeof(T));
            if (!objectAttribute.CanGet)
                throw new NotSupportedException();

            GetAllCommand command = new GetAllCommand(objectAttribute.Scope, query);
            // command["stats"] = "";
            return GetRecords(connection.ExecuteCommand(command));
        }

        public static T[] GetAll(Connection connection, T queriedObject)
        {
            return GetAll(connection, queriedObject.BuildQuery());
        }
        #endregion

        #region GetAll - Async
        public delegate void GetAllCallback(RecordType type, T[] objects);

        private struct GetAllObject
        {
            public GetAllCallback Callback;
            public List<T> Objects;

            public void OnEvent(Connection conn, Record record)
            {
                if (record.Type == RecordType.Record)
                {
                    T obj = new T();
                    obj.Id = RecordId.Parse(record.ItemId, conn);
                    record.CopyTo(obj, conn);
                    Objects.Add(obj);
                }
                else if (record.Type == RecordType.Done)
                {
                    Callback(record.Type, Objects.ToArray());
                }
                else
                {
                    Callback(record.Type, null);
                }
            }
        }

        public static CommandTag GetAll(Connection connection, GetAllCallback callback, params Query[] query)
        {
            var objectAttribute = GetObjectAttribute(typeof(T));
            if (!objectAttribute.CanGet)
                throw new NotSupportedException();

            GetAllCommand command = new GetAllCommand(objectAttribute.Scope, query);

            GetAllObject getAll = new GetAllObject() { Callback = callback, Objects = new List<T>() };

            return connection.ExecuteCommand(command, new AsyncResponseCallback(getAll.OnEvent));
        }

        public static CommandTag GetAll(Connection connection, GetAllCallback callback, T queriedObject)
        {
            return GetAll(connection, callback, queriedObject.BuildQuery());
        }
        #endregion

        #region Listen - Async
        public delegate void ListenCallback(ListenResults result);

        private struct ListenObject
        {
            public ListenCallback Callback;

            public void OnEvent(Connection conn, Record record)
            {
                ListenResults results = new ListenResults();
                results.Connection = conn;
                results.Tag = record.Tag;
                results.Type = record.Type;

                if (record.Type == RecordType.Record)
                {
                    results.Id = RecordId.Parse(record.ItemId, conn);
                    results.Record = new T();
                    record.CopyTo(results.Record, conn);
                }

                Callback(results);
            }
        }

        public struct ListenResults
        {
            public Connection Connection;
            public RecordType Type;
            public CommandTag Tag;
            public T Record;
            public RecordId Id;
        }

        public static CommandTag Listen(Connection connection, ListenCallback callback, params Query[] query)
        {
            var objectAttribute = GetObjectAttribute(typeof(T));
            if (!objectAttribute.CanListen)
                throw new NotSupportedException();

            ListenCommand command = new ListenCommand(objectAttribute.Scope);
            command.Query.AddRange(query);

            ListenObject listen = new ListenObject();
            listen.Callback = callback;

            return connection.ExecuteCommand(command, new AsyncResponseCallback(listen.OnEvent));
        }

        public static CommandTag Listen(Connection connection, ListenCallback callback, T queriedObject)
        {
            return Listen(connection, callback, queriedObject.BuildQuery());
        }
        #endregion
    }
}
