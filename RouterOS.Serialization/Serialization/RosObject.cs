﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using RouterOS.API;

namespace RouterOS.Serialization
{
    public partial class RosObject
    {
        private static Dictionary<Type, RosValueMember[]> m_members = new Dictionary<Type, RosValueMember[]>();
        private static Dictionary<Type, RosObjectAttribute> m_attributes = new Dictionary<Type, RosObjectAttribute>();
        private Dictionary<CommandKey, string> m_savedState = new Dictionary<CommandKey, string>();

        public DateTime LastUpdateTime { get; set; }

        public RosObject()
        {
        }

        public Dictionary<CommandKey, string> SavedState
        {
            get { return m_savedState; }
            set { m_savedState = value; }
        }

        public RosObjectAttribute Attributes
        {
            get { return GetObjectAttribute(GetType()); }
        }

        public static RosObjectAttribute GetObjectAttribute(Type type)
        {
            RosObjectAttribute attribute;
            if (m_attributes.TryGetValue(type, out attribute))
                return attribute;

            object[] objectAttribute = type.GetCustomAttributes(typeof(RosObjectAttribute), true);
            if (objectAttribute.Length == 0)
                throw new NotSupportedException("Class not supported: " + type.ToString());

            attribute = (RosObjectAttribute)objectAttribute[0];
            m_attributes.Add(type, attribute);
            return attribute;
        }
        
        private static RosValueAttribute GetValueAttribute(MemberInfo member)
        {
            object[] serializableAttribute = member.GetCustomAttributes(typeof(RosValueAttribute), true);
            if (serializableAttribute.Length != 0)
                return ((RosValueAttribute)serializableAttribute[0]);
            return null;
        }

        public static RosValueMember[] GetMembers(Type type)
        {
            RosValueMember[] retValue;

            if (m_members.TryGetValue(type, out retValue))
                return retValue;

            List<RosValueMember> members = new List<RosValueMember>();

            foreach (var member in type.FindMembers(MemberTypes.Field | MemberTypes.Property, BindingFlags.Public | BindingFlags.Instance, null, null))
            {
                RosValueAttribute serializableAttribute = GetValueAttribute(member);
                if (serializableAttribute == null)
                    continue;
                members.Add(new RosValueMember() { Key = new CommandKey(member.Name), Member = member, Attribute = serializableAttribute });
            }

            retValue = members.ToArray();
            m_members[type] = retValue;
            return retValue;
        }

        public RosValueMember[] GetMembers()
        {
            return GetMembers(GetType());
        }

        public IEnumerable<RosValueMember> GetMembers(bool dynamic, bool undefined)
        {
            return GetMembers().Where(vm =>
                (dynamic || !vm.Attribute.IsDynamic) &&
                (undefined || vm.GetValue(this) != null));
        }

        public IEnumerable<RosValueMember> GetKeyMembers()
        {
            return GetMembers().Where(vm => vm.Attribute.IsKey);
        }

        public IEnumerable<RosValueMember> GetRequiredMembers()
        {
            return GetMembers().Where(vm => vm.Attribute.IsRequired);
        }

        public IEnumerable<RosValueMember> GetChangedMembers()
        {
            return GetMembers().Where(vm => 
                !vm.Attribute.IsDynamic && 
                vm.GetValue(this) != null && 
                (!SavedState.ContainsKey(vm.Key) || SavedState[vm.Key] != vm.GetValue(this).ToString()));
        }

        public void SaveChanges()
        {
            foreach (var member in GetMembers())
            {
                object value = member.GetValue(this);
                if (value != null)
                    SavedState[member.Key] = value.ToString();
                else
                    SavedState.Remove(member.Key);
            }
        }

        public Query[] BuildQuery(Connection conn)
        {
            List<Query> queries = new List<Query>();
            foreach (var keyValue in GetChangedMembers())
                queries.Add(new EqualQuery(keyValue.Name, keyValue.GetValue(this).ToString(conn)));
            if (queries.Count == 0)
                return null;
            return queries.ToArray();
        }
    }

    public static partial class RosSerializationHelper
    {
        public static void CopyTo(this RosObject obj, ValueCommand cmd, Connection conn)
        {
            foreach (var keyValue in obj.GetChangedMembers())
                cmd.KeyValues[keyValue.Name] = keyValue.GetValue(obj).ToString(conn);
        }

        public static void CopyTo(this Record record, RosObject obj, Connection conn)
        {
            RosObject.CopyTo copyTo = RosObject.GetCopyTo(obj.GetType());

            if (copyTo != null)
            {
                try
                {
                    copyTo(record, obj, conn);
                    obj.SavedState = record.KeyValues;
                }
                catch (System.Security.VerificationException)
                {
                    Console.WriteLine("[EMIT] Failed to emit for type: {0}", obj.GetType());
                }
            }
            else
            {
                foreach (var member in obj.GetMembers())
                {
                    // get value from record or get default
                    string value;
                    if (!record.TryGetValue(member.Attribute.Name, out value))
                    {
                        value = member.Default;

                        if (value == null)
                        {
                            member.SetValue(obj, null);
                            obj.SavedState.Remove(member.Key);
                            continue;
                        }
                    }

                    // set value in class
                    try
                    {
                        object valueT = member.GetObject(value, conn);
                        member.SetValue(obj, valueT);
                        obj.SavedState[member.Key] = value;
                    }
                    catch (Exception)
                    {
                    }
                }
            }
            obj.LastUpdateTime = DateTime.Now;
        }
    }
}
