﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace RouterOS
{
    public struct MACAddress
    {
        private byte[] macAddress;
        private string text;

        public byte[] Address
        {
            get { return macAddress; }
            set
            {
                if (value != null && value.Length != 6)
                    throw new ArgumentException();
                macAddress = value;
            }
        }

        public MACAddress(byte[] macAddress)
        {
            if (macAddress.Length != 6)
                throw new ArgumentException();
            this.macAddress = macAddress;
            this.text = null;
        }

        public MACAddress(byte[] macAddress, int offset, int size)
        {
            if (size != 6)
                throw new ArgumentException();
            this.macAddress = new byte[size];
            this.text = null;
            Array.Copy(macAddress, offset, this.macAddress, 0, size);
        }

        public static MACAddress Parse(string macAddress)
        {
            string str = macAddress.Replace("-", "").Replace(":", "").ToLower();
            if (str.Length != 6 * 2)
                throw new ArgumentException();

            byte[] macAddressBytes = new byte[6];
            for (int i = 0; i < 6; i++)
                byte.TryParse(str.Substring(i * 2, 2), NumberStyles.HexNumber, null, out macAddressBytes[i]);
            return new MACAddress(macAddressBytes);
        }

        public override string ToString()
        {
            if (text == null)
            {
                if (macAddress == null || macAddress.Length != 6)
                    text = "00:00:00:00:00:00";
                else
                    text = String.Format("{0:X2}:{1:X2}:{2:X2}:{3:X2}:{4:X2}:{5:X2}",
                    macAddress[0], macAddress[1], macAddress[2],
                    macAddress[3], macAddress[4], macAddress[5]);
            }
            return text;
        }

        public static bool operator ==(MACAddress a, MACAddress b)
        {
            if (a.macAddress == b.macAddress)
                return true;
            if (a.macAddress == null || b.macAddress == null)
                return false;
            return a.macAddress.SequenceEqual(b.macAddress);
        }

        public static bool operator !=(MACAddress a, MACAddress b)
        {
            if (a.macAddress == b.macAddress)
                return false;
            if (a.macAddress == null || b.macAddress == null)
                return true;
            return !a.macAddress.SequenceEqual(b.macAddress);
        }

        public override int GetHashCode()
        {
            if (macAddress != null)
                return RouterOS.API.CommandKey.ComputeHash(macAddress);
            return 0;
        }

        public override bool Equals(object obj)
        {
            if (obj is MACAddress)
                return this == (MACAddress)obj;
            return false;
        }
    }
}
