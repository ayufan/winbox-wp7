﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RouterOS
{
    public struct Bits
    {
        public const long Kilobit = 1000;
        public const long Megabit = 1000 * Kilobit;
        public const long Gigabit = 1000 * Megabit;
        public static readonly Bits Unlimited = new Bits(-1);

        public long Count
        {
            get;
            set;
        }
        public bool IsUnlimited
        {
            get
            {
                return Count == -1;
            }
        }

        public Bits(long bits)
            : this()
        {
            Count = bits;
        }

        public static Bits Parse(string str)
        {
            if (str == "unlimited")
            {
                return Unlimited;
            }
            else if (str.Contains("k"))
            {
                return new Bits((long)(double.Parse(str.Split('k')[0]) * Kilobit));
            }
            else if (str.Contains("M"))
            {
                return new Bits((long)(double.Parse(str.Split('M')[0]) * Megabit));
            }
            else if (str.Contains("G"))
            {
                return new Bits((long)(double.Parse(str.Split('G')[0]) * Gigabit));
            }
            else
            {
                return new Bits((long)(double.Parse(str)));
            }
        }

        public string GetNiceString()
        {
            if (IsUnlimited)
                return "unlimited";
            if (Count > Gigabit)
                return Math.Round((double)Count / Gigabit, 1) + "G";
            if (Count > Megabit)
                return Math.Round((double)Count / Megabit, 1) + "M";
            if (Count > Kilobit)
                return Math.Round((double)Count / Kilobit, 1) + "k";
            return Count.ToString();
        }

        public override string ToString()
        {
            if (IsUnlimited)
                return "unlimited";
            if (Count == 0)
                return "0";
            if ((Count % Gigabit) == 0)
                return (Count / Gigabit) + "G";
            if ((Count % Megabit) == 0)
                return (Count / Megabit) + "M";
            if ((Count % Kilobit) == 0)
                return (Count / Kilobit) + "k";
            return Count.ToString();
        }

        public override int GetHashCode()
        {
            return Count.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj is Bytes)
                return this.Count == ((Bytes)obj).Count;
            return false;
        }

        public static implicit operator long(Bits bits)
        {
            return bits.Count;
        }

        public static implicit operator Bits(long value)
        {
            return new Bits(value);
        }
    }
}
