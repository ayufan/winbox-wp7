﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RouterOS
{
    public struct Bytes
    {
        public const long Kilobyte = 1024;
        public const long Megabyte = 1024 * Kilobyte;
        public const long Gigabyte = 1024 * Megabyte;
        public static readonly Bytes Unlimited = new Bytes(-1);

        public long Count
        {
            get;
            set;
        }
        public bool IsUnlimited
        {
            get
            {
                return Count == -1;
            }
        }

        public Bytes(long bytes)
            : this()
        {
            Count = bytes;
        }

        public static Bytes Parse(string str)
        {
            if (str == "unlimited")
            {
                return Unlimited;
            }
            else if (str.Contains("k"))
            {
                return new Bytes((long)(double.Parse(str.Split('k')[0]) * Kilobyte));
            }
            else if (str.Contains("M"))
            {
                return new Bytes((long)(double.Parse(str.Split('M')[0]) * Megabyte));
            }
            else if (str.Contains("G"))
            {
                return new Bytes((long)(double.Parse(str.Split('G')[0]) * Gigabyte));
            }
            else
            {
                return new Bytes((long)(double.Parse(str)));
            }
        }

        public string GetNiceString()
        {
            if (IsUnlimited)
                return "unlimited";
            if (Count > Gigabyte)
                return Math.Round((double)Count / Gigabyte, 1) + "G";
            if (Count > Megabyte)
                return Math.Round((double)Count / Megabyte, 1) + "M";
            if (Count > Kilobyte)
                return Math.Round((double)Count / Kilobyte, 1) + "k";
            return Count.ToString();
        }

        public override string ToString()
        {
            if (IsUnlimited)
                return "unlimited";
            if (Count == 0)
                return "0";
            if ((Count % Gigabyte) == 0)
                return (Count / Gigabyte) + "G";
            if ((Count % Megabyte) == 0)
                return (Count / Megabyte) + "M";
            if ((Count % Kilobyte) == 0)
                return (Count / Kilobyte) + "k";
            return Count.ToString();
        }

        public override int GetHashCode()
        {
            return Count.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj is Bits)
                return this.Count == ((Bits)obj).Count;
            return false;
        }

        public static implicit operator long(Bytes bytes)
        {
            return bytes.Count;
        }

        public static implicit operator Bytes(long value)
        {
            return new Bytes(value);
        }
    }
}
