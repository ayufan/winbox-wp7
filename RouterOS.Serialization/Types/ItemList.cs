﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using RouterOS.API;
using System.Text;

namespace RouterOS
{
    public class ItemList<T> : List<T>
    {
        public ItemList(params T[] items)
            : base(items)
        {
        }

        public static ItemList<T> Parse(string str, Connection conn)
        {
            ItemList<T> list = new ItemList<T>();
            if (string.IsNullOrWhiteSpace(str))
                return list;

            string[] items = str.Split(new char[] { ',' });
            foreach (string item in items)
            {
                list.Add(TypeFormatter.ConvertFromString<T>(str, conn));
            }
            return list;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (T item in this)
            {
                if (sb.Length != 0)
                    sb.Append(',');
                sb.Append(item.ToString());
            }
            return sb.ToString();
        }
    }

    public struct DblValue<T1,T2> where T1:new()
    {
        public T1 V1 { get; set; }
        public T2 V2 { get; set; }

        public static DblValue<T1,T2> Parse(string text, Connection conn)
        {
            string[] items = text.Split(new char[] { '/' });
            DblValue<T1, T2> value = new DblValue<T1, T2>();
            if (items.Length >= 1)
                value.V1 = TypeFormatter.ConvertFromString<T1>(items[0], conn);
            if (items.Length >= 2)
                value.V2 = TypeFormatter.ConvertFromString<T2>(items[1], conn);
            return value;
        }

        public override string ToString()
        {
            return String.Format("{0}/{1}", V1, V2);
        }
    }
}
