﻿using System;
using System.Collections.Generic;
using System.Reflection.Emit;
using RouterOS.API;

namespace RouterOS.Formatters
{
    public class ArrayTypeFormatter : ITypeFormatter
    {
        private Type Type
        {
            get;
            set;
        }

        public ArrayTypeFormatter(Type type)
        {
            Type = type;
        }

        public object ConvertFromString(string value, Connection connection)
        {
            string[] strs = value.Split(',');
            Array objs = Array.CreateInstance(Type, strs.Length);
            for (int i = 0; i < strs.Length; ++i)
                objs.SetValue(TypeFormatter.ConvertFromString(Type, strs[i], connection), i);
            return objs;
        }

        public string ConvertToString(object value, Connection connection)
        {
            if (value is Array)
            {
                Array arr = (Array)value;
                string str = "";
                for (int i = 0; i < arr.Length; ++i)
                {
                    if (str != "")
                        str += ",";
                    str += TypeFormatter.ConvertToString(Type, arr.GetValue(i), connection);
                }
                return str;
            }
            throw new NotSupportedException();
        }

        public bool EmitConvertFromString(ILGenerator il, LocalBuilder value)
        {
            return false;
        }
    }
}
