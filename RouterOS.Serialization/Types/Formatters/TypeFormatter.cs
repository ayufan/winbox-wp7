﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Net;
using System.Globalization;
using System.Reflection.Emit;
using RouterOS.API;
using CommonLib;

namespace RouterOS
{
    public interface ITypeFormatter
    {
        object ConvertFromString(string value, Connection connection);
        string ConvertToString(Object value, Connection connection);
        bool EmitConvertFromString(ILGenerator il, LocalBuilder value);
    }

    public static partial class TypeFormatter
    {
        static Dictionary<Type, ITypeFormatter> FormatterDict = new Dictionary<Type, ITypeFormatter>();

        static TypeFormatter()
        {
            var types = AppDomain.CurrentDomain.GetAssemblies().SelectMany(s => s.GetTypes()).Where(p => typeof(ITypeFormatter).IsAssignableFrom(p));
            foreach (var type in types)
            {
                TypeFormatterAttribute[] attrs = (TypeFormatterAttribute[])type.GetCustomAttributes(typeof(TypeFormatterAttribute), false);
                foreach(var attr in attrs)
                {
                    FormatterDict[attr.FormattedType] = (ITypeFormatter)Activator.CreateInstance(type);
                }
            }
        }

        public static bool GetIsNullable(this Type type)
        {
            return type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>);
        }

        private static ITypeFormatter GetFormatterForType(Type type)
        {
            if (GetIsNullable(type))
            {
                type = Nullable.GetUnderlyingType(type);
            }

            ITypeFormatter formatter;
            if (FormatterDict.TryGetValue(type, out formatter))
                return formatter;

            if (type.IsEnum)
            {
                formatter = new Formatters.EnumTypeFormatter(type);
            }
            else if (type.IsArray)
            {
                formatter = new Formatters.ArrayTypeFormatter(type.GetElementType());
            }
            else
            {
                formatter = new Formatters.GenericTypeFormatter(type);
            }
            FormatterDict[type] = formatter;
            return formatter;
        }

        public static object ConvertFromString(Type type, string value, Connection connection)
        {
            ITypeFormatter typeFormatter = GetFormatterForType(type);
            return typeFormatter.ConvertFromString(value, connection);
        }

        internal static void EmitConvertFromString(ILGenerator il, Type type, LocalBuilder value)
        {
            ITypeFormatter typeFormatter = GetFormatterForType(type);
            if (typeFormatter.EmitConvertFromString(il, value))
            {
                if (GetIsNullable(type))
                {
                    il.Emit(OpCodes.Newobj, type.GetConstructor(new Type[] { Nullable.GetUnderlyingType(type)}));
                }
                return;
            }

            il.Emit(OpCodes.Ldtoken, type);
            il.Emit(OpCodes.Call, typeof(Type).GetMethod("GetTypeFromHandle", new Type[1]{typeof(RuntimeTypeHandle)}));
            il.Emit(OpCodes.Ldloc, value);
            il.Emit(OpCodes.Ldarg_2);
            il.Emit(OpCodes.Call, typeof(TypeFormatter).GetMethod("ConvertFromString", new Type[] { typeof(Type), typeof(string), typeof(Connection) }));
            il.Emit(OpCodes.Unbox_Any, type);
        }

        public static string ToString(this Object value, Connection connection)
        {
            if (value == null)
                return null;
            ITypeFormatter typeFormatter = GetFormatterForType(value.GetType());
            return typeFormatter.ConvertToString(value, connection);
        }

        public static string ConvertToString(Type type, Object value, Connection connection)
        {
            ITypeFormatter typeFormatter = GetFormatterForType(type);
            return typeFormatter.ConvertToString(value, connection);
        }

        public static T ConvertFromString<T>(string value, Connection connection)
        {
            ITypeFormatter typeFormatter = GetFormatterForType(typeof(T));
            return (T)typeFormatter.ConvertFromString(value, connection);
        }

        public static string ConvertToString<T>(T value, Connection connection)
        {
            ITypeFormatter typeFormatter = GetFormatterForType(typeof(T));
            return typeFormatter.ConvertToString(value, connection);
        }
    }
}
