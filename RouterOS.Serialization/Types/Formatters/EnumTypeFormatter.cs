﻿using System;
using System.Collections.Generic;
using System.Reflection.Emit;
using RouterOS.API;
using RouterOS.Serialization;

namespace RouterOS.Formatters
{
    public class EnumTypeFormatter : ITypeFormatter
    {
        private Type Type
        {
            get;
            set;
        }
        private Dictionary<string, object> From
        {
            get;
            set;
        }
        private Dictionary<object, string> To
        {
            get;
            set;
        }

        public EnumTypeFormatter(Type type)
        {
            Type = type;
            From = new Dictionary<string, object>();
            To = new Dictionary<object, string>();

            foreach (var fi in Type.GetFields())
            {
                RosValueAttribute[] attrs = (RosValueAttribute[])fi.GetCustomAttributes(typeof(RosValueAttribute), false);
                if (attrs.Length > 0)
                {
                    string e = attrs[0].Name;
                    object o = Enum.Parse(Type, fi.Name, true);
                    From[e] = o;
                    To[o] = e;
                }
            }
        }

        public object ConvertFromString(string value, Connection connection)
        {
            return From[value];
        }

        public string ConvertToString(object value, Connection connection)
        {
            return To[value];
        }

        public bool EmitConvertFromString(ILGenerator il, LocalBuilder value)
        {
            return false;
        }
    }

}
