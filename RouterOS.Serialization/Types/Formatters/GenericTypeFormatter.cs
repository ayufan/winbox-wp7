﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;
using RouterOS.API;

namespace RouterOS.Formatters
{
    public class GenericTypeFormatter : ITypeFormatter
    {
        private Type Type
        {
            get;
            set;
        }
        private MethodInfo ParseMethod
        {
            get;
            set;
        }
        private bool ParseNoConn
        {
            get;
            set;
        }
        private MethodInfo ToStringMethod
        {
            get;
            set;
        }
        private bool ToStringNoConn
        {
            get;
            set;
        }

        public GenericTypeFormatter(Type type)
        {
            Type = type;
            ParseMethod = Type.GetMethod("Parse", new Type[] { typeof(string), typeof(Connection) });
            if (ParseMethod == null)
            {
                ParseMethod = Type.GetMethod("Parse", new Type[] { typeof(string) });
                if (ParseMethod == null)
                    throw new NotSupportedException();
                ParseNoConn = true;
            }
            if (!ParseMethod.IsStatic)
                throw new NotSupportedException();

            ToStringMethod = Type.GetMethod("ToString", new Type[] { typeof(Connection) });
            if (ToStringMethod == null)
            {
                ToStringMethod = Type.GetMethod("ToString", new Type[] { });
                if (ToStringMethod == null)
                    throw new NotSupportedException();
                ToStringNoConn = true;
            }
            if (ToStringMethod.IsStatic)
                throw new NotSupportedException();
        }

        public object ConvertFromString(string value, Connection connection)
        {
            if (ParseNoConn)
            {
                return ParseMethod.Invoke(null, new object[] { value });
            }
            else
            {
                object obj;
                if (!connection.GetObjectFromCache(Type, value, out obj))
                {
                    obj = ParseMethod.Invoke(null, new object[] { value, connection });
                    connection.AddObjectToCache(Type, value, obj);
                }
                return obj;
            }
        }

        public string ConvertToString(object value, Connection connection)
        {
            if (ToStringNoConn)
                return (string)ToStringMethod.Invoke(value, new object[] { });
            else
                return (string)ToStringMethod.Invoke(value, new object[] { connection });
        }

        public bool EmitConvertFromString(ILGenerator il, LocalBuilder value)
        {
            il.Emit(OpCodes.Ldloc, value);
            if (ParseNoConn)
            {
                il.Emit(OpCodes.Call, ParseMethod);
            }
            else
            {
                il.Emit(OpCodes.Ldarg_2);
                il.Emit(OpCodes.Call, ParseMethod);
            }
            return true;
        }
    }
}
