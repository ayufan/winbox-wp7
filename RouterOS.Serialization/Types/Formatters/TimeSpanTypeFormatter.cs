﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;
using RouterOS.API;

namespace RouterOS.Formatters
{
    [TypeFormatter(typeof(TimeSpan))]
    public class TimeSpanFormatter : ITypeFormatter, System.Windows.Data.IValueConverter
    {
        public static TimeSpan ConvertFromStringType(string value)
        {
            int days = 0, hours = 0, minutes = 0, seconds = 0, milliseconds = 0;

            int weeksOffset = value.IndexOf('w');
            if (weeksOffset >= 0)
            {
                days += 7 * int.Parse(value.Substring(0, weeksOffset));
                value = value.Substring(weeksOffset + 1);
            }

            int daysOffset = value.IndexOf('d');
            if (daysOffset >= 0)
            {
                days += int.Parse(value.Substring(0, daysOffset));
                value = value.Substring(daysOffset + 1);
            }

            int hoursOffset = value.IndexOf('h');
            if (hoursOffset >= 0)
            {
                hours += int.Parse(value.Substring(0, hoursOffset));
                value = value.Substring(hoursOffset + 1);
            }

            int minutesOffset = value.IndexOf('h');
            if (minutesOffset >= 0)
            {
                minutes += int.Parse(value.Substring(0, minutesOffset));
                value = value.Substring(minutesOffset + 1);
            }

            int secondsOffset = value.IndexOf('h');
            if (secondsOffset >= 0)
            {
                seconds += int.Parse(value.Substring(0, secondsOffset));
                value = value.Substring(secondsOffset + 1);
            }

            if (hours == 0 && minutes == 0 && seconds == 0)
            {
                string[] dotsep = value.Split('.');
                string[] hms = dotsep[0].Split(':');

                if (hms.Length == 3)
                {
                    hours += int.Parse(hms[0]);
                    minutes += int.Parse(hms[1]);
                    seconds += int.Parse(hms[2]);
                }

                if (dotsep.Length > 1)
                    milliseconds += int.Parse(dotsep[1]);
            }

            return new TimeSpan(days, hours, minutes, seconds, milliseconds);
        }

        public object ConvertFromString(string value, Connection connection)
        {
            return ConvertFromStringType(value);
        }

        public string ConvertToString(object value, Connection connection)
        {
            if (value is TimeSpan)
            {
                TimeSpan ts = (TimeSpan)value;
                string ret = "";

                if (ts.Days > 7)
                {
                    ret += (ts.Days / 7) + "w";
                }

                if ((ts.Days % 7) > 0)
                {
                    ret += (ts.Days % 7) + "d";
                }

                ret += String.Format("{0}:{1}:{2}", ts.Hours, ts.Minutes, ts.Seconds);

                if (ts.Milliseconds > 0)
                {
                    ret += "." + ts.Milliseconds;
                }
                return ret;
            }
            throw new NotSupportedException();
        }

        public bool EmitConvertFromString(ILGenerator il, LocalBuilder value)
        {
            MethodInfo convertMethod = GetType().GetMethod("ConvertFromStringType");
            il.Emit(OpCodes.Ldloc, value);
            il.Emit(OpCodes.Call, convertMethod);
            return true;
        }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
                return "";

            if (value is TimeSpan?)
            {
                TimeSpan? valueTS = (TimeSpan?)value;
                if (valueTS.HasValue)
                    return ConvertToString(valueTS.Value, null);
                else
                    return "";
            }

            return ConvertToString(value, null);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }

    public class TimeSpanToNiceStringConverter : System.Windows.Data.IValueConverter
    {
        public string ConvertToString(object value, Connection connection)
        {
            if (value is TimeSpan)
            {
                TimeSpan ts = (TimeSpan)value;
                string ret = "";

                if (ts.Days > 7)
                    ret += (ts.Days / 7) + "w";

                if ((ts.Days % 7) > 0)
                    ret += (ts.Days % 7) + "d";

                if (ts.Hours > 0)
                    ret += ts.Hours + "h";

                if (ts.Minutes > 0)
                    ret += ts.Minutes + "m";

                if (ts.Seconds > 0)
                    ret += ts.Seconds + "s";

                if (ts.Milliseconds > 0)
                    ret += ts.Milliseconds + "ms";

                if (ret == "")
                    ret = "<1ms";
                return ret;
            }
            throw new NotSupportedException();
        }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
                return "";

            if (value is TimeSpan?)
            {
                TimeSpan? valueTS = (TimeSpan?)value;
                if (valueTS.HasValue)
                    return ConvertToString(valueTS.Value, null);
                else
                    return "";
            }

            return ConvertToString(value, null);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }

    public class TimeSpanNoneFormatter : TimeSpanFormatter, ITypeFormatter
    {
        public new static TimeSpan ConvertFromStringType(string value)
        {
            if (value == "none")
                return TimeSpan.Zero;
            return TimeSpanFormatter.ConvertFromStringType(value);
        }

        public new object ConvertFromString(string value, Connection connection)
        {
            return ConvertFromStringType(value);
        }

        public new string ConvertToString(object value, Connection connection)
        {
            if (value is TimeSpan)
            {
                TimeSpan ts = (TimeSpan)value;
                if (ts == TimeSpan.Zero)
                    return "none";
            }
            return base.ConvertToString(value, connection);
        }

        public new bool EmitConvertFromString(ILGenerator il, LocalBuilder value)
        {
            MethodInfo convertMethod = GetType().GetMethod("ConvertFromStringType");
            il.Emit(OpCodes.Ldloc, value);
            il.Emit(OpCodes.Call, convertMethod);
            return true;
        }
    }
}
