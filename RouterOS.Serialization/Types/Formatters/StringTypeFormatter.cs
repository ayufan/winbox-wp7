﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;
using RouterOS.API;

namespace RouterOS.Formatters
{
    [TypeFormatter(typeof(string))]
    public class StringTypeFormatter : ITypeFormatter
    {
        public object ConvertFromString(string value, Connection connection)
        {
            return value;
        }

        public string ConvertToString(object value, Connection connection)
        {
            return value as string;
        }

        public bool EmitConvertFromString(ILGenerator il, LocalBuilder value)
        {
            il.Emit(OpCodes.Ldloc, value);
            return true;
        }
    }
}
