﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;
using RouterOS.API;

namespace RouterOS.Formatters
{
    [TypeFormatter(typeof(bool))]
    public class BooleanTypeFormatter : ITypeFormatter
    {
        public static bool ConvertFromStringType(string value)
        {
            if (value == "yes" || value == "true")
                return true;
            if (value == "no" || value == "false")
                return false;
            throw new ArgumentException();
        }

        public object ConvertFromString(string value, Connection connection)
        {
            return ConvertFromStringType(value);
        }

        public string ConvertToString(object value, Connection connection)
        {
            if (value is bool)
            {
                return (bool)value ? "yes" : "no";
            }
            throw new ArgumentException();
        }

        public bool EmitConvertFromString(ILGenerator il, LocalBuilder value)
        {
            MethodInfo convertMethod = GetType().GetMethod("ConvertFromStringType", new Type[] { typeof(string) });
            il.Emit(OpCodes.Ldloc, value);
            il.Emit(OpCodes.Call, convertMethod);
            return true;
        }
    }
}
