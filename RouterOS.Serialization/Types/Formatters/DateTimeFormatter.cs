﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;
using System.Globalization;
using RouterOS.API;

namespace RouterOS.Formatters
{
    [TypeFormatter(typeof(DateTime))]
    public class DateTimeFormatter : ITypeFormatter
    {
        private static DateTimeFormatInfo FormatInfo;

        static DateTimeFormatter()
        {
            FormatInfo = new DateTimeFormatInfo();
            // FormatInfo.DateSeparator = "/";
        }

        public static DateTime ConvertFromStringType(string value)
        {
            return DateTime.ParseExact(value, new string[] { "MMM/dd/yyyy HH:mm:ss", "MMM/dd HH:mm:ss", "HH:mm:ss" }, FormatInfo, DateTimeStyles.AllowWhiteSpaces);
        }

        public object ConvertFromString(string value, Connection connection)
        {
            return ConvertFromStringType(value);
        }

        public string ConvertToString(object value, Connection connection)
        {
            if (value is DateTime)
            {
                DateTime dt = (DateTime)value;
                if (dt.Year == 0)
                {
                    return dt.ToString("HH:mm:ss", FormatInfo);
                }
                else
                {
                    return dt.ToString("MMM/dd/yyyy HH:mm:ss", FormatInfo);
                }
            }
            throw new NotSupportedException();
        }

        public bool EmitConvertFromString(ILGenerator il, LocalBuilder value)
        {
            MethodInfo convertMethod = GetType().GetMethod("ConvertFromStringType");
            il.Emit(OpCodes.Ldloc, value);
            il.Emit(OpCodes.Call, convertMethod);
            return true;
        }
    }
}
