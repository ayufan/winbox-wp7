﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;
using RouterOS.API;

namespace RouterOS.Formatters
{
    public enum VersionBuild : int
    {
        Alpha,
        Beta,
        Candidate,
        Regular
    }

    [TypeFormatter(typeof(Version))]
    public class VersionTypeFormatter : ITypeFormatter
    {
        public static Version ConvertFromStringType(string value)
        {
            if (value.Contains("rc"))
                value = value.Replace("rc", "." + ((int)VersionBuild.Candidate).ToString() + ".");
            else if (value.Contains("beta"))
                value = value.Replace("beta", "." + ((int)VersionBuild.Beta).ToString() + ".");
            else if (value.Contains("alpha"))
                value = value.Replace("alpha", "." + ((int)VersionBuild.Alpha).ToString() + ".");

            Version version = Version.Parse(value);

            if (version.Build != 0 && version.Revision == 0)
            {
                version = new Version(version.Major, version.Minor, (int)VersionBuild.Regular, version.Build);
            }

            return version;
        }

        public object ConvertFromString(string value, Connection connection)
        {
            return ConvertFromStringType(value);
        }

        public string ConvertToString(object value, Connection connection)
        {
            if (value is Version)
            {
                Version version = (Version)value;
                switch ((VersionBuild)version.Build)
                {
                    case VersionBuild.Regular:
                        if (version.Revision != 0)
                            return String.Format("{0}.{1}.{2}", version.Major, version.Minor, version.Revision);
                        else
                            return String.Format("{0}.{1}", version.Major, version.Minor);

                    case VersionBuild.Alpha:
                        return String.Format("{0}.{1}alpha{2}", version.Major, version.Minor, version.Revision);

                    case VersionBuild.Beta:
                        return String.Format("{0}.{1}beta{2}", version.Major, version.Minor, version.Revision);

                    case VersionBuild.Candidate:
                        return String.Format("{0}.{1}rc{2}", version.Major, version.Minor, version.Revision);
                }
            }
            throw new NotSupportedException();
        }

        public bool EmitConvertFromString(ILGenerator il, LocalBuilder value)
        {
            MethodInfo convertMethod = GetType().GetMethod("ConvertFromStringType");
            il.Emit(OpCodes.Ldloc, value);
            il.Emit(OpCodes.Call, convertMethod);
            return true;
        }
    }
}
