﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace RouterOS
{
    public struct IPPrefix
    {
        private IPAddress ip;
        private int mask;
        private string text;

        public IPAddress IP
        {
            get { return ip; }
            set { if (ip != value) { ip = value; text = null; } }
        }
        public int Mask
        {
            get { return mask; }
            set { if (mask != value) { mask = value; text = null; } }
        }
        public uint Network
        {
            get { return (uint)~(((1 << (32 - Mask)) - 1) << Mask); }
        }

        public IPPrefix(IPAddress ip, int mask)
            : this()
        {
            if (mask < 0 || mask > 32)
                throw new ArgumentException();
            this.ip = ip;
            this.mask = mask;
        }

        public static IPPrefix Parse(string mask)
        {
            string[] ip = mask.Split(new char[] { '/' });
            return new IPPrefix(IPAddress.Parse(ip[0]), ip.Length == 2 ? int.Parse(ip[1]) : 32);
        }

        public override string ToString()
        {
            if(text == null)
                text = String.Format("{0}/{1}", ip != null ? ip.ToString() : "0.0.0.0", mask);
            return text;
        }
    }
}
