﻿using System;
using RouterOS.Serialization;


namespace RouterOS.Tools
{
    [RosObject("file", CanGet=true, CanListen=true, CanRemove=true, CanSet=true)]
    public class File : RosItemObject<File>
    {
        public const string BackupType = "backup";

        [RosValue("name")]
        public string NameAndExt { get; set; }

        public string Name
        {
            get
            {
                return System.IO.Path.GetFileNameWithoutExtension(string.IsNullOrEmpty(NameAndExt) ? "" : NameAndExt);
            }
        }

        [RosValue("creation-time")]
        public DateTime CreationTime { get; set; }

        [RosValue("size")]
        public Bytes Size { get; set; }
        
        [RosValue("type")]
        public string Type { get; set; }

        [RosValue("package-build-time")]
        public DateTime PackageBuildTime { get; set; }

        [RosValue("package-name")]
        public string PackageName { get; set; }

        [RosValue("package-version")]
        public Version PackageVersion { get; set; }
    }
}

