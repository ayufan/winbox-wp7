﻿using System;
using RouterOS.Serialization;


namespace RouterOS.Tools
{
    [RosObject("tool traceroute", IsCommand=true)]
    public class Trace : RosValueObject
    {
        [RosValue("address", IsDynamic=true)]
        public string Address { get; set; }

        [RosValue("rt1", IsDynamic = true)]
        public TimeSpan? Rt1 { get; set; }

        [RosValue("rt2", IsDynamic = true)]
        public TimeSpan? Rt2 { get; set; }

        [RosValue("rt3", IsDynamic = true)]
        public TimeSpan? Rt3 { get; set; }

        [RosValue("status", IsDynamic = true)]
        public string Status { get; set; }
    }

    public enum TraceProtocol
    {
        [RosValue("icmp")]
        Icmp,

        [RosValue("udp")]
        Udp
    }

    public class TraceOptions : RosObject
    {
        [RosValue("address")]
        public string Address { get; set; }

        [RosValue("dscp")]
        public int? Dscp { get; set; }

        [RosValue("protocol")]
        public TraceProtocol? Protocol { get; set; }

        [RosValue("port")]
        public int? UdpPort { get; set; }

        [RosValue("interface")]
        public Ref<Interface>? Interface { get; set; }

        [RosValue("timeout")]
        public TimeSpan? Timeout { get; set; }

        [RosValue("routing-table")]
        public string RoutingTable { get; set; }

        [RosValue("size")]
        public int? Size { get; set; }

        [RosValue("src-address")]
        public System.Net.IPAddress SrcAddress { get; set; }
    }
}
