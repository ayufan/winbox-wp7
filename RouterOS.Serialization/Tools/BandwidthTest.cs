﻿using System;
using RouterOS.Serialization;

namespace RouterOS.Tools
{
    [RosObject("tool bandwidth-test", IsCommand=true)]
    public class BandwidthTest : RosValueObject
    {
        [RosValue("status")]
        public string Status { get; set; }

        [RosValue("duration")]
        public TimeSpan? Duration { get; set; }

        [RosValue("direction")]
        public BandwidthTestDirection Direction { get; set; }

        [RosValue("rx-current")]
        public Bits RxCurrent { get; set; }

        public long RxValue { get { return RxCurrent.Count; } }

        [RosValue("rx-10-second-average")]
        public Bits Rx10sAverage { get; set; }

        [RosValue("rx-total-average")]
        public Bits RxTotalAverage { get; set; }

        [RosValue("tx-current")]
        public Bits TxCurrent { get; set; }

        public long TxValue { get { return TxCurrent.Count; } }

        [RosValue("tx-10-second-average")]
        public Bits Tx10sAverage { get; set; }

        [RosValue("tx-total-average")]
        public Bits TxTotalAverage { get; set; }
    }

    public enum BandwidthTestDirection
    {
        [RosValue("receive")]
        Receive,

        [RosValue("transmit")]
        Transmit,

        [RosValue("both")]
        Both
    }

    public enum BandwidthTestProtocol
    {
        [RosValue("tcp")]
        TCP,

        [RosValue("udp")]
        UDP
    }

    public class BandwidthTestOptions : RosObject
    {
        [RosValue("address")]
        public string Address { get; set; }

        [RosValue("direction")]
        public BandwidthTestDirection? Direction { get; set; }

        [RosValue("protocol")]
        public BandwidthTestProtocol? Protocol { get; set; }

        [RosValue("local-udp-tx-size")]
        public int? LocalUdpTxSize { get; set; }

        [RosValue("remote-udp-tx-size")]
        public int? RemoteUdpTxSize { get; set; }

        [RosValue("local-tx-speed")]
        public int? LocalTxSpeed { get; set; }

        [RosValue("remote-tx-speed")]
        public int? RemoteTxSpeed { get; set; }
        
        [RosValue("user")]
        public string User { get; set; }

        [RosValue("password")]
        public string Password { get; set; }

        [RosValue("interval")]
        public TimeSpan? Interval { get; set; }
            
        [RosValue("duration")]
        public TimeSpan? Duration { get; set; }

        [RosValue("random-data")]
        public bool? RandomData { get; set; }

        [RosValue("tcp-connection-count")]
        public int? TcpConnectionCount { get; set; }
    }
}
