﻿using System;
using RouterOS.Serialization;


namespace RouterOS.Tools
{
    [RosObject("ping", IsCommand=true)]
    public class Ping : RosValueObject
    {
        [RosValue("host", IsDynamic=true)]
        public string Host { get; set; }

        [RosValue("size", IsDynamic = true)]
        public int? Size { get; set; }

        [RosValue("ttl", IsDynamic = true)]
        public int? Ttl { get; set; }

        [RosValue("time", IsDynamic = true)]
        public TimeSpan? Time { get; set; }

        [RosValue("sent", IsDynamic = true)]
        public int? Sent { get; set; }

        [RosValue("received", IsDynamic = true)]
        public int? Received { get; set; }

        [RosValue("packet-loss", IsDynamic = true)]
        public int? PacketLoss { get; set; }

        [RosValue("min-rtt", IsDynamic = true)]
        public TimeSpan? MinRTT { get; set; }

        [RosValue("max-rtt", IsDynamic = true)]
        public TimeSpan? MaxRTT { get; set; }

        [RosValue("avg-rtt", IsDynamic = true)]
        public TimeSpan? AvgRTT { get; set; }

        [RosValue("status", IsDynamic = true)]
        public string Status { get; set; }
    }

    public class PingOptions : RosObject
    {
        [RosValue("address")]
        public string Address { get; set; }

        [RosValue("arp-ping")]
        public bool? ArpPing { get; set; }

        [RosValue("interface")]
        public Ref<Interface>? Interface { get; set; }

        [RosValue("interval")]
        public TimeSpan? Interval { get; set; }

        [RosValue("routing-table")]
        public string RoutingTable { get; set; }

        [RosValue("size")]
        public int? Size { get; set; }

        [RosValue("src-address")]
        public System.Net.IPAddress SrcAddress { get; set; }

        [RosValue("ttl")]
        public int? Ttl { get; set; }
    }
}
