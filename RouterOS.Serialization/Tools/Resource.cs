﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RouterOS.Serialization;

namespace RouterOS.Tools
{
    [RosObject("system resource", CanGet = true, CanSet = true, CanUnset = true)]
    public class Resource : RosValueObject<Resource>
    {
        [RosValue("uptime", IsDynamic = true)]
        public TimeSpan? Uptime { get; set; }

        [RosValue("version", IsDynamic = true)]
        public Version Version { get; set; }

        [RosValue("free-memory", IsDynamic = true)]
        public long? FreeMemory { get; set; }

        [RosValue("total-memory", IsDynamic = true)]
        public long? TotalMemory { get; set; }

        [RosValue("cpu", IsDynamic = true)]
        public string Cpu { get; set; }

        [RosValue("cpu-count", IsDynamic = true)]
        public int? CpuCount { get; set; }

        [RosValue("cpu-frequency", IsDynamic = true)]
        public int? CpuFrequency { get; set; }

        [RosValue("cpu-load", IsDynamic = true)]
        public int? CpuLoad { get; set; }

        [RosValue("free-hdd-space", IsDynamic = true)]
        public long? FreeHddSpace { get; set; }

        [RosValue("total-hdd-space", IsDynamic = true)]
        public long? TotalHddSpace { get; set; }

        [RosValue("write-sect-since-reboot", IsDynamic = true)]
        public int? WriteSinceReboot { get; set; }

        [RosValue("write-sect-total", IsDynamic = true)]
        public int? WriteTotal { get; set; }

        [RosValue("bad-blocks", IsDynamic = true)]
        public int? BadBlocks { get; set; }

        [RosValue("architecture-name", IsDynamic = true)]
        public string ArchitectureName { get; set; }

        [RosValue("board-name", IsDynamic = true)]
        public string BoardName { get; set; }

        [RosValue("platform", IsDynamic = true)]
        public string Platform { get; set; }

        public long MemUsagePercent
        {
            get
            {
                if (TotalMemory.GetValueOrDefault() == 0)
                    return 100;

                return (TotalMemory.GetValueOrDefault() - FreeMemory.GetValueOrDefault()) * 100 / 
                    TotalMemory.GetValueOrDefault();
            }
        }
        public long MemTotalPercent
        {
            get { return 100; }
        }
        public long MemFreePercent
        {
            get
            {
                return TotalMemory.GetValueOrDefault() != 0 ?
                    FreeMemory.GetValueOrDefault() * 100 / TotalMemory.GetValueOrDefault() : 0;
            }
        }


        public long DiskUsagePercent
        {
            get
            {
                if (TotalHddSpace.GetValueOrDefault() == 0)
                    return 100;

                return (TotalHddSpace.GetValueOrDefault() - FreeHddSpace.GetValueOrDefault()) * 100 /
                    TotalHddSpace.GetValueOrDefault();
            }
        }
        public long DiskTotalPercent
        {
            get { return 100; }
        }
        public long DiskFreePercent
        {
            get
            {
                return TotalHddSpace.GetValueOrDefault() != 0 ?
                    FreeHddSpace.GetValueOrDefault() * 100 / TotalHddSpace.GetValueOrDefault() : 0;
            }
        }
    }
}
