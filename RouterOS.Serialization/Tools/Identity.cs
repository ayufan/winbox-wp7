﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RouterOS.Serialization;

namespace RouterOS.Tools
{
    [RosObject("system identity", CanGet=true, CanSet=true, UseInterval=2000)]
    public class Identity : RosValueObject<Identity>
    {
        [RosValue("name")]
        public string Name { get; set; }
    }
}
